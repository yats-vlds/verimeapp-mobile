import Reactotron from 'reactotron-react-native';
import {reactotronRedux} from 'reactotron-redux'
import sagaPlugin from 'reactotron-redux-saga'
import AsyncStorage from '@react-native-community/async-storage';
const reactotron = Reactotron
    .configure({name: 'verime'})
    .useReactNative()
    .use(reactotronRedux())
    .use(sagaPlugin())
    .setAsyncStorageHandler(AsyncStorage) //  <- here i am!
    .connect() //Don't forget about me!

export default reactotron
// Reactotron
//     .setAsyncStorageHandler(AsyncStorage) // AsyncStorage would either come from `react-native` or `@react-native-community/async-storage` depending on where you get it from
//     .configure() // controls connection & communication settings
//     .useReactNative()
//     // add all built-in react native plugins
//     .connect() // let's connect!
// const tron = Reactotron.configure()
// .useReactNative()
// +  .use(reactotronRedux())
// .setAsyncStorageHandler(AsyncStorage)
// .connect();

// + export default tron;