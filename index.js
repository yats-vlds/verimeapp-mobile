/**
 * @format
 */

import {AppRegistry} from 'react-native';
import messaging from '@react-native-firebase/messaging';
import invokeApp from 'react-native-invoke-app';

import App from './App';
import {name as appName} from './app.json';
import videoCalling from './src/components/videoCalling';

AppRegistry.registerComponent(appName, () => App);


AppRegistry.registerHeadlessTask(
  'RNPushNotificationActionHandlerTask', () => notificationActionHandler,
);

const notificationActionHandler = async (data) => {
  // Your background task
  const yourObject = { route: videoCalling };
  console.log(yourObject);
  invokeApp({
    data: yourObject,
  })
}