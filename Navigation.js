import React, {Component} from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Icon from 'react-native-vector-icons/Ionicons';

import Home from './src/screens/Home';
import review from './src/components/review';
import search from './src/screens/Search';
import PaymentVerification from './src/screens/PaymentVerification';
import policeConfirmation from './src/components/policeConfirmation';
import commissionerPin from './src/components/commisionerPin';
import congratulation from './src/components/congratulation';
import Documents from './src/screens/Documents';
import video_home from './src/components/video_home';
import AddNewDocument from './src/components/AddNewDocument';
import uploadPage from './src/components/uploadPage';
import Settings from './src/screens/Settings';
import NumberVerification from './src/screens/NumberVerification';
import videoPlayer from './src/components/videoPlayer';
import DocumentView from './src/screens/DocumentView';
import ReScanDocument from './src/screens/ReScanDocument';
import DocumentScan from './src/components/DocumentScan';
import ReScanDocumentReviewToUpload from './src/screens/ReScanDocumentReviewToUpload';
import DocumentReviewToUpload from './src/components/DocumentReviewToUpload';
import policeScannerView from './src/components/policeScannerView';
import documentRequest from './src/components/documentRequest';
import BadgeIcon from './src/components/badgeIcon';
import searchDocumentView from './src/components/searchDocumentView';
import Favourites from './src/components/Favourites';
import History from './src/components/History';
import digitaSignature from './src/components/digitalSignature';
import scanStampedDocument from './src/screens/ScanStampedDocument';
import signDocument from './src/components/signDocument';
import videoCalling from './src/components/videoCalling';
import policeVerification from './src/components/policeVerification';
import {Chatbot} from "./src/screens/Chatbot"

import verifier from './src/components/verifier';
import Empty from './src/components/Empty';
import Logout from './src/components/Logout';

const Tab = createBottomTabNavigator();
const Stack = createStackNavigator();
const Search = createStackNavigator();

var f = 0;
export function BottomNavigation() {
  return (
    <Tab.Navigator
      screenOptions={({route}) => ({
        tabBarIcon: ({focused, color, size}) => {
          let iconName;

          if (route.name === 'HOME') {
            iconName = focused ? 'home' : 'home';
          } else if (route.name === 'REQUESTS') {
            iconName = focused ? 'people-outline' : 'people-outline';
          } else if (route.name === 'SEARCH') {
            iconName = focused ? 'search' : 'search';
          } else if (route.name == 'FAVOURITE') {
            iconName = focused ? 'star' : 'star';
          }
          // You can return any component that you like here!
          return (
            <View>
              {route.name === 'REQUESTS' ? <BadgeIcon /> : null}
              <Icon
                name={iconName}
                size={route.name == 'REQUESTS' ? 23 : 20}
                style={{marginTop: route.name == 'REQUESTS' ? 10 : 0}}
                color="#148567"
              />
            </View>
          );
        },
      })}
      tabBarOptions={{
        // keyboardHidesTabBar: true,
        // position: 'absolute',
        tabBarPosition: 'bottom',
        activeTintColor: '#148567',
        inactiveTintColor: 'gray',
        style: {height: 70, alignItems: 'center'},
        labelStyle: {fontSize: 13, height: 20, marginBottom: 7},
        tabStyle: {height: 70, maxWidth: 88},
        route: HomeStack,
      }}>
      <Tab.Screen name="HOME" component={HomeStack} />
      <Tab.Screen name="REQUESTS" component={RequestsStack} />
      <Tab.Screen name="SEARCH" component={SearchStack} />
      <Tab.Screen name="FAVOURITE" component={HistoryStack} />
    </Tab.Navigator>
  );
}

function HomeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="home"
        component={Home}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="Chatbot"
        component={Chatbot}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="Documents"
        component={DocumentStack}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="DocumentScan"
        component={DocumentScan}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="DocumentReviewToUpload"
        component={DocumentReviewToUpload}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="AddNewDocument"
        component={AddNewDocument}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="ReScanDocument"
        component={ReScanDocument}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="video_home"
        component={video_home}
        options={{animationEnabled: false, headerShown: false}}
      />
      {/* <Stack.Screen name="commissionerPin" component={commissionerPin} options={{animationEnabled: false,headerShown: false}} />                     */}
      <Stack.Screen
        name="Settings"
        component={Settings}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="NumberVerification"
        component={NumberVerification}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="policeVerification"
        component={policeVerification}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="videoPlayer"
        component={videoPlayer}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="History"
        component={History}
        options={{animationEnabled: false, headerShown: false}}
      />

      <Stack.Screen
        name="videoCalling"
        component={videoCalling}
        options={{animationEnabled: false, headerShown: false}}
      />
      {/* <Stack.Screen name="friendSuggestion" component={friendSuggestion} options={{animationEnabled: false,headerShown: false}} /> */}
    </Stack.Navigator>
  );
}
function DocumentStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Documents"
        component={Documents}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="AddNewDocument"
        component={AddNewDocument}
        options={{animationEnabled: false, headerShown: false}}
      />

      <Stack.Screen
        name="ReScanDocument"
        component={ReScanDocument}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="DocumentView"
        component={DocumentView}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="DocumentScan"
        component={DocumentScan}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="DocumentReviewToUpload"
        component={DocumentReviewToUpload}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="ReScanDocumentReviewToUpload"
        component={ReScanDocumentReviewToUpload}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="uploadPage"
        component={uploadPage}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="congratulation"
        component={congratulation}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="policeScannerView"
        component={policeScannerView}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="PaymentVerification"
        component={PaymentVerification}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="policeConfirmation"
        component={policeConfirmation}
        options={{animationEnabled: false, headerShown: false}}
      />

      <Stack.Screen
        name="commissionerPin"
        component={commissionerPin}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="scanStampedDocument"
        component={scanStampedDocument}
        options={{animationEnabled: false, headerShown: false}}
      />

      <Stack.Screen
        name="digitaSignature"
        component={digitaSignature}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="verifier"
        component={verifier}
        options={{animationEnabled: false, headerShown: false}}
      />
    </Stack.Navigator>
  );
}
function RequestsStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="review"
        component={review}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="documentRequest"
        component={documentRequest}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="signDocument"
        component={signDocument}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="digitaSignature"
        component={digitaSignature}
        options={{animationEnabled: false, headerShown: false}}
      />
    </Stack.Navigator>
  );
}

function SearchStack() {
  return (
    <Search.Navigator>
      {/* <Stack.Screen
        name="Empty"
        component={Empty}
        options={{animationEnabled: false, headerShown: false}}
      /> */}
      <Stack.Screen
        name="search"
        component={search}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="searchDocumentView"
        component={searchDocumentView}
        options={{animationEnabled: false, headerShown: false}}
      />
    </Search.Navigator>
  );
}
function HistoryStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="History"
        component={History}
        options={{animationEnabled: false, headerShown: false}}
      />
      <Stack.Screen
        name="Favourites"
        component={Favourites}
        options={{animationEnabled: false, headerShown: false}}
      />
    </Stack.Navigator>
  );
}
// const MainNavigator = createStackNavigator({

//     welcome: {

//         screen: welcome,
//         navigationOptions: ({navigation}) => ({

//             header: null,
//             drawerLockMode: 'locked-closed',

//         }),
//     },
//     EnterPhone: {
//         screen: EnterPhone,
//         navigationOptions: ({navigation}) => ({

//             header: null,
//             drawerLockMode: 'locked-closed',
//         }),
//     },
//     OTP: {
//         screen: OTP,
//         navigationOptions: ({navigation}) => ({

//             header: null,
//             drawerLockMode: 'locked-closed',
//         }),
//     },
//     Kyc: {
//         screen: Kyc,
//         navigationOptions: ({navigation}) => ({

//             header: null,
//             drawerLockMode: 'locked-closed',
//         }),

//     }
//     ,
//     photo: {
//         screen: photo,
//         navigationOptions: ({navigation}) => ({
//             header: null,
//             drawerLockMode: 'locked-closed',
//         }),

//     },
//     video: {
//         screen: video,

//         navigationOptions: ({navigation}) => ({
//             header: null,
//             drawerLockMode: 'locked-closed',
//         }),

//     }

// },{
//     initialRouteName: 'welcome',
//     transitionConfig: () => fromLeft(),
// })
// // const DrawerNavigatorExample = createDrawerNavigator({

// // }
// // , {
// //   contentComponent: DrawerContentComponent,
// //   drawerWidth: 250
// // }
// // );

// const App = createAppContainer(MainNavigator);
