import React, {Component} from 'react';
import {createStore, applyMiddleware, compose} from 'redux';
import {Provider, connect} from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';
import invokeApp from 'react-native-invoke-app';
import {
  ToastAndroid,
} from 'react-native';
// import App from './Navigation';

import OnboardingStack from './src/navigation/OnboardingStack'
import {BottomNavigation} from './Navigation'

import rootReducer from './src/redux/index';
import Reactotron from './rectron_config';

import * as RootNavigation from './RootNavigation';
import {NavigationContainer} from '@react-navigation/native';

import { persistStore, persistReducer } from 'redux-persist'
import AsyncStorage from '@react-native-community/async-storage';

import createSagaMiddleware from 'redux-saga'

import messaging from '@react-native-firebase/messaging';

import rootSaga from './src/redux/sagas';
import * as Actions from '@redux/onboarding/actions';
import * as ActionsUser from '@redux/actions';
import * as UserActions from '@redux/user/actions';

import { navigationRef } from './RootNavigation';

import videoCalling from './src/components/videoCalling';



// create our new saga monitor
const sagaMonitor = Reactotron.createSagaMonitor()

const sagaMiddleware = createSagaMiddleware({sagaMonitor})

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: []
};

const persistedReducer = persistReducer(persistConfig, rootReducer);


let store = createStore(
  persistedReducer,
  compose(
    applyMiddleware(sagaMiddleware),
    Reactotron.createEnhancer(),
  ),
);

sagaMiddleware.run(rootSaga);
let persistor = persistStore(store)

if (__DEV__) {
  import('./rectron_config').then((d) => console.log('Reactotron Configured'));
}



const notificationActionHandler = async (remoteMessage) => {
  console.log(remoteMessage);
  // Your background task
  const yourObject = {route: videoCalling};

  invokeApp({
    data: yourObject,
  });
};

// messaging().setBackgroundMessageHandler(notificationActionHandler);

// AppRegistry.registerHeadlessTask(
//   'RNPushNotificationActionHandlerTask',
//   () => notificationActionHandler,
// );


export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timePassed: false,
      step: store.getState().onboarding.step,
    };
  }

  componentDidMount() {

    setTimeout(() => {
      if (store.getState().user.meetingId) {
        RootNavigation.navigate('videoCalling', {meetingId: store.getState().user.meetingId});
        store.dispatch(ActionsUser.setMeeting(null));
      }
    }, 3000)

    this.unsubscribe = store.subscribe(() => {
      const error = store.getState().onboarding.error;
      const step = store.getState().onboarding.step;
      if (error !== null && typeof error === 'string') {
        if (error !== null && typeof error === 'string' ) {
          ToastAndroid.show(error, ToastAndroid.SHORT);
          store.dispatch(Actions.clearError())
        }
      }
      if (this.step != step) {
        this.setState({step: step});
      }

      if (store.getState().user.meetingId) {

        setTimeout(() => {
          RootNavigation.navigate('videoCalling', {meetingId: store.getState().user.meetingId});
          store.dispatch(ActionsUser.setMeeting(null));
        }, 3000)
      }
    })

    messaging()
      .getToken()
      .then(token => {
        console.log(token);
        store.dispatch(UserActions.updateFcmToken(token))
      });

    this.messageSubscription = messaging().onMessage((message) => {
      notificationHandler(message);
    })

    messaging().onNotificationOpenedApp(remoteMessage => {
      if (remoteMessage.data.type == 'videoCall') {
        // RootNavigation.navigate('videoCalling', {meetingId: remoteMessage.data.meetingId});

        store.dispatch(ActionsUser.setMeeting(remoteMessage.data.meetingId))
      }
    });



    const notificationHandler = async (message) => {
      console.log(message);
      if (message.data.type) {
        switch (message.data.type) {
          case 'documentRequest':
            store.dispatch(UserActions.getDocumentRequests())
            ToastAndroid.show("New Document Request received", ToastAndroid.SHORT)
            break;
          case 'videoCall':
            // const yourObject = {route: videoCalling};
            invokeApp();
            store.dispatch(ActionsUser.setMeeting(message.data.meetingId))
            // RootNavigation.navigate('videoCalling', {meetingId: message.data.meetingId});

            // store.dispatch(UserActions.getDocumentRequests())
            ToastAndroid.show(message.notification.body, ToastAndroid.LONG)
            break;

          default:
            break;
        }
      }
    }

    messaging().setBackgroundMessageHandler(notificationHandler);
  }

  componentWillUnmount() {
    this.unsubscribe();
    this.messageSubscription();
    messaging().onTokenRefresh(token => {
      console.log(token);
      store.dispatch(UserActions.updateFcmToken(token))
    });

  }

  // componentWillMount() {
  //   DeviceEventEmitter.addListener('appInvoked', data => {
  //     const {route} = data;
  //     console.log('DeviceEmitter', data);
  //     // Using react-navigation library for navigation.
  //     RootNavigation.navigate('welcome', {ok: 'ok'});
  //   });
  // }

  // componentDidMount() {
  //   setTimeout(() => {
  //     this.setTimePassed();
  //   }, 1000);
  // }

  // setTimePassed() {
  //   this.setState({timePassed: true});
  // }

  render() {
    // if (!this.state.timePassed) {
    //   return (
    //     <View>
    //       <StatusBar hidden={true} />
    //       <Image
    //         style={{height: '100%', width: '100%'}}
    //         source={Images.splashScreen}
    //       />
    //     </View>
    //   );
    // } else
      return (
        <Provider store={store}>
          <PersistGate loading={null} persistor={persistor}>

            <NavigationContainer
              ref={navigationRef}
              linking={{
                config: {
                  screens: {
                    videoCalling: 'feed/:sort',
                  },
                },
              }}>

              {/*{this.state.step < 6 &&*/}
              {/*<OnboardingStack />*/}
              {/*}*/}
              {/*{this.state.step >= 6 &&*/}
              <BottomNavigation />
              {/*}*/}

            {/*Navigation*/}

        		</NavigationContainer>

          </PersistGate>
        </Provider>
      );

  }
}
