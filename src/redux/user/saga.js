import { call, put, takeEvery, select, takeLatest } from 'redux-saga/effects'

import {
  GET_USER,
  GET_DOCUMENTS,

  UPLOAD_CUSTOM_DOCUMENT,
  RESCAN_DOCUMENT,
  UPDATE_FCM_TOKEN,
  GET_DOCUMENTS_REQUESTS,
  ACCEPT_DOCUMENT,
  REJECT_DOCUMENT,
  HANDLE_REQUEST_ERROR,
  MARK_FAVORITE_DOCUMENT,
  SUBMIT_DOCS_FOR_VERIFICATION,
  SEND_BACK_SIGNED_DOC,
} from './types'

import {
  HANDLE_UNAUTHORIZED
} from './../onboarding/types.js'

import { Api } from './api'
import * as Actions from './actions';
import * as OnboardingActions from '../onboarding/actions';

import * as RootNavigation from './../../../RootNavigation.js';
import { useNavigation } from '@react-navigation/native';

const getUserId = state => state.onboarding.user_id;
const getToken = state => state.onboarding.token;

function* updateFcmToken(action) {
   try {

      const fcmtoken = action.payload;

      const token = yield select(getToken);

      const success = yield call(Api.updateFcmToken, token, fcmtoken);
      
    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* uploadRescanDoc(action) {
   try {

      const {id, pdfLink, name} = action.payload;

      const token = yield select(getToken);

      const success = yield call(Api.uploadRescanCustomDocument, token, pdfLink, id, name);
      
      if (success) {
        yield put(Actions.uploadCustomDocSuccess())
        yield put(Actions.getDocuments())
        RootNavigation.navigate('Documents')
      } else {
        throw new Error("Error while uploading document");
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* sendBackSignedDoc(action) {
   try {

      const {signedDoc, signReqId} = action.payload;

      const token = yield select(getToken);

      const success = yield call(Api.sendSignedPdf, token, signedDoc, signReqId);
      
      if (success) {
        yield put(Actions.getDocumentRequests())
        yield put(Actions.getDocuments())
        RootNavigation.navigate('Documents')
      } else {
        throw new Error("Error while uploading document");
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* uploadCustomDoc(action) {
   try {

      const {pdfLink, name} = action.payload;

      const token = yield select(getToken);

      const success = yield call(Api.uploadCustomDocument, token, pdfLink, name);
      
      if (success) {
        yield put(Actions.uploadCustomDocSuccess())
        yield put(Actions.getDocuments())
        RootNavigation.navigate('Documents')
      } else {
        throw new Error("Error while uploading document");
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* verifyDocList(action) {
   try {

      const {scannedDoc, pin} = action.payload;

      const token = yield select(getToken);
      const docList = yield select((state) => state.user.verificationList);

      

      const success = yield call(Api.verifyDocList, token, docList, scannedDoc, pin);
      console.log(success);
      if (success) {
        yield put(Actions.getDocuments())
        RootNavigation.navigate('verifier')
      } else {
        throw new Error("Error while uploading document");
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* acceptDocumentRequest(action) {  
   try {

      const token = yield select(getToken);

      const doc_id = action.payload;

      const success = yield call(Api.acceptDocumentRequest, token, doc_id);

      if (success) {
        // yield put(Actions.acceptDocumentRequestSuccess())
        yield put(Actions.getDocumentRequests())
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* rejectDocumentRequest(action) {  
   try {

      const token = yield select(getToken);

      const doc_id = action.payload;

      const success = yield call(Api.rejectDocumentRequest, token, doc_id);

      if (success) {
        // yield put(Actions.rejectDocumentRequestSuccess())
        yield put(Actions.getDocumentRequests())
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* markFavouriteDocument(action) {  
   try {

      const token = yield select(getToken);

      const doc_id = action.payload;

      const success = yield call(Api.markFavouriteDocument, token, doc_id);

      if (success) {
        yield put(Actions.getDocumentRequests())
      }

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* getDocumentRequests() {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.getDocumentRequests, token);
      
      yield put(Actions.getDocumentRequestsSuccess(data.request_list, data.accepted_list, data.rejected_list, data.sign_request_list))

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}
function* getDocuments() {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.getDocuments, token);
      
      yield put(Actions.getDocumentsSuccess(data))

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* getUser() {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.getUser, token);
      
      yield put(Actions.getUserSuccess(data))

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* handleLogout() {  
   try {

      // const token = yield select(getToken);

      // const data = yield call(Api.logoutUser, token);
      
      // yield put(Actions.getUserSuccess(data))

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}



function* userSaga() {
  yield takeLatest(GET_USER, getUser);
  yield takeLatest(GET_DOCUMENTS, getDocuments);
  yield takeLatest(UPLOAD_CUSTOM_DOCUMENT, uploadCustomDoc);
  yield takeLatest(RESCAN_DOCUMENT, uploadRescanDoc);
  yield takeLatest(GET_DOCUMENTS_REQUESTS, getDocumentRequests);
  yield takeLatest(ACCEPT_DOCUMENT, acceptDocumentRequest);
  yield takeLatest(REJECT_DOCUMENT, rejectDocumentRequest);
  
  yield takeLatest(MARK_FAVORITE_DOCUMENT, markFavouriteDocument);
  
  yield takeLatest(SUBMIT_DOCS_FOR_VERIFICATION, verifyDocList);
  
  yield takeLatest(SEND_BACK_SIGNED_DOC, sendBackSignedDoc);
  
  yield takeLatest(HANDLE_UNAUTHORIZED, handleLogout);
  
  
  yield takeLatest(UPDATE_FCM_TOKEN, updateFcmToken);

  yield takeLatest(HANDLE_REQUEST_ERROR, handleRequestError);
}



function* handleRequestError(action) {  

  const {code, message} = action.payload;
  
  switch (code) {
    case 401:
      yield put(OnboardingActions.handleLogout());
      yield put(OnboardingActions.navigateToStep());
      break;
    case 400:
      yield put(Actions.requestFailed(message))
      break;
    default:
      yield put(Actions.requestFailed(message))
      break;
  }

}


export default userSaga;