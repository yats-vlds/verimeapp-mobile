import {
  CLEAR_ERROR,
  REQUEST_FAILED,
  HANDLE_REQUEST_ERROR,
  UPLOAD_DOCUMENT,
  UPLOAD_DOCUMENT_SUCCESS,
  
  UPLOAD_PHOTO,
  UPLOAD_PHOTO_SUCCESS,
  
  UPLOAD_VIDEO,
  UPLOAD_VIDEO_SUCCESS,
  
  GET_USER,
  GET_USER_SUCCESS,

  GET_DOCUMENTS,
  GET_DOCUMENTS_SUCCESS,

  
  HANDLE_UNAUTHORIZED,
  UPLOAD_CUSTOM_DOCUMENT,
  UPLOAD_CUSTOM_DOCUMENT_SUCCESS,

  RESCAN_DOCUMENT,
  RESCAN_DOCUMENT_SUCCESS,

  GET_DOCUMENTS_REQUESTS,
  GET_DOCUMENTS_REQUESTS_SUCCESS,
  UPDATE_FCM_TOKEN,
  ACCEPT_DOCUMENT,
  REJECT_DOCUMENT,
  MARK_FAVORITE_DOCUMENT,

  DOC_LIST_FOR_VERIFICATION,
  SUBMIT_DOCS_FOR_VERIFICATION,
  SEND_BACK_SIGNED_DOC,
  SET_MEETING,
} from './types';

export const setMeeting = (meetingId) => ({
  type: SET_MEETING,
  payload: meetingId
})
export const clearError = () => ({
  type: CLEAR_ERROR
})
export const handleRequestError = (code, message) => ({
  type: HANDLE_REQUEST_ERROR,
  payload: {
    code,
    message
  }
})
export const requestFailed = (error) => ({
  type: REQUEST_FAILED,
  payload: error
})
export const handleLogout = () => ({
  type: HANDLE_UNAUTHORIZED
})


export const uploadDoc = (pdf, image) => ({
  type: UPLOAD_DOCUMENT,
  payload: {
    pdf, 
    image
  }
})
export const uploadDocSuccess = (data) => ({
  type: UPLOAD_DOCUMENT_SUCCESS,
  payload: data
})


export const uploadVideo = (video) => ({
  type: UPLOAD_VIDEO,
  payload: video
})
export const uploadVideoSuccess = (step) => ({
  type: UPLOAD_VIDEO_SUCCESS,
  payload: step
})


export const uploadPhoto = (photo) => ({
  type: UPLOAD_PHOTO,
  payload: photo
})
export const uploadPhotoSuccess = (step) => ({
  type: UPLOAD_PHOTO_SUCCESS,
  payload: step
})

export const getDocumentRequests = () => ({
  type: GET_DOCUMENTS_REQUESTS
})
export const getDocumentRequestsSuccess = (requestList, acceptedList, rejectedList, signRequestList) => ({
  type: GET_DOCUMENTS_REQUESTS_SUCCESS,
  payload: {
    requestList, 
    acceptedList, 
    rejectedList,
    signRequestList
  }
})

export const getUser = () => ({
  type: GET_USER
})
export const getUserSuccess = (data) => ({
  type: GET_USER_SUCCESS,
  payload: data
})

export const getDocuments = () => ({
  type: GET_DOCUMENTS
})
export const getDocumentsSuccess = (data) => ({
  type: GET_DOCUMENTS_SUCCESS,
  payload: data
})

export const acceptDocumentRequest = (doc_id) => ({
  type: ACCEPT_DOCUMENT,
  payload: doc_id
})
// export const acceptDocumentRequestSuccess = (data) => ({
//   type: ACCEPT_DOCUMENT_SUCCESS,
//   payload: data
// })

export const rejectDocumentRequest = (doc_id) => ({
  type: REJECT_DOCUMENT,
  payload: doc_id
})
// export const rejectDocumentRequestSuccess = (data) => ({
//   type: REJECT_DOCUMENT_SUCCESS,
//   payload: data
// })

export const uploadCustomDoc = (pdfLink, name) => ({
  type: UPLOAD_CUSTOM_DOCUMENT,
  payload: {pdfLink, name}
})
export const uploadCustomDocSuccess = () => ({
  type: UPLOAD_CUSTOM_DOCUMENT_SUCCESS,
})


export const reScanDoc = (id, pdfLink, name) => ({
  type: RESCAN_DOCUMENT,
  payload: {id, pdfLink, name}
})
export const reScanDocSuccess = () => ({
  type: RESCAN_DOCUMENT_SUCCESS,
})

export const updateFcmToken = (fcmToken) => ({
  type: UPDATE_FCM_TOKEN,
  payload: fcmToken
})

export const markFavouriteDocument = (doc_id) => ({
  type: MARK_FAVORITE_DOCUMENT,
  payload: doc_id
})
export const docsSelectedForVerification = (list) => ({
  type: DOC_LIST_FOR_VERIFICATION,
  payload: list
})
export const submitDocumentsForPoliceVerification = (scannedDoc, pin) => ({
  type: SUBMIT_DOCS_FOR_VERIFICATION,
  payload: {
    scannedDoc,
    pin,
  }
})
export const sendBackSignedDoc = (signedDoc, signReqId) => ({
  type: SEND_BACK_SIGNED_DOC,
  payload: {
    signedDoc,
    signReqId
  }
})