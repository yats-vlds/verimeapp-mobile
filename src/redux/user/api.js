import Constants from '@constants/index';
import Axios from 'axios';

const SERVER_URL = Constants.SERVER_URL + "/api";
var RNFS = require('react-native-fs');


export const instance = Axios.create({
  baseURL: SERVER_URL,
  timeout: 1000,
  headers: { 'X-Custom-Header': 'foobar' }
});

const api = async (url, options) => {
  try {
    return await fetch(`${SERVER_URL}${url}`, options);
  } catch (e) {
    throw e;
  }
}

export const Api = {
  register: async (phoneNo) => {

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({phone: phoneNo}),
    };

    try {
      const response = await api("/register", options)

      if (response.status == 201 || response.status == 409) {
          return await response.json();
      } else {
        throw new Error("Error connecting with server.");
      }
      
    } catch (e) {
      throw e;
    }
  },
  
  verifyOtp: async (user_id, code) => {

    const otp = Number(code);

    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({code: otp}),
    };

    try {
      const response = await api(`/user/${user_id}/verification`, options)

      switch (response.status) {
        case 200:
          return await response.json();
      
        case 401:
        case 404:
          throw new Error("Invalid OTP");
        case 400:
          throw new Error("Code not provided");

        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },


  uploadRescanCustomDocument: async (token, pdfLink, id, name) => {

    console.log(pdfLink, id, name)
    const form = new FormData();

    form.append('name', name);
    form.append('documents', {
      uri: pdfLink,
      type: 'application/pdf',
      name: 'document.pdf',
    });


    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };


    try {
      
      const response = await api(`/docs/${id}/rescan`, options);

      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },


  updateFcmToken: async (token, fcmToken) => {

    const form = new FormData();
    form.append('registration_id', fcmToken);


    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };


    try {
      
      const response = await api(`/user/registrationid`, options);

      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },
  
  uploadCustomDocument: async (token, pdfLink, name) => {

    const form = new FormData();

    form.append('name', name);
    form.append('type', 'document');
    form.append('documents', {
      uri: pdfLink,
      type: 'application/pdf',
      name: 'document.pdf',
    });


    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };


    try {
      
      const response = await api(`/docs`, options);

      switch (response.status) {
        case 201:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },

  sendSignedPdf: async (token, signedDoc, signReqId) => {

    const form = new FormData();

    form.append('requestId', signReqId);
    form.append('documents', {
      uri: signedDoc,
      type: 'application/pdf',
      name: 'document.pdf',
    });


    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };


    try {
      
      const response = await api(`/documents/sign`, options);

      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },

  verifyDocList: async (token, docList, pdfLink, pin) => {

    const form = new FormData();
    console.log("TesT", docList, pdfLink, pin);
    form.append('pin', pin);
    docList.forEach(docId => {
      
      form.append('docList[]', docId);
    });
    form.append('stampedDocumnet', {
      uri: pdfLink,
      type: 'application/pdf',
      name: 'document.pdf',
    });


    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };


    try {
      
      const response = await api(`/document/pin`, options);
      console.log(response);
      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        default:
          throw new Error("Some error occured");
      }
      
    } catch (e) {
      throw e;
    }
  },

  uploadVideo: async (token, data) => {

    const form = new FormData();
    form.append('video', {
      uri: data,
      type: 'video/mp4',
      name: 'video.mp4',
    });

    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };

    try {
      
      const response = await api(`/user/video`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }

          // .then(response => checkVideo(response.json(), dispatch, navi))
          // .catch(e => failVideo(dispatch, e));
          
    } catch (e) {
      throw e;
    }
    
  },

  uploadPhoto: async (token, data) => {



    const form = new FormData();
    form.append('img', {
      uri: data,
      type: 'image/jpg',
      name: 'image.jpg',
    });
  
    const options = {
      method: 'POST',
      headers: {
        'content-type': 'multipart/form-data;',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: form,
    };

    try {
      
      const response = await api(`/user/image`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }

          // .then(response => checkVideo(response.json(), dispatch, navi))
          // .catch(e => failVideo(dispatch, e));
          
    } catch (e) {
      throw e;
    }
    
  },
  
  updatePersonalInfo: async (token, data) => {

  
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        firstname: data.firstname,
        surname: data.surname,
        birth: data.dob,
        street_address: data.streetAddress,
        postal_address: data.postalAddress,
        email: data.email,
        mobile_no: data.mobileNo,
        tel_no: data.telNo,
        // person_status: data.person_status,
      }),
    };

    try {
      
      const response = await api(`/user/information`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },

  postalVerification: async (token, data) => {

  
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        firstname: data.firstname,
        surname: data.surname,
        address1: data.address1,
        addressOptional: data.addressOptional,
        city: data.city,
        zipCode: data.zipCode,
        email: data.email,
        type: 'postal'
      }),
    };

    try {
      
      const response = await api(`/user/paymentVerification`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  requestPaymentUrl: async (token, data) => {

  
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/user/paymentUrl`, options);
      
      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  rejectDocumentRequest: async (token, doc_id) => {
 
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/user/documents/${doc_id}/reject`, options);
      
      switch (response.status) {
        case 200:
          return true;
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  acceptDocumentRequest: async (token, doc_id) => {
 
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/user/documents/${doc_id}/accept`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  markFavouriteDocument: async (token, doc_id) => {
 
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/user/documents/${doc_id}/favorite`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  getDocumentRequests: async (token) => {

  
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/documents/request/list`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  getDocuments: async (token) => {

  
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/documents`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },
  getUser: async (token) => {

  
    const options = {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
    };

    try {
      
      const response = await api(`/user`, options);
      
      switch (response.status) {
        case 200:
          return await response.json();
        case 401:
          var e = new Error("Unauthorized");
          e.code = 401;
          throw e;
        case 400: 
          const error_detail = await response.json();
          const throwable = new Error(error_detail.error)
          throwable.code = 400;
          throw throwable;
          
        default:
          break;
      }
          
    } catch (e) {
      throw e;
    }
    
  },

}