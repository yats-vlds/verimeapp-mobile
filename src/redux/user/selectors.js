import { useSelector } from "react-redux";


export const getDocRequestList = () => (
  useSelector((state) => state.user.docRequestList)
);
export const getDocAcceptedList = () => (
  useSelector((state) => state.user.docAcceptedList)
);
export const getDocFavouriteList = () => (
  getDocAcceptedList().filter((value) => (value.star && value.star == 1))
);
export const getDocRejectedList = () => (
  useSelector((state) => state.user.docRejectedList)
);
export const getDocumentList = () => (
  useSelector((state) => state.user.documents)
);