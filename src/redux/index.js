import {combineReducers} from 'redux';
import addReducer from './reducers/phoneReducer';
import onboarding from './onboarding/reducer';
import user from './user/reducer';


import AsyncStorage from '@react-native-community/async-storage';
import { persistStore, persistReducer } from 'redux-persist'


const persistConfig = {
  key: 'onboarding',
  storage: AsyncStorage,
  blacklist: ['loading', 'error', 'payment_url']
};

const onboardingPersisted = persistReducer(persistConfig, onboarding);


export default combineReducers({
  addReducer,
  onboarding: onboardingPersisted,
  user,
});
