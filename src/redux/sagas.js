import { all, spawn, call } from 'redux-saga/effects'

import onboardingSaga from './onboarding/saga';
import userSaga from './user/saga';

// export default function* rootSaga() {
//   yield all([
//     yeild fork(onboardingSaga)
//   ])
// }


export default function* rootSaga () {
  const sagas = [
    onboardingSaga,
    userSaga
  ];

  yield all(sagas.map(saga =>
    spawn(function* () {
      while (true) {
        try {
          yield call(saga)
          break
        } catch (e) {
          console.log(e)
        }
      }
    }))
  );
}