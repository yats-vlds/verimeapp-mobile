import {
    CHECK_PHONE,
    PHONE_SUCCESSFUL,
    PHONE_FAIL,
    CHECK_OTP,
    OTP_SUCCESSFUL,
    OTP_FAIL,
    CHECK_DOC,
  } from '../actions/types';
  
const INTIALSTATE = {
  loading: false,
  user_id: null,
  token: null,
  step: null,
};

export default (state = INTIALSTATE, action) => {

  const {type, payload} = action;

  switch (type) {
    
    case CHECK_PHONE:
      return {...state, loading: true};
    case PHONE_SUCCESSFUL:
      return {...state, user_id: payload, loading: false};
    case PHONE_FAIL:
      return {...state, user_id: null, loading: false};
    case CHECK_OTP:
      return {...state, loading: true};
    case OTP_SUCCESSFUL:
      return {...state, loading: false, token: payload.token, step: payload.step};
    case OTP_FAIL:
      return {...state, loading: false};
    case CHECK_DOC:
      return {...state, loading: true}; 
  }

  return INTIALSTATE;
};
  