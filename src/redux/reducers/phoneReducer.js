import {
  ASYNC_TOKEN_ID,
  GET_CUSTOM_DOCS,
  GET_CUSTOM_DOCS_SUCCESS,
  GET_CUSTOM_DOCS_FAIL,
  CHECK_PHONE,
  PHONE_SUCCESSFUL,
  PHONE_FAIL,
  CHECK_OTP,
  OTP_SUCCESSFUL,
  OTP_FAIL,
  CHECK_DOC,
  DOC_FAIL,
  DOC_SUCCESSFUL,
  CHECK_PAYMENT,
  PAYMENT_FAIL,
  PAYMENT_SUCCESSFUL,
  CHECK_VIDEO,
  VIDEO_SUCCESSFUL,
  VIDEO_FAIL,
  SAVE_TOKEN,
  LOGIN_USER,
  ADD,
  BEFOREGOBACK,
  AFTERCGOBACK,
  VIDEO_SAVE,
  VIDEO_CALL,
  IDOCPASS,
  SAVE_CUSTOM_NAME,
  SAVE_URI,
  CALL_ID,
  CALL_TOKEN,
  ACCEPT_DOC_LIST,
  PERSONAL_NAME,
  PERSONAL_SUR_NAME,
  PERSONAL_EMAIL,
  PERSONAL_BIRTHDAY,
  PERSONAL_EMPLOYER,
  PERSONAL_ID,
  PEROSNAL_STREET_ADDRESS,
  PERSONAL_POSTAL_ADDRESS,
  PERSONAL_TELL,
  PERSONAL_INDUSTRY,
  PERSONAL_OCCUPATION,
  CHECK_REQUEST_DOCUMENT_SUCCESSFUL,
  CHECK_REQUEST_DOCUMENT,
  CHECK_REQUEST_DOCUMENT_FAIL,
  PUSH_SCREEN,
  FAVOURITE,
  ACCEPTEDDOCS,
  REJECTED_DOCUMENT_LIST,
  SCAN_STAMP_DOCS,
  SCAN_STAMP_DOCS_SENT,
} from '../actions/types';

const INTIALSTATE = {
  personal_name: '',
  personal_surName: '',
  personal_email: '',
  personal_birthday: '',
  personal_employer: '',
  personal_id: '',
  personal_streetAddress: '',
  personal_postalAddress: '',
  personal_tell: '',
  personal_industry: '',
  personal_occupation: '',
  saveName: '',
  saveUri: '',
  prevName: '',
  token: '',
  call_token: '',
  async_id: '',
  async_token: false,
  prevLine: 0,
  loading: false,
  payment: false,
  userInfo: '',
  get_docs: '',
  docs: '',
  img: '',
  vid: '',
  async_loader: false,
  firstItems: [{key: 'ID', color: '#148567'}],
  item: [{addLineNumber: 0}],
  id: '',
  call_id: '',
  document: '',
  video: '',
  call: false,
  acceptDocList: '',
  document_loader: false,
  Id_u: '',
  push_screen: false,
  accepteddocs: [],
  rejecteddocs: [],
};

const todos = (state = INTIALSTATE, action) => {
  switch (action.type) {
    case PUSH_SCREEN:
      return {...state, push_screen: action.payload};
    case FAVOURITE:
      var arr = {requested_list: []};

      state.acceptDocList.requested_list.map(item => {
        if (item.id == action.payload) {
          if (item.favorite) {
            item.favorite = false;
          } else {
            item.favorite = true;
          }
        }
      });
      return {...state};
    case PERSONAL_NAME:
      return {...state, personal_name: action.payload};
    case PERSONAL_SUR_NAME:
      return {...state, personal_surName: action.payload};
    case PERSONAL_BIRTHDAY:
      return {...state, personal_birthday: action.payload};
    case PERSONAL_EMAIL:
      return {...state, personal_email: action.payload};
    case PERSONAL_EMPLOYER:
      return {...state, personal_employer: action.payload};
    case PERSONAL_ID:
      return {...state, personal_id: action.payload};
    case PERSONAL_TELL:
      return {...state, personal_tell: action.payload};
    case PEROSNAL_STREET_ADDRESS:
      return {...state, personal_streetAddress: action.payload};
    case PERSONAL_POSTAL_ADDRESS:
      return {...state, personal_postalAddress: action.payload};
    case PERSONAL_INDUSTRY:
      return {...state, personal_industry: action.payload};
    case PERSONAL_OCCUPATION:
      return {...state, personal_occupation: action.payload};
    case ADD:
      var t = state.userInfo.image;
      var v = state.userInfo.video;
      var d = state.docs;

      return {...state, img: t, vid: v, get_docs: d};

    case ASYNC_TOKEN_ID:
      return {
        ...state,
        async_id: action.async_id,
        async_token: action.async_token,
        userInfo: action.userInfo,
        async_loader: action.loader,
      };
    case SAVE_TOKEN:
      return {...state, token: action.payload};
    case CALL_TOKEN:
      if (state.token != '') {
        var tt = state.token;
      } else {
        var tt = state.async_token;
      }
      return {...state, call_token: tt};

    case BEFOREGOBACK:
      return {...state, key: action.key};
    case AFTERCGOBACK:
      return {...state, key: action.key};
    case LOGIN_USER:
      return {...state};
    case CHECK_PHONE:
      return {...state, loading: true};
    case PHONE_SUCCESSFUL:
      return {...state, id: action.payload, loading: false};
    case PHONE_FAIL:
      return {...state, loading: false};
    case CHECK_OTP:
      return {...state, loading: true};
    case OTP_SUCCESSFUL:
      return {...state, loading: false, token: action.payload};
    case OTP_FAIL:
      return {...state, loading: false};
    case CHECK_DOC:
      return {...state, loading: true};
    case DOC_SUCCESSFUL:
      return {...state, loading: false};
    case DOC_FAIL:
      return {...state, loading: false};
    case CHECK_PAYMENT:
      return {...state, loading: true};
    case PAYMENT_SUCCESSFUL:
      return {...state, loading: false, payment: true};
    case PAYMENT_FAIL:
      return {...state, loading: false};
    case CHECK_VIDEO:
      return {...state, loading: true};
    case VIDEO_SUCCESSFUL:
      return {...state, loading: false};
    case VIDEO_FAIL:
      return {...state, loading: false};
    case GET_CUSTOM_DOCS:
      return {...state, async_loader: true};
    case GET_CUSTOM_DOCS_SUCCESS:
      action.payload.map(item => {
        item['select'] = false;
        item['sent'] = false;
      });
      return {...state, docs: action.payload, async_loader: false};
    case GET_CUSTOM_DOCS_FAIL:
      return {...state, async_loader: false};

    case CALL_ID:
      if (state.id != '') {
        var temp = state.id;
      } else {
        var temp = state.async_id;
      }
      return {...state, call_id: temp};
    case VIDEO_SAVE:
      return {...state, video: action.payload};
    case VIDEO_CALL:
      var v = state.video;
      return {...state, videos: v};
    case IDOCPASS:
      var firstItems = [];
      firstItems = state.firstItems;
      return {...state, firstItems: firstItems};
    case SAVE_CUSTOM_NAME:
      return {...state, saveName: action.payload, call: true};
    case SAVE_URI:
      const su = action.payload;
      return {...state, saveUri: su};
    case ACCEPT_DOC_LIST:
      action.payload.requested_list.map(item => {
        item['loader'] = false;
        item['favorite'] = false;
      });

      return {...state, acceptDocList: action.payload, document_loader: false};
    case CHECK_REQUEST_DOCUMENT:
      // state.acceptDocList.forEach(function(part, index) {
      //     if (part.id = payload) {
      //         state.acceptDocList[index].loading = true;
      //     }
      // });
      state.acceptDocList.requested_list.map(item => {
        if (item.id == action.payload) {
          item.loader = true;
        }
      });
      return {...state};
    case CHECK_REQUEST_DOCUMENT_FAIL:
      state.acceptDocList.requested_list.map(item => {
        if (item.id == action.payload) {
          item.loader = false;
        }
      });

      return {...state};
    case CHECK_REQUEST_DOCUMENT_SUCCESSFUL:
      var arr = {requested_list: []};
      arr.requested_list = state.acceptDocList.requested_list.filter(
        item => item.id != action.payload,
      );
      //   console.log('',arr)
      return {...state, acceptDocList: arr};
    case ACCEPTEDDOCS:
      return {...state, accepteddocs: action.payload};
    case REJECTED_DOCUMENT_LIST:
      return {...state, rejecteddocs: action.payload};
    case SCAN_STAMP_DOCS:
      state.get_docs.map(item => {
        if (item.id == action.payload) {
          if (item.select == false) item.select = true;
          else {
            item.select = false;
          }
        }
      });
      return {...state};
    case SCAN_STAMP_DOCS_SENT:
      state.get_docs.map(item => {
        if (item.id == action.payload) {
          if (item.sent == false && item.select == true) {
            item.sent = true;
            item.select = false;
          } else if (item.sent == true && item.select == true) {
            (item.sent = true), (item.select = false);
          } else {
            item.sent = false;
          }
        }
      });
      return {...state};
  }

  return INTIALSTATE;
};
export default todos;
