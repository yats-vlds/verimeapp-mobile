import {
    SET_USER_ID,
  } from '../actions/types';
  
const INTIALSTATE = {
  id: null,
};

export default (state = INTIALSTATE, action) => {

  const {type, payload} = action;

  switch (type) {
    
    case SET_USER_ID:
      return {...state, id: payload};
      
    
    
  }

  return INTIALSTATE;
};
  