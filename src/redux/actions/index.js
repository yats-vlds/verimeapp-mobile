import {
  ASYNC_TOKEN_ID,
  CHECK_PHONE,
  PHONE_SUCCESSFUL,
  PHONE_FAIL,
  CHECK_OTP,
  OTP_SUCCESSFUL,
  OTP_FAIL,
  CHECK_DOC,
  CHECK_PAYMENT,
  PAYMENT_SUCCESSFUL,
  PAYMENT_FAIL,
  DOC_SUCCESSFUL,
  DOC_FAIL,
  CHECK_VIDEO,
  VIDEO_SUCCESSFUL,
  VIDEO_FAIL,
  IDU,
  SAVE_TOKEN,
  CALL_TOKEN,
  CALL_ID,
  ADD,
  GET_CUSTOM_DOCS,
  GET_CUSTOM_DOCS_SUCCESS,
  GET_CUSTOM_DOCS_FAIL,
  ACCEPT_DOC_LIST,
  PERSONAL_NAME,
  PERSONAL_SUR_NAME,
  PERSONAL_EMAIL,
  PERSONAL_BIRTHDAY,
  PERSONAL_EMPLOYER,
  PERSONAL_ID,
  PEROSNAL_STREET_ADDRESS,
  PERSONAL_POSTAL_ADDRESS,
  PERSONAL_TELL,
  PERSONAL_INDUSTRY,
  PERSONAL_OCCUPATION,
  CHECK_REQUEST_DOCUMENT,
  CHECK_REQUEST_DOCUMENT_SUCCESSFUL,
  CHECK_REQUEST_DOCUMENT_FAIL,
  PUSH_SCREEN,
  VIEW_DIFFERENCE,
  FAVOURITE,
  ACCEPTEDDOCS,
  STAR_UPDATED,
  REJECTED_DOCUMENT_LIST,
  SCAN_STAMP_DOCS,
  SCAN_STAMP_DOCS_SENT,
} from './types';

import {ActionSheetIOS, Alert} from 'react-native';
import Toast from 'react-native-simple-toast';
import {State} from 'react-native-gesture-handler';
import NetInfo from '@react-native-community/netinfo';
var RNFS = require('react-native-fs');

import AsyncStorage from '@react-native-community/async-storage';
import {checkNotifications} from 'react-native-permissions';
const axios = require('axios');
const mime = require('mime');

import Constants from '../../Constants';

const SERVER_URL = Constants.SERVER_URL;

export const pushScreen = check => {
  return {
    type: PUSH_SCREEN,
    payload: check,
  };
};
export const favouriteColor = id => {
  return {type: FAVOURITE, payload: id};
};

export const personalName = name => {
  return {
    type: PERSONAL_NAME,
    payload: name,
  };
};
export const personalSurName = surName => {
  return {
    type: PERSONAL_SUR_NAME,
    payload: surName,
  };
};

export const personalBirthday = birthday => {
  return {
    type: PERSONAL_BIRTHDAY,
    payload: birthday,
  };
};
export const personalId = personalId => {
  return {
    type: PERSONAL_ID,
    payload: personalId,
  };
};
export const personalStreet = personalStreet => {
  return {
    type: PEROSNAL_STREET_ADDRESS,
    payload: personalStreet,
  };
};
export const personalPostalAddress = personalPostal => {
  return {
    type: PERSONAL_POSTAL_ADDRESS,
    payload: personalPostal,
  };
};
export const personaltell = personalTell => {
  return {
    type: PERSONAL_TELL,
    payload: personalTell,
  };
};
export const personalEmployer = personalEmployer => {
  return {
    type: PERSONAL_EMPLOYER,
    payload: personalEmployer,
  };
};
export const personalEmail = personalEmail => {
  return {
    type: PERSONAL_EMAIL,
    payload: personalEmail,
  };
};
export const personalIndustry = industry => {
  return {
    type: PERSONAL_INDUSTRY,
    payload: industry,
  };
};
export const personalOccupation = occupation => {
  return {
    type: PERSONAL_OCCUPATION,
    payload: occupation,
  };
};
export const Add = () => {
  return {
    type: ADD,
  };
};
export const saveToken = token => {
  return {
    type: SAVE_TOKEN,
    payload: token,
  };
};
export const callToken = () => {
  return {
    type: CALL_TOKEN,
  };
};

export const tokenStatus = (id, token, navi) => {
  token = token.replace(/^"(.*)"$/, '$1');

  return async dispatch => {
    dispatch({
      type: ASYNC_TOKEN_ID,
      async_id: '',
      async_token: '',
      userInfo: '',
      loader: true,
    });

    console.log(`${SERVER_URL}/api/user/${id}`, token);
    await fetch(`${SERVER_URL}/api/user/${id}`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
      .then(response => successToken(response.text(), id, token, dispatch))
      .catch(e => failToken(e, dispatch, navi));
  };

  // return {type: ASYNC_TOKEN_ID,async_id: id,async_token: token}
};
successToken = async (response, id, token, dispatch) => {
  const res = await response;
  console.log(res);

  dispatch({
    type: ASYNC_TOKEN_ID,
    async_id: id,
    async_token: token,
    userInfo: res,
  });
};
failToken = (e, dispatch, navi) => {
  // alert(e)
  console.log(e);
  Toast.show('Some thing went wrong', Toast.SHORT);

  dispatch({type: ASYNC_TOKEN_ID, async_id: '', async_token: '', userInfo: ''});
};
export const asyncLoader = () => {
  return {
    type: ASYNC_TOKEN_ID,
    async_id: '',
    async_token: '',
    userInfo: '',
    loader: true,
  };
};

export const registerNumber = (num, navi, formated_number) => {
  return async dispatch => {
    dispatch({type: CHECK_PHONE});
    await fetch(`${SERVER_URL}/api/register`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({phone: num}),
    })
      .then(response => {
        if (response.status == 201 || response.status == 409) {
          return response.json();
        } else {
          throw new Error("Error connecting with server.");
        }
      })
      .then(data => checkUser(data, dispatch, navi, num, formated_number))
      .catch(e => failUser(dispatch));
  };
};

const checkUser = async (data, dispatch, navi, number, formated_number) => {
  dispatch({
    type: PHONE_SUCCESSFUL,
    payload: data.id,
  });
  if (data != 'user_exist') {
    navi.navigate('otp', {phoneNo: formated_number});
  } else {
    navi.navigate('otp', {phoneNo: number});
  }
};
const failUser = (dispatch, e) => {
  // Toast.show('Something went wrong',Toast.SHORT)
  dispatch({
    type: PHONE_FAIL,
  });
};

export const verifyOTP = (code, id, navi) => {
  return async dispatch => {
    dispatch({type: CHECK_OTP});
    const pin = Number(code);
    await fetch(
      `${SERVER_URL}/api/user/${id}/verification`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          code: pin,
        }),
      },
    )
      .then(response => checkOTP(response, dispatch, navi))
      .catch(e => failOTP(dispatch));
  };
};
checkOTP = async (response, dispatch, navi) => {
  var res = null;
  if (response.status == 200) {
    res = await response.json();
  } else {
    Toast.show('Code not matched', Toast.SHORT);
    dispatch({type: OTP_FAIL});
    return;
  }

  dispatch({type: OTP_SUCCESSFUL, payload: {token: res.token, step: res.steps}});

  navigateToStep(navi, res.steps);
  
};

export const navigateToStep = (navi, step) => {

  switch (step) {
    case '1':
      navi.navigate('kyc');
      break;
    case '2':
      navi.navigate('video');
      break;
    case '3':
      navi.navigate('photo');
      break;
    case '4':
      navi.navigate('personalInfo');
      break;
    case '5':
      navi.navigate('PaymentVerification');
      break;
    case '6':
      navi.navigate('splashScreen2');
      break;
    default:
      navi.navigate('splashScreen2');
      break;
  }
}
failOTP = dispatch => {
  Toast.show('Something went wrong', Toast.SHORT);
  dispatch({type: OTP_FAIL});
};

export const saveId = data => {
  return {
    type: IDU,
    payload: data,
  };
};
export const callId = () => {
  return {
    type: CALL_ID,
  };
};
export const sendPhoto = (data, token, id, navi) => {
  const imguri = 'file:///' + data.split('file:/').join('');
  // console.log('data',data)
  const pp = data.replace('file://', '');

  const t = pp.split('/')[7];

  const form = new FormData();
  form.append('img', {
    uri: data,
    type: 'image/jpg',
    name: 'image.jpg',
  });

  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(
      `${SERVER_URL}/api/user/${id}/image`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: form,
      },
    )
      .then(response => checkDoc(response.json(), dispatch, navi))
      .catch(e => failDoc(dispatch, e));
  };
};
checkDoc = async (response, dispatch, navi) => {
  const r = await response;
  // console.log(r);
  if (r.success == 'uploaded') {
    Toast.show('Image Uploaded', Toast.SHORT);
    dispatch({type: DOC_SUCCESSFUL});
    navi.navigate('personalInfo');
  } else {
    Toast.show('Image upload fail', Toast.SHORT);
    dispatch({type: DOC_FAIL});
  }
};
failDoc = (dispatch, e) => {
  alert(e);
  Toast.show('Image upload fail', Toast.SHORT);
  dispatch({type: DOC_FAIL});
};

export const sendVideo = (data, token, id, navi) => {
  // console.log('.........11111...',data,token,id,navi);

  const form = new FormData();
  form.append('video', {
    uri: data,
    type: 'video/mp4',
    name: 'video.mp4',
  });
  // console.log('..................................',form)
  return async dispatch => {
    dispatch({type: CHECK_VIDEO});
    await fetch(
      `${SERVER_URL}/api/user/${id}/video`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: form,
      },
    )
      .then(response => checkVideo(response.json(), dispatch, navi))
      .catch(e => failVideo(dispatch, e));
  };
};

checkVideo = async (response, dispatch, navi) => {
  const r = response;
  // console.log('????????????????????????',r);
  Toast.show('Video Uploaded successfully', Toast.SHORT);
  dispatch({type: VIDEO_SUCCESSFUL});
  navi.navigate('photo');
};
failVideo = (dispatch, e) => {
  alert(e);
  Toast.show('Video upload fail', Toast.SHORT);
  dispatch({type: VIDEO_FAIL});
};
export const sendDoc = (data, token, id, navi, dataLink) => {
  const form = new FormData();
  form.append('ID', {
    uri: data,
    type: 'document/pdf',
    name: 'document.pdf',
  });

  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(
      `${SERVER_URL}/api/user/${id}/documents`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: form,
      },
    )
      .then(response => checkPhoto(response.json(), dispatch, navi, dataLink))
      .catch(e => failPhoto(dispatch, e));
  };
};
checkPhoto = async (response, dispatch, navi, dataLink) => {
  const t = await response;
  // if (t.message == 'document upload successfully  ') {
  Toast.show('Document Uploaded', Toast.SHORT);
  dispatch({type: DOC_SUCCESSFUL});
  await RNFS.unlink(dataLink)
    .then(() => {
      console.log('FILE DELETED');
    })
    // `unlink` will throw an error, if the item to unlink does not exist
    .catch(err => {
      console.log(err.message);
    });
  await navi.navigate('video');
  // } else {
  // Toast.show('Please carefully scan the document', Toast.SHORT);
  // dispatch({type: DOC_FAIL});
  //}
};
failPhoto = (dispatch, e) => {
  alert(e);
  Toast.show('Document upload fail', Toast.SHORT);
  dispatch({type: DOC_FAIL});
};

export const paymentV = (
  first_name,
  last_name,
  card_number,
  type,
  id,
  token,
  navi,
) => {
  return async dispatch => {
    dispatch({type: CHECK_PAYMENT});
    await fetch(
      `${SERVER_URL}/api/user/${id}/payment/verification`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'Content-Type': 'application/x-www-form-urlencode',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          first_name: first_name,
          last_name: last_name,
          type: type,
          card_number: card_number,
        }),
      },
    )
      .then(response => checkPayment(response.json(), dispatch, navi))
      .catch(e => failPayment(dispatch, e));
  };
};
export const paymentPostal = (
  first_name,
  last_name,
  address,
  address_op,
  city,
  zip_code,
  email,
  type,
  id,
  token,
  navi,
) => {
  return async dispatch => {
    dispatch({type: CHECK_PAYMENT});
    await fetch(
      `${SERVER_URL}/api/user/${id}/payment/verification`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          'Content-Type': 'application/x-www-form-urlencode',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          first_name: first_name,
          last_name: last_name,
          address: address,
          address_op: address_op,
          city: city,
          zip_code: zip_code,
          email: email,
          type: type,
        }),
      },
    )
      .then(response => checkPayment(response.json(), dispatch, navi))
      .catch(e => failPayment(dispatch, e));
  };
};
checkPayment = async (response, dispatch, navi) => {
  const res = await response;
  Toast.show('Information uploaded successfully', Toast.SHORT);
  dispatch({type: PAYMENT_SUCCESSFUL});

  navi.navigate('splashScreen2');
};
failPayment = (dispatch, e) => {
  alert(e);
  dispatch({type: PAYMENT_FAIL});
};

export const personalV = (
  firstname,
  sure_name,
  birth,
  street_address,
  postal_address,
  mobile_no,
  tel_no,
  person_status,
  email,
  id,
  token,
  navi,
) => {
  //token = token.replace(/^"(.*)"$/,'$1');

  return async dispatch => {
    dispatch({type: CHECK_PAYMENT});
    await fetch(
      `${SERVER_URL}/api/user/${id}/information`,
      {
        method: 'POST',
        headers: {
          Authorization: `Bearer ${token}`,
          'Content-Type': 'application/json',
          // 'Content-Type': 'multipart/form-data',
          // 'Content-Type': 'application/x-www-form-urlencode',
          Accept: 'application/json',
        },
        body: JSON.stringify({
          firstname: firstname,
          sure_name: sure_name,
          birth: birth,
          street_address: street_address,
          postal_address: postal_address,
          mobile_no: mobile_no,
          tel_no: tel_no,
          person_status: person_status,
          email: email,
        }),
      },
    )
      .then(response => checkPerson(response.json(), dispatch, navi))
      .catch(e => failPerson(dispatch, e));
  };
};
checkPerson = async (response, dispatch, navi) => {
  const b = await response;
  console.log(b);
  Toast.show('Information uploaded successfully', Toast.SHORT);
  dispatch({type: PAYMENT_SUCCESSFUL});
  navi.navigate('PaymentVerification');
};
failPerson = () => {
  dispatch({type: PAYMENT_FAIL});
};

export const addCustomDocument = (
  data,
  token,
  name,
  document,
  navi,
  dataLink,
) => {
  const form = new FormData();
  form.append('name', name);
  form.append('type', 'document');
  form.append('documents', {
    uri: data,
    type: 'document/pdf',
    name: 'document.pdf',
  });

  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(`${SERVER_URL}/api/docs`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      body: form,
    })
      .then(response => {
        console.log(response.status);
        checkCustomDoc(response, dispatch, navi, dataLink);
      })
      .catch(e => failCustomDoc(dispatch, e));
  };
};
checkCustomDoc = async (response, dispatch, navi, dataLink) => {
  if (response.status == 201) {
    Toast.show('Document Uploaded', Toast.SHORT);
    dispatch({type: DOC_SUCCESSFUL});
    await RNFS.unlink(dataLink)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });
    navi.navigate('congratulation');
  } else {
    Toast.show('Please scan document carefully', Toast.SHORT);
    dispatch({type: DOC_FAIL});
  }
};
failCustomDoc = (dispatch, e) => {
  alert(e);
  Toast.show('Document upload fail', Toast.SHORT);
  dispatch({type: DOC_FAIL});
};
export const getAllCustomDocuments = token => {
  token = token.replace(/^"(.*)"$/, '$1');

  return async dispatch => {
    dispatch({type: GET_CUSTOM_DOCS});

    await fetch(`${SERVER_URL}/api/documents`, {
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
      },
    })
      .then(response => successDocs(response.json(), dispatch))
      .catch(e => failDocs(dispatch));
  };

  // return {type: ASYNC_TOKEN_ID,async_id: id,async_token: token}
};
successDocs = async (response, dispatch) => {
  const t = await response;

  dispatch({type: GET_CUSTOM_DOCS_SUCCESS, payload: t.documents});
};
failDocs = dispatch => {
  dispatch({type: GET_CUSTOM_DOCS_FAIL});
};

export const reScanDoc = (data, token, id, name, navi, dataLink) => {
  console.log(data, token, id);

  const form = new FormData();
  form.append('name', name);
  form.append('documents', {
    uri: data,
    type: 'document/pdf',
    name: 'document.pdf',
  });

  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(
      `${SERVER_URL}/api/docs/${id}/rescan`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: form,
      },
    )
      .then(response => checkReDoc(response.json(), dispatch, navi, dataLink))
      .catch(e => failReDoc(dispatch, e));
  };
};

checkReDoc = async (response, dispatch, navi, dataLink) => {
  const t = await response;
  console.log('...', response);
  // if (t.message == 'rescan successfull') {
  Toast.show('Document Uploaded', Toast.SHORT);
  dispatch({type: DOC_SUCCESSFUL});

  await RNFS.unlink(dataLink)
    .then(() => {
      console.log('FILE DELETED');
    })
    // `unlink` will throw an error, if the item to unlink does not exist
    .catch(err => {
      console.log(err.message);
    });
  navi.navigate('congratulation');
  // } else {
  //   Toast.show('Please rescan carefully', Toast.SHORT);
  //   dispatch({type: DOC_FAIL});
  // }
};
failReDoc = (dispatch, e) => {
  alert(e);
  Toast.show('Image upload fail', Toast.SHORT);
  dispatch({type: DOC_FAIL});
};

export const policeVerify = (data, token, code, navi, dataLink) => {
  const form = new FormData();
  form.append('name', 'Police Verification');
  form.append('type', 'police');
  form.append('code', code);
  form.append('police_confirmation', {
    uri: data,
    type: 'document/pdf',
    name: 'document.pdf',
  });

  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(`${SERVER_URL}/api/docs`, {
      method: 'POST',
      headers: {
        Authorization: `Bearer ${token}`,
        'Content-Type': 'multipart/form-data',
        Accept: 'application/json',
      },
      body: form,
    })
      .then(response => checkPolice(response.json(), dispatch, navi, dataLink))
      .catch(e => failPolice(dispatch, e));
  };
};
checkPolice = async (response, dispatch, navi, dataLink) => {
  const t = await response;
  console.log(t);
  if (t.message == 'insert successfull') {
    Toast.show('Document Uploaded', Toast.SHORT);
    dispatch({type: DOC_SUCCESSFUL});
    await RNFS.unlink(dataLink)
      .then(() => {
        console.log('FILE DELETED');
      })
      // `unlink` will throw an error, if the item to unlink does not exist
      .catch(err => {
        console.log(err.message);
      });
    navi.navigate('verifier');
  } else {
    Toast.show('Image upload fail', Toast.SHORT);
    dispatch({type: DOC_FAIL});
  }
};

failPolice = (dispatch, e) => {
  alert(e);
  Toast.show('Image upload fail', Toast.SHORT);
  dispatch({type: DOC_FAIL});
};

export const acceptDocument = token => {

  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/documents/request/list`,
      {
        method: 'GET',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => checkAcceptDoc(response.json(), dispatch))
      .catch(e => failAcceptDoc(dispatch, e));
  };
};

checkAcceptDoc = async (response, dispatch) => {
  const r = await response;

  dispatch({type: ACCEPT_DOC_LIST, payload: r});
};
failAcceptDoc = (dispatch, e) => {
  alert(e);
};

export const confirmDocument = (user_id, id, token) => {
  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    dispatch({type: CHECK_REQUEST_DOCUMENT, payload: id});

    await fetch(
      `${SERVER_URL}/api/user/${user_id}/documents/${id}/accept`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => helo(dispatch, response.json(), token, id))
      .catch(e => fail(e, dispatch, id, token));
  };
};
const helo = async (dispatch, respnse, token, id) => {
  const res = await respnse;
  // dispatch(acceptDocument(token))
  dispatch({type: CHECK_REQUEST_DOCUMENT_SUCCESSFUL, payload: id});
};
const fail = (e, dispatch, id, token) => {
  alert(e);
  dispatch({type: CHECK_REQUEST_DOCUMENT_FAIL, payload: id});
  // dispatch(acceptDocument(token));
};
export const rejectDocument = (user_id, id, token) => {
  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    dispatch({type: CHECK_REQUEST_DOCUMENT, payload: id});

    await fetch(
      `${SERVER_URL}/api/user/${user_id}/documents/${id}/reject`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => helo(dispatch, response.json(), token, id))
      .catch(e => fail(e, dispatch, id, token));
  };
};

export const acceptDocList = token => {
  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/documents/accept/list`,
      {
        method: 'GET',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => acceptdoclist(dispatch, response.json(), token))
      .catch(e => faillist(e, dispatch, token));
  };
};
acceptdoclist = async (dispatch, response, token) => {
  const res = await response;
  dispatch({type: ACCEPTEDDOCS, payload: res});
};
faillist = (e, dispatch, token) => {};

export const startUpdate = (user_id, id, token) => {
  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/user/${user_id}/documents/${id}/favorite`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => starResponse(dispatch, response.json(), token))
      .catch(e => alert(e));
  };
};

starResponse = (dispatch, response, token) => {
  dispatch(acceptDocList(token));
};
export const rejectedDocuments = token => {
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/documents/reject/list`,
      {
        method: 'GET',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(response => rejDocList(dispatch, response.json(), token))
      .catch(e => alert(e));
  };
};

rejDocList = async (dispatch, response, token) => {
  const res = await response;
  dispatch({type: REJECTED_DOCUMENT_LIST, payload: res});
};

export const sendFirebaseId = (id, token, fcmToken) => {
  token = token.replace(/^"(.*)"$/, '$1');
  const form = new FormData();
  form.append('registration_id', fcmToken);
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/user/${id}/registrationid`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },

        body: form,
      },
    )
      .then(res => {
        checkR(res.json());
      })
      .catch(e => alert(e));
  };
};
checkR = async res => {
  const gg = await res;
};
export const scanStampDocs = id => {
  return {
    type: SCAN_STAMP_DOCS,
    payload: id,
  };
};

export const scanStampDocsSent = id => {
  return {
    type: SCAN_STAMP_DOCS_SENT,
    payload: id,
  };
};

export const policeSelectedDocs = (id, name, token) => {
  token = token.replace(/^"(.*)"$/, '$1');
  const form = new FormData();
  form.append('text', name);
  form.append('document_id', id);
  return async dispatch => {
    await fetch(
      `${SERVER_URL}/api/police/selected/docs/send`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
        body: form,
      },
    )
      .then(res => {
        cc(res);
      })
      .catch(e => alert(e));
  };
};
const cc = async res => {
  const er = await res.json();
  console.log(er);
};
export const policeVerificationPin = (pin, token, navi) => {
  return async dispatch => {
    dispatch({type: CHECK_DOC});
    await fetch(`https://demo.xcieo.com/roza/iami2/public/api/document/pin`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        accept: 'application/json',
        Authorization: `Bearer ${token}`,
      },
      body: JSON.stringify({
        pin: pin,
      }),
    })
      .then(res => {
        ccPin(res, token, dispatch, navi);
      })
      .catch(e => {
        alert(e);
        dispatch({type: DOC_FAIL});
      });
  };
};
const ccPin = async (res, token, dispatch, navi) => {
  const r = await res.json();
  console.log('.....', r);
  if (r.status == 400) {
    console.log('ok');
    dispatch(policeVerificationDocuments(token, navi));
  }
  if (r.status == 200) {
    dispatch(policeVerificationDocuments(token, navi));
  }
};
export const policeVerificationDocuments = (token, navi) => {
  return async dispatch => {
    await fetch(
      `https://demo.xcieo.com/roza/iami2/public/api/police/selected/docs/send/update`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(res => {
        policeverificationDocs(res, dispatch, navi);
      })
      .catch(e => alert(e));
  };
};
policeverificationDocs = async (res, dispatch, navi) => {
  const c = await res.json();
  console.log(c);
  dispatch({type: DOC_SUCCESSFUL});
  navi.navigate('verifier');
};
export const paymentVerified = async (token, navi) => {
  token = token.replace(/^"(.*)"$/, '$1');
  return async dispatch => {
    await fetch(
      `https://demo.xcieo.com/roza/iami2/public/api/api/user/payment/verified`,
      {
        method: 'POST',
        headers: {
          'content-type': 'multipart/form-data;',
          accept: 'application/json',
          Authorization: `Bearer ${token}`,
        },
      },
    )
      .then(res => donepayment(res, navi))
      .catch(e => alert(e));
  };
};
const donepayment = async (res, navi) => {
  const t = await res.json();
  console.log(t);
  navi.navigate('splashScreen2');
};
