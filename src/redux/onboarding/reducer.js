import {
  CLEAR_ERROR,
  REQUEST_FAILED,
  OTP_REQUEST,
  OTP_REQUEST_SUCCESS,
  VERIFY_OTP,
  VERIFY_OTP_SUCCESS,
  UPLOAD_DOCUMENT,
  UPLOAD_DOCUMENT_SUCCESS,
  UPLOAD_VIDEO,
  UPLOAD_VIDEO_SUCCESS,
  HANDLE_UNAUTHORIZED,
  UPLOAD_PHOTO,
  UPLOAD_PHOTO_SUCCESS,
  UPDATE_PERSONAL_INFO,
  UPDATE_PERSONAL_INFO_SUCCESS,
  POSTAL_VERIFICATION,
  POSTAL_VERIFICATION_SUCCESS,
  REQUEST_PAYMENT_URL,
  REQUEST_PAYMENT_URL_SUCCESS,
  GET_USER,
  GET_USER_SUCCESS,
} from './types';


const INTIAL_STATE = {
  loading: false,
  user_id: null,
  token: null,
  step: 0,
  error: null,

  personalInfo: null,
  payment_url: null,
  user_data: null,
};

export default (state = INTIAL_STATE, action) => {

  const { type, payload } = action;

  switch (type) {
    
    case CLEAR_ERROR:
      return { ...state, error: null };

    case OTP_REQUEST:
      return { ...state, loading: true };
    case OTP_REQUEST_SUCCESS:
      return { ...state, loading: false, user_id: payload };
    
    case VERIFY_OTP:
      return { ...state, loading: true };
    case VERIFY_OTP_SUCCESS:
      return { ...state, loading: false, token: payload.token, step: payload.step };

    case UPLOAD_DOCUMENT:
      return { ...state, loading: true }
    case UPLOAD_DOCUMENT_SUCCESS:
      return { ...state, loading: false, step: payload.step }


    case UPLOAD_VIDEO:
      return { ...state, loading: true }
    case UPLOAD_VIDEO_SUCCESS:
      return { ...state, loading: false, step: payload.step }

    case UPLOAD_PHOTO:
      return { ...state, loading: true }
    case UPLOAD_PHOTO_SUCCESS:
      return { ...state, loading: false, step: payload.step }

    case UPDATE_PERSONAL_INFO:
      return { ...state, loading: true }
    case UPDATE_PERSONAL_INFO_SUCCESS:
      return { ...state, loading: false, step: payload.step, personalInfo: payload.personal_info }

    case POSTAL_VERIFICATION:
      return { ...state, loading: true }
    case POSTAL_VERIFICATION_SUCCESS:
      return { ...state, loading: false, step: payload.step }


    case REQUEST_PAYMENT_URL:
      return { ...state, loading: true }
    case REQUEST_PAYMENT_URL_SUCCESS:
      return { ...state, loading: false, payment_url: payload }
 
    case GET_USER:
      return { ...state, loading: true }
    case GET_USER_SUCCESS:
      return { ...state, loading: false, user_data: payload, step: payload.steps }


    case REQUEST_FAILED:
      return { ...state, loading: false, error: payload };
    
    case HANDLE_UNAUTHORIZED: 
      return INTIAL_STATE;

    default:
      return state;
  }
};
