
export const CLEAR_ERROR = 'CLEAR_ERROR';
export const REQUEST_FAILED = 'REQUEST_FAILED';
export const HANDLE_REQUEST_ERROR = 'HANDLE_REQUEST_ERROR';
export const NAVIGATE_STEP = 'NAVIGATE_STEP';

export const OTP_REQUEST = 'OTP_REQUEST';
export const OTP_REQUEST_SUCCESS = 'OTP_REQUEST_SUCCESS';
export const OTP_REQUEST_FAILED = 'OTP_REQUEST_FAILED';

export const VERIFY_OTP = 'VERIFY_OTP';
export const VERIFY_OTP_SUCCESS = 'VERIFY_OTP_SUCCESS';
export const VERIFY_OTP_FAILED = 'VERIFY_OTP_FAILED';

export const UPLOAD_DOCUMENT = 'UPLOAD_DOCUMENT';
export const UPLOAD_DOCUMENT_SUCCESS = 'UPLOAD_DOCUMENT_SUCCESS';
export const UPLOAD_DOCUMENT_FAILED = 'UPLOAD_DOCUMENT_FAILED';

export const UPLOAD_VIDEO = 'UPLOAD_VIDEO';
export const UPLOAD_VIDEO_SUCCESS = 'UPLOAD_VIDEO_SUCCESS';

export const UPLOAD_PHOTO = 'UPLOAD_PHOTO';
export const UPLOAD_PHOTO_SUCCESS = 'UPLOAD_PHOTO_SUCCESS';

export const UPDATE_PERSONAL_INFO = 'UPDATE_PERSONAL_INFO';
export const UPDATE_PERSONAL_INFO_SUCCESS = 'UPDATE_PERSONAL_INFO_SUCCESS';

export const POSTAL_VERIFICATION = 'POSTAL_VERIFICATION';
export const POSTAL_VERIFICATION_SUCCESS = 'POSTAL_VERIFICATION_SUCCESS';

export const REQUEST_PAYMENT_URL = 'REQUEST_PAYMENT_URL';
export const REQUEST_PAYMENT_URL_SUCCESS = 'REQUEST_PAYMENT_URL_SUCCESS';

export const GET_USER = 'GET_USER';
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS';

export const HANDLE_UNAUTHORIZED = 'HANDLE_UNAUTHORIZED';