import { call, put, takeEvery, select, takeLatest } from 'redux-saga/effects'

import {
  OTP_REQUEST,
  VERIFY_OTP,
  NAVIGATE_STEP,
  UPLOAD_DOCUMENT,
  UPLOAD_VIDEO,
  UPLOAD_PHOTO,
  UPDATE_PERSONAL_INFO,
  POSTAL_VERIFICATION,
  REQUEST_PAYMENT_URL,
  GET_USER,

  HANDLE_REQUEST_ERROR,
} from './types'

import { Api } from './api'
import * as Actions from './actions';

import * as RootNavigation from './../../../RootNavigation.js';

const getUserId = state => state.onboarding.user_id;
const getToken = state => state.onboarding.token;

function* requestOtp(action) {  
   try {
    const phoneNo = action.payload.replace(/\s+/g, '');
      const user = yield call(Api.register, phoneNo);
      yield put(Actions.otpReqSuccess(user.id))
      RootNavigation.navigate('Otp', {phoneNo: action.payload})
   } catch (e) {
      yield put(Actions.requestFailed(e.message))
   }
}

function* verifyOTP(action) {  
   try {

      const userId = yield select(getUserId);

      const data = yield call(Api.verifyOtp, userId, action.payload);

      yield put(Actions.verifyOTPSuccess({token: data.token, step: data.steps}))

      yield put(Actions.navigateToStep());
   } catch (e) {
      yield put(Actions.requestFailed(e.message))
   }
}

function* uploadDoc(action) {  
   try {

      const {pdf, image} = action.payload;
      const token = yield select(getToken);

      const data = yield call(Api.uploadDoc, token, pdf, image);
      
      yield put(Actions.uploadDocSuccess({step: data.steps}))

      yield put(Actions.navigateToStep());
    } catch (e) {
      yield put(Actions.handleRequestError(e.code, e.message));
   }
}

function* uploadVideo(action) {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.uploadVideo, token, action.payload);
      
      yield put(Actions.uploadVideoSuccess({step: data.steps}))
      yield put(Actions.navigateToStep());

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* uploadPhoto(action) {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.uploadPhoto, token, action.payload);
      
      yield put(Actions.uploadPhotoSuccess({step: data.steps}))
      yield put(Actions.navigateToStep());

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* updatePersonalInfo(action) {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.updatePersonalInfo, token, action.payload);
      
      yield put(Actions.updatePersonalInfoSuccess({step: data.steps, personal_info: action.payload}))
      yield put(Actions.navigateToStep());

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* postalVerification(action) {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.postalVerification, token, action.payload);
      
      yield put(Actions.postalVerificationSuccess({step: data.steps}))
      yield getUser();

    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* requestPaymentUrl(action) {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.requestPaymentUrl, token, action.payload);
      
      if (data.url && typeof data.url == 'string' ) {
        yield put(Actions.requestPaymentUrlSuccess(data.url))
      } else {
        yield put(Actions.handleRequestError(400, "Unable to request for this payment method"));
      }


    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}

function* getUser() {  
   try {

      const token = yield select(getToken);

      const data = yield call(Api.getUser, token);
      
      yield put(Actions.getUserSuccess(data))
      
      yield put(Actions.navigateToStep());


    } catch (error) {
      yield put(Actions.handleRequestError(error.code, error.message));
   }
}



function* onboardingSaga() {
  yield takeLatest(OTP_REQUEST, requestOtp);
  yield takeLatest(VERIFY_OTP, verifyOTP);
  yield takeLatest(NAVIGATE_STEP, navigateToStep);
  yield takeLatest(UPLOAD_DOCUMENT, uploadDoc);
  yield takeLatest(UPLOAD_VIDEO, uploadVideo);
  yield takeLatest(UPLOAD_PHOTO, uploadPhoto);
  yield takeLatest(UPDATE_PERSONAL_INFO, updatePersonalInfo);
  yield takeLatest(POSTAL_VERIFICATION, postalVerification);
  yield takeLatest(REQUEST_PAYMENT_URL, requestPaymentUrl);
  yield takeLatest(GET_USER, getUser);

  yield takeLatest(HANDLE_REQUEST_ERROR, handleRequestError);
}



function* handleRequestError(action) {  

  const {code, message} = action.payload;
  
  switch (code) {
    case 401:
      yield put(Actions.handleLogout());
      yield put(Actions.navigateToStep());
      break;
    case 400:
      yield put(Actions.requestFailed(message))
      break;
    default:
      yield put(Actions.requestFailed(message))
      break;
  }

}


function* navigateToStep() {

   var step = yield select((state) => state.onboarding.step);

    console.log(typeof step);
    console.log(step);

   if (typeof step == 'number') {
     step += "";
   }

   switch (step) {
     case '1':
       RootNavigation.navigate('Kyc');
       break;
     case '2':
       RootNavigation.navigate('Video');
       break;
     case '3':
       RootNavigation.navigate('Photo');
       break;
     case '4':
       RootNavigation.navigate('PersonalInfo');
       break;
     case '5':
       RootNavigation.navigate('PaymentVerification');
       break;
     case '6':
      //  RootNavigation.navigate('WelcomeScreen');
       break;
     default:
      //  RootNavigation.navigate('WelcomeScreen');
       break;
   }
 }

export default onboardingSaga;
