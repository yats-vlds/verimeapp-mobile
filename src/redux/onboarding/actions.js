import {
  CLEAR_ERROR,
  REQUEST_FAILED,
  HANDLE_REQUEST_ERROR,
  NAVIGATE_STEP,
  
  OTP_REQUEST,
  OTP_REQUEST_SUCCESS,
  OTP_REQUEST_FAILED,

  VERIFY_OTP,
  VERIFY_OTP_SUCCESS,

  UPLOAD_DOCUMENT,
  UPLOAD_DOCUMENT_SUCCESS,
  
  UPLOAD_PHOTO,
  UPLOAD_PHOTO_SUCCESS,
  
  UPLOAD_VIDEO,
  UPLOAD_VIDEO_SUCCESS,
  
  UPDATE_PERSONAL_INFO,
  UPDATE_PERSONAL_INFO_SUCCESS,

  POSTAL_VERIFICATION,
  POSTAL_VERIFICATION_SUCCESS,

  REQUEST_PAYMENT_URL,
  REQUEST_PAYMENT_URL_SUCCESS,

  GET_USER,
  GET_USER_SUCCESS,

  
  HANDLE_UNAUTHORIZED,
} from './types';

export const clearError = () => ({
  type: CLEAR_ERROR
})
export const handleRequestError = (code, message) => ({
  type: HANDLE_REQUEST_ERROR,
  payload: {
    code,
    message
  }
})
export const requestFailed = (error) => ({
  type: REQUEST_FAILED,
  payload: error
})
export const handleLogout = () => ({
  type: HANDLE_UNAUTHORIZED
})
export const getOtp = (phoneNo) => ({
  type: OTP_REQUEST,
  payload: phoneNo
})
export const otpReqSuccess = (data) => ({
  type: OTP_REQUEST_SUCCESS,
  payload: data
})
export const otpReqFailed = (error) => ({
  type: OTP_REQUEST_FAILED,
  payload: error
})


export const verifyOTP = (otp) => ({
  type: VERIFY_OTP,
  payload: otp
})
export const verifyOTPSuccess = (data) => ({
  type: VERIFY_OTP_SUCCESS,
  payload: data
})
export const navigateToStep = () => ({
  type: NAVIGATE_STEP
})


export const uploadDoc = (pdf, image) => ({
  type: UPLOAD_DOCUMENT,
  payload: {
    pdf, 
    image
  }
})
export const uploadDocSuccess = (data) => ({
  type: UPLOAD_DOCUMENT_SUCCESS,
  payload: data
})


export const uploadVideo = (video) => ({
  type: UPLOAD_VIDEO,
  payload: video
})
export const uploadVideoSuccess = (step) => ({
  type: UPLOAD_VIDEO_SUCCESS,
  payload: step
})


export const uploadPhoto = (photo) => ({
  type: UPLOAD_PHOTO,
  payload: photo
})
export const uploadPhotoSuccess = (step) => ({
  type: UPLOAD_PHOTO_SUCCESS,
  payload: step
})

export const updatePersonalInfo = (info) => ({
  type: UPDATE_PERSONAL_INFO,
  payload: info
})
export const updatePersonalInfoSuccess = (data) => ({
  type: UPDATE_PERSONAL_INFO_SUCCESS,
  payload: data
})


export const postalVerification = (info) => ({
  type: POSTAL_VERIFICATION,
  payload: info
})
export const postalVerificationSuccess = (data) => ({
  type: POSTAL_VERIFICATION_SUCCESS,
  payload: data
})

export const requestPaymentUrl = () => ({
  type: REQUEST_PAYMENT_URL
})
export const requestPaymentUrlSuccess = (data) => ({
  type: REQUEST_PAYMENT_URL_SUCCESS,
  payload: data
})

export const getUser = () => ({
  type: GET_USER
})
export const getUserSuccess = (data) => ({
  type: GET_USER_SUCCESS,
  payload: data
})

