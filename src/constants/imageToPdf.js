import React from 'react';
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native';
import RNImageToPdf from 'react-native-image-to-pdf';
import PDFLib, {PDFDocument, PDFPage} from 'react-native-pdf-lib';
var RNFS = require('react-native-fs');
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';

import Constants from '../Constants';

const SERVER_URL = Constants.SERVER_URL;

const finalImage = '';
var logo = `${SERVER_URL}/assets/img/red_cropped.png`;

export const ImageToPdf = async (image, number) => {
  var checkWidth = '';
  var checkHeight = '';
  var documentName = `Document${Math.floor(Math.random() * 1000 + 1)}.pdf`;
  Image.getSize(logo, (width, height) => {});
  await Image.getSize(image, (width, height) => {
    (checkWidth = width), (checkHeight = height);
  });

  console.log(image);
  var t = '';
  let sourcePathGreenLogo =
    '/storage/emulated/0/Android/data/com.verime/files' + '/greenLogo.png';
  let sourcePathRedLogo =
    '/storage/emulated/0/Android/data/com.verime/files' + '/redLogo.png';
  let sourcePath = `/storage/emulated/0/Android/data/com.verime/files' + '/${documentName}`;
  const docsDir = await PDFLib.getDocumentsDirectory();
  //   this.setState({pdfReady: `file://${pdf.filePath}`});
  const redLogoPath = `${docsDir}/redLogo.png`;
  const greenLogoPath = `${docsDir}/greenLogo.png`;
  // const imagePath = `${docsDir}/greenLogo.png`;
  const pdfPath = `${docsDir}/${documentName}`;
  await ImageResizer.createResizedImage(
    image,
    checkWidth,
    checkHeight,
    'PNG',
    100,
  ).then(async response => {
    t = response.path;
  });
  try {
    const options = {
      imagePaths: [t],
      name: documentName,
      maxSize: {
        // optional maximum image dimension - larger images will be resized
        width: checkWidth,
        height: checkHeight,
      },
      quality: 1.0,
      // optional compression paramter
    };

    await RNImageToPdf.createPDFbyImages(options).then(async pdf => {
      sourcePath = pdf.filePath;
    });
    await ImgToBase64.getBase64String(
      `${SERVER_URL}/assets/img/logo.png`,
    )
      .then(async base64String => {
        await RNFS.writeFile(greenLogoPath, base64String, 'base64');
      })
      .catch(err => alert(err));

    await ImgToBase64.getBase64String(
      `${SERVER_URL}/assets/img/red_cropped.png`,
    )
      .then(async base64String => {
        await RNFS.writeFile(redLogoPath, base64String, 'base64');
      })
      .catch(err => alert(err));

    await RNFS.moveFile(sourcePath, pdfPath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    await RNFS.copyFile(redLogoPath, sourcePathRedLogo)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    await RNFS.copyFile(greenLogoPath, sourcePathGreenLogo)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    const page1 = PDFPage.modify(0).drawImage(greenLogoPath, 'png', {
      x: checkWidth - 200,
      y: 30,
      width: 161,
      height: 60,
    });
    const page2 = PDFPage.modify(0).drawImage(redLogoPath, 'png', {
      x: checkWidth - 200,
      y: 100,
      width: 161,
      height: 60,
    });
    var modifiedPath;
    await PDFDocument.modify(pdfPath)
      .modifyPages(page1, page2)
      .write() // Returns a promise that resolves with the PDF's path
      .then(path => {
        modifiedPath = path;
        console.log('PDF modified at: ' + path);
      })
      .catch(e => {
        alert(e);
      });
    await RNFS.moveFile(pdfPath, sourcePath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
  } catch (e) {
    console.log(e);
  }
  return sourcePath;
};


const getImageDimensions = async (image) => {
  var width = '';
  var height = '';

  await Image.getSize(image, (w, h) => {
    width = w;
    height = h;
  });

  return {width, height};
}

export const ImageToPdfOnly = async (image) => {
  var checkWidth = '';
  var checkHeight = '';
  var documentName = `Document${Math.floor(Math.random() * 1000 + 1)}.pdf`;
  
  await Image.getSize(image, (width, height) => {
    (checkWidth = width), (checkHeight = height);
  });

  console.log(image);
  var t = '';
  
  let sourcePath = `/storage/emulated/0/Android/data/com.verime/files' + '/${documentName}`;
  const docsDir = await PDFLib.getDocumentsDirectory();
  
  const pdfPath = `${docsDir}/${documentName}`;
  await ImageResizer.createResizedImage(
    image,
    checkWidth,
    checkHeight,
    'PNG',
    100,
  ).then(async response => {
    t = response.path;
  });
  try {
    const options = {
      imagePaths: [t],
      name: documentName,
      maxSize: {
        // optional maximum image dimension - larger images will be resized
        width: checkWidth,
        height: checkHeight,
      },
      quality: 1.0,
      // optional compression paramter
    };

    await RNImageToPdf.createPDFbyImages(options).then(async pdf => {
      sourcePath = pdf.filePath;
    });

    await RNFS.moveFile(sourcePath, pdfPath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    var modifiedPath;
    await PDFDocument.modify(pdfPath)

      .write() // Returns a promise that resolves with the PDF's path
      .then(path => {
        modifiedPath = path;
        console.log('PDF modified at: ' + path);
      })
      .catch(e => {
        alert(e);
      });
    await RNFS.moveFile(pdfPath, sourcePath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
  } catch (e) {
    console.log(e);
  }
  return sourcePath;
};

export const ImageToPdfGreen = async (image) => {
  var checkWidth = '';
  var checkHeight = '';
  var documentName = `Document${Math.floor(Math.random() * 1000 + 1)}.pdf`;
  Image.getSize(logo, (width, height) => {});
  await Image.getSize(image, (width, height) => {
    (checkWidth = width), (checkHeight = height);
  });

  console.log(image);
  var t = '';
  let sourcePathGreenLogo =
    '/storage/emulated/0/Android/data/com.verime/files' + '/greenLogo.png';
    
  let sourcePath = `/storage/emulated/0/Android/data/com.verime/files' + '/${documentName}`;
  const docsDir = await PDFLib.getDocumentsDirectory();

  const greenLogoPath = `${docsDir}/greenLogo.png`;

  const pdfPath = `${docsDir}/${documentName}`;
  await ImageResizer.createResizedImage(
    image,
    checkWidth,
    checkHeight,
    'PNG',
    100,
  ).then(async response => {
    t = response.path;
  });
  try {
    const options = {
      imagePaths: [t],
      name: documentName,
      maxSize: {
        // optional maximum image dimension - larger images will be resized
        width: checkWidth,
        height: checkHeight,
      },
      quality: 1.0,
      // optional compression paramter
    };

    await RNImageToPdf.createPDFbyImages(options).then(async pdf => {
      sourcePath = pdf.filePath;
    });
    await ImgToBase64.getBase64String(
      `${SERVER_URL}/assets/img/logo.png`,
    )
      .then(async base64String => {
        await RNFS.writeFile(greenLogoPath, base64String, 'base64');
      })
      .catch(err => alert(err));

    await RNFS.moveFile(sourcePath, pdfPath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    await RNFS.copyFile(greenLogoPath, sourcePathGreenLogo)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
    const page1 = PDFPage.modify(0).drawImage(greenLogoPath, 'png', {
      x: checkWidth - 200,
      y: 30,
      width: 161,
      height: 60,
    });
    var modifiedPath;
    await PDFDocument.modify(pdfPath)
      .modifyPages(page1)
      .write() // Returns a promise that resolves with the PDF's path
      .then(path => {
        modifiedPath = path;
        console.log('PDF modified at: ' + path);
      })
      .catch(e => {
        alert(e);
      });
    await RNFS.moveFile(pdfPath, sourcePath)
      .then(result => {})
      .catch(err => {
        console.log(err);
      });
  } catch (e) {
    console.log(e);
  }
  return sourcePath;
};
