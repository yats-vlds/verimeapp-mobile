import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  BackHandler,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {RNCamera} from 'react-native-camera';

export default class camera extends React.Component {
  render() {
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        style={styles.preview}
        type={RNCamera.Constants.Type.back}
        flashMode={RNCamera.Constants.FlashMode.off}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        androidRecordAudioPermissionOptions={{
          title: 'Permission to use audio recording',
          message: 'We need your permission to use your audio',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        onGoogleVisionBarcodesDetected={({barcodes}) => {
          // console.log(barcodes);
        }}
      />
    );
  }
}
const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    alignItems: 'center',
    height: 50,
    width: '70%',
  },
});
