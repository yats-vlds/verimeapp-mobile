import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default class notificationPage extends React.Component {
  render() {
    return (
      <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View>
            <Text style={{fontSize: 18}}>Hey!</Text>
          </View>
          <View style={{marginTop: 20}}>
            <Text style={{fontSize: 18}}>
              Standard bank has requested access to{' '}
            </Text>
            <Text style={{fontSize: 18, alignSelf: 'center'}}>
              your ID,Passport,renteal agreement
            </Text>
          </View>
          <View style={{marginTop: 20}}>
            <TouchableOpacity
              style={{
                backgroundColor: '#148567',
                height: 50,
                width: 250,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, color: 'white'}}>Refuse access</Text>
            </TouchableOpacity>
          </View>
          <View style={{marginTop: 20}}>
            <TouchableOpacity
              style={{
                backgroundColor: '#148567',
                height: 50,
                width: 250,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, color: 'white'}}>Grant access</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
