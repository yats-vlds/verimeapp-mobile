import React from 'react';
import {View, Text, TouchableOpacity, ScrollView, Alert} from 'react-native';
import DocumentRequest from './documentRequest';
import HomeHeader from './HeaderHome';
import {connect} from 'react-redux';
import * as Progress from 'react-native-progress';
import Image from 'react-native-image-progress';
import {} from '../redux/actions/index';
import firebase from '@react-native-firebase/app';
import AsyncStorage from '@react-native-community/async-storage';
class review extends React.Component {
  // componentDidMount() {

  //     this.checkTokenStatus();

  // }
  // async checkTokenStatus() {
  //     await this.checkPermission();
  //      // Register all listener for notification
  //    await  this.createNotificationListeners();
  // }
  // async checkPermission() {
  //     const enabled = await firebase.messaging().hasPermission();
  //     // If Premission granted proceed towards token fetch
  //     if (enabled) {
  //       this.getToken();
  //     } else {
  //       // If permission hasn’t been granted to our app, request user in requestPermission method.
  //       this.requestPermission();
  //     }
  //   }

  //   async getToken() {
  //     let fcmToken = await AsyncStorage.getItem('fcmToken');

  //     if (!fcmToken) {
  //       fcmToken = await firebase.messaging().getToken();
  //       if (fcmToken) {

  //         // user has a device token
  //         await AsyncStorage.setItem('fcmToken', fcmToken);
  //       }
  //     }
  //   }

  //   async requestPermission() {
  //     try {
  //       await firebase.messaging().requestPermission();
  //       // User has authorised
  //       this.getToken();
  //     } catch (error) {
  //       // User has rejected permissions
  //       console.log('permission rejected');
  //     }
  //   }

  //   async createNotificationListeners() {

  //     // This listener triggered when notification has been received in foreground
  //     this.notificationListener = firebase.notifications().onNotification((notification) => {
  //       const { title, body } = notification;
  //       this.displayNotification(title, body);
  //     });

  //     // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
  //     this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {

  //       const { title, body } = notificationOpen.notification;
  //       this.displayNotification(title, body);
  //     });

  //     // This listener triggered when app is closed and we click,tapped and opened notification
  //     const notificationOpen = await firebase.notifications().getInitialNotification();
  //     if (notificationOpen) {
  //       const { title, body } = notificationOpen.notification;
  //       this.displayNotification(title, body);
  //     }
  //   }

  //   displayNotification(title, body) {
  //     // we display notification in alert box with title and body
  //     Alert.alert(
  //       title, body,
  //       [
  //         { text: 'Ok', onPress: () => console.log('ok pressed') },
  //       ],
  //       { cancelable: false },
  //     );
  //   }

  render() {
    return (
      <View style={{flex: 1}}>
        <HomeHeader />

        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={{marginTop: 20, alignItems: 'center'}}>
            <Text
              style={{fontSize: 20, alignSelf: 'center', fontWeight: 'bold'}}>
              Requests
            </Text>
          </View>
          <View style={{marginBottom: 20}}>
            <DocumentRequest navi={this.props.navigation} />
            {/* <DocumentRequest />
                        <DocumentRequest />
                        <DocumentRequest />
                        <DocumentRequest />
                        <DocumentRequest />
                        <DocumentRequest /> */}
          </View>
        </ScrollView>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {},
)(review);
