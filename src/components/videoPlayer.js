import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import Video from 'react-native-video';
import HomeHeader from './HeaderHome';
import {connect} from 'react-redux';
import {Add} from '../redux/actions/index';

class videoPlayer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pause: true,
    };
  }
  componentDidMount() {
    this.props.Add();
  }
  changeMe() {
    if (this.state.pause) {
      this.setState({pause: false});
    } else {
      this.setState({pause: true});
    }
  }
  render() {
    return (
      <ScrollView>
        <HomeHeader />
        <View
          style={{
            alignItems: 'center',
          }}>
          <Video
            source={{
              uri: this.props.addition.vid ? this.props.addition.vid : null,
            }} // Can be a URL or a local file.
            ref={ref => {
              this.player = ref;
            }} // Store reference
            onBuffer={this.onBuffer} // Callback when remote video is buffering
            onError={this.videoError}
            paused={this.state.pause}
            resizeMode="cover"
            onEnd={() => this.setState({pause: true})}
            // Callback when video cannot be loaded
            style={styles.backgroundVideo}
          />
          <View>
            <TouchableOpacity
              onPress={() => this.changeMe()}
              style={{
                backgroundColor: '#148567',
                height: 50,
                width: 250,
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 20,
              }}>
              {this.state.pause ? (
                <Text style={{fontSize: 20, color: 'white'}}>Play</Text>
              ) : (
                <Text style={{fontSize: 20, color: 'white'}}>Pause</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}
var styles = StyleSheet.create({
  backgroundVideo: {
    height: 400,
    width: 350,
    marginTop: 10,
    backgroundColor: 'black',
  },
});
const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {Add},
)(videoPlayer);
