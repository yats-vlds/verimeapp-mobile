import React from 'react';
import {View, Text, TouchableOpacity, Im} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import FastImage from 'react-native-fast-image';
import * as Progress from 'react-native-progress';
import Image from 'react-native-image-progress';

import {useNavigation} from '@react-navigation/native'
import { useSelector } from 'react-redux';

export default () => {
  const data = useSelector((state) => state.user.user_data);
  var image;
  var first_name;
  if (data) {
    first_name = data.first_name;
    image = data.image;
  }
  const navigation = useNavigation();
  return (
    <View
      style={{backgroundColor: '#148567', height: 130, alignItems: 'center'}}>
      <View style={{width: '85%'}}>
        <View style={{justifyContent: 'center', marginTop: '3%'}}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={{height: 50, width: 50, justifyContent: 'center'}}>
            <Icon name="long-arrow-left" size={15} color="white" />
          </TouchableOpacity>
          <View>
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                marginBottom: '20%',
              }}>
              <View>
                {first_name ? (
                  <Text
                    style={{
                      fontSize: 25,
                      color: 'black',
                      alignSelf: 'center',
                      fontWeight: 'bold',
                    }}>
                    {first_name}
                  </Text>
                ) : null}
              </View>
              {image ? (
                <View style={{alignItems: 'flex-end'}}>
                  <Image
                    source={{uri: image}}
                    indicator={Progress.Pie}
                    indicatorProps={{
                      size: 80,
                      borderWidth: 0,
                      color: 'white',

                      unfilledColor: '#148567',
                    }}
                    style={{
                      borderRadius: 50,
                      width: 70,
                      height: 70,
                      borderWidth: 1,
                      overflow: 'hidden',
                      backgroundColor: '#148567',
                      borderColor: '#148567',
                      borderWidth: 2,
                    }}
                  />
                </View>
              ) : (
                <Progress.Pie size={30} progress={0.4} indeterminate={true} />
              )}
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}
