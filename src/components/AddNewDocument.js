import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ActivityIndicator,
} from 'react-native';
import Header from './Header';
import RNPickerSelect from 'react-native-picker-select';
import {connect} from 'react-redux';
import {uploadCustomDoc} from '@redux/user/actions';
import Toast from 'react-native-simple-toast';

class AddNewDocument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pickerText: '',
      customName: '',
    };
  }
  render() {
    return (
      <View
        style={{
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <Header value={this.props.navigation} title="Add New Document" />

        <View style={{marginTop: 100, width: '100%'}}>
          <View style={{marginTop: 10, marginBottom: 10}}>
            <Text style={{fontSize: 16}}>Document Type</Text>
          </View>

          <RNPickerSelect
            onValueChange={value => this.setState({pickerText: value})}
            items={[
              {label: 'PASSPORT', value: 'PASSPORT'},
              {label: 'DRIVING', value: 'DRIVING'},
              {label: 'CUSTOM', value: 'CUSTOM'},
            ]}
            style={{inputAndroidContainer: {backgroundColor: 'red'}}}
          />
        </View>
        {this.state.pickerText == 'CUSTOM' ? (
          <View style={{marginTop: 20}}>
            <View style={{marginBottom: 5}}>
              <Text style={{fontSize: 16}}>Please Specify</Text>
            </View>
            <TextInput
              onChangeText={text => {
                this.setState({customName: text});
              }}
              style={{
                width: 300,
                height: 50,
                borderWidth: 1,
                borderColor: '#dedede',
              }}
            />
          </View>
        ) : null}

        {this.props.loading ? (
          <ActivityIndicator
            size="large"
            color="black"
            style={{marginTop: 25}}
          />
        ) : (
          <TouchableOpacity
            onPress={() => {
              if (
                this.state.pickerText == 'CUSTOM' &&
                this.state.customName != ''
              ) {
                this.props.uploadCustomDoc(
                  `file://${this.props.route.params.uri}`,
                  this.state.customName,
                );
              } else if (
                this.state.pickerText != '' &&
                this.state.pickerText !== 'CUSTOM'
              ) {
                this.props.uploadCustomDoc(
                  `file://${this.props.route.params.uri}`,
                  this.state.pickerText,
                );
              } else {
                Toast.show('Field is missing', Toast.SHORT);
              }
            }}
            style={{
              height: 50,
              width: 300,
              backgroundColor: '#148567',
              marginTop: 50,
              justifyContent: 'center',
              borderRadius: 10,
            }}>
            <Text style={{fontSize: 20, textAlign: 'center', color: 'white'}}>
              DONE
            </Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
const mapStateToProps = state => {
  const {loading} = state.user;
  return {
    loading,
  };
};
export default connect(
  mapStateToProps,
  {uploadCustomDoc},
)(AddNewDocument);
