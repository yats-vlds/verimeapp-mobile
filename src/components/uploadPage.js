import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  Image,
  ScrollView,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import RNPickerSelect from 'react-native-picker-select';
import {connect} from 'react-redux';
import {documentCall} from '../redux/actions/index';
import Header from './Header';
import HomeHeader from './HeaderHome';
class uploadPage extends React.Component {
  componentDidMount() {
    this.props.documentCall();
  }
  render() {
    return (
      <ScrollView>
        <HomeHeader />
        <View style={{alignItems: 'center', marginTop: 15}}>
          <Text style={{fontSize: 20, fontWeight: 'bold'}}>RE-Scan</Text>
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          <Image
            source={this.props.addition.docs}
            style={{height: 350, width: 250}}
          />

          <View style={{marginTop: 20, alignItems: 'center'}}>
            <View style={{flexDirection: 'row'}}>
              <Icon name="copy" size={30} color="#18ad86" />
              <View style={{marginLeft: '5%', alignItems: 'center'}}>
                <Text style={{fontSize: 16, textAlign: 'center'}}>
                  Do not take photo of screen
                </Text>
                <Text style={{fontSize: 16}}>Take scan in well lit area </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: '3%',
                justifyContent: 'flex-start',
              }}>
              <Icon name="info-circle" size={30} color="#18ad86" />
              <View
                style={{
                  marginLeft: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 16}}>Take scan in well lit area </Text>
              </View>
            </View>
          </View>

          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('DocumentScan')}
            style={{
              marginTop: 10,
              width: 200,
              height: 50,
              backgroundColor: '#148567',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
            }}>
            <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
              Upload
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {documentCall},
)(uploadPage);
