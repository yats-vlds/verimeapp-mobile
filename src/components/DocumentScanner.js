import React, {PureComponent} from 'react';
import {
  ActivityIndicator,
  Image,
  Dimensions,
  Platform,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import Header from '../constants/header';
import Pdf from 'react-native-pdf';
import DocScannerOverlay from "./DocScannerOverlay";


import RNImageToPdf from 'react-native-image-to-pdf';
import PDFLib, {PDFDocument, PDFPage} from 'react-native-pdf-lib';


var RNFS = require('react-native-fs');
import ImgToBase64 from 'react-native-image-base64';
import ImageResizer from 'react-native-image-resizer';
import {ImageToPdf} from '../constants/imageToPdf';

import {sendDoc} from '../Redux/Action/index';
var RNFS = require('react-native-fs');


import Constants from '../Constants';

const SERVER_URL = Constants.SERVER_URL;

var checkImage = '';

class DocumentScanner extends PureComponent {

  constructor(props) {
    super(props);
    this.state = {
        viewScanLayout: false,
        data: ''
    };

  }


  render() {
    if (this.state.viewScanLayout) {
      return (
          <View style={{flex: 1}}>
            <DocScannerOverlay style={{flex: 1}} setData={async (data) => {
              
              this.setState({viewScanLayout: false});
              
              checkImage = await ImageToPdf(data.croppedImage, 1);

              this.setState({
                data: data.croppedImage,
              });

              await this.props.navigation.navigate('DocumentReviewToUpload', {
                imageUri: checkImage,
              });
              
              }} />
          </View>
      );
    }
    return (
      <View style={{flex: 1}}>


        {this.state.data != undefined && this.state.data != '' ? (
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexGrow: 1,
            }}>
            <View
              style={{
                flexDirection: 'column',
                marginLeft: 40,
                marginRight: 40,
                flex: 1,
                marginTop: 40,
                marginBottom: 40,
              }}>
              <View style={{width: '100%', flexDirection: 'row', flex: 1}}>
                <View style={{width: '90%'}}>
                  <Header value={this.props.navigation} title="Scan" />
                </View>
              </View>
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginTop: 10,
                  marginBottom: 10,
                  flex: 1,
                }}>
                <Pdf
                  source={{uri: this.state.data, cache: true}}
                  style={{
                    width: 400,
                    height: 400,
                    overflow: 'hidden',
                    flex: 1,
                  }}
                />
              </View>
              <View style={{alignItems: 'flex-start', marginLeft: '15%'}} />
              <View style={{marginTop: 25, alignItems: 'center'}}>
                <View style={{flexDirection: 'row'}}>
                  <Icon name="copy" size={30} color="#18ad86" />
                  <View style={{marginLeft: '5%', alignItems: 'center'}}>
                    <Text style={{fontSize: 16, textAlign: 'center'}}>
                      Do not take photo of screens
                    </Text>
                    {/* <Text style={{fontSize: 16}}>Take scan in well lit area </Text> */}
                  </View>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 25,
                    justifyContent: 'flex-start',
                  }}>
                  <Icon name="info-circle" size={30} color="#18ad86" />
                  <View
                    style={{
                      marginLeft: '5%',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Text style={{fontSize: 16}}>
                      Take scan in well lit area{' '}
                    </Text>
                  </View>
                </View>
              </View>
              {this.props.loading ? (
                <ActivityIndicator size="large" color="black" />
              ) : (
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 25,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={async () => {
                      await RNFS.unlink(this.state.data)
                        .then(() => {
                          console.log('FILE DELETED');
                        })
                        // `unlink` will throw an error, if the item to unlink does not exist
                        .catch(err => {
                          console.log(err.message);
                        });
                      this.setState({viewScanLayout: true});
                    }}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>Re-Scan</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      this.props.sendDoc(
                        `file://${this.state.data}`,
                        this.state.data,
                      );
                    }}
                    style={styles.start2}>
                    <Text style={{color: 'white', fontSize: 20}}>Next</Text>
                  </TouchableOpacity>
                </View>
              )}

              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'column',
                  flexGrow: 1,
                  justifyContent: 'flex-end',
                  marginTop: 25,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 50,
                      backgroundColor: '#63d4b6',
                      opacity: 0.7,
                    }}
                  />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleOne} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                </View>
              </View>
            </View>
          </ScrollView>
        ) : (
          <View
            style={{flex: 1}}
            onLayout={event => {
              // This is used to detect multi tasking mode on iOS/iPad
              // Camera use is not allowed
              if (this.state.didLoadInitialLayout && Platform.OS === 'ios') {
                const screenWidth = Dimensions.get('screen').width;
                const isMultiTasking =
                  Math.round(event.nativeEvent.layout.width) <
                  Math.round(screenWidth);
                if (isMultiTasking) {
                  this.setState({isMultiTasking: true, loadingCamera: false});
                } else {
                  this.setState({isMultiTasking: false});
                }
              } else {
                this.setState({didLoadInitialLayout: true});
              }
            }}>
            <StatusBar
              backgroundColor="black"
              barStyle="light-content"
              hidden={Platform.OS !== 'android'}
            />
            {/* {this.renderCameraView()} */}
            <ScrollView
              contentContainerStyle={{
                flexGrow: 1,
              }}
              showsVerticalScrollIndicator={false}>
              <View
                style={{
                  flexDirection: 'column',
                  marginLeft: 40,
                  marginRight: 40,
                  flex: 1,
                  marginTop: 40,
                  marginBottom: 40,
                }}>
                <View style={{width: '100%', flexDirection: 'row'}}>
                  <View style={{width: '90%'}}>
                    <Header value={this.props.navigation} title="Scan" />
                  </View>
                </View>

                <View style={{alignItems: 'center', marginTop: 25, flex: 1}}>
                  <TouchableOpacity
                    onPress={() => {
                      console.log(this.state.viewScanLayout);
                      this.setState({viewScanLayout: true})
                    }}
                    style={{
                      height: 330,
                      width: '100%',
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1,
                      backgroundColor: '#dedede',
                    }}>
                    <Text style={{fontSize: 20, color: 'grey'}}>
                      Tap To Scan Document
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={{marginTop: 25, alignItems: 'center'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="copy" size={30} color="#18ad86" />
                    <View style={{marginLeft: '5%', alignItems: 'center'}}>
                      <Text style={{fontSize: 16, textAlign: 'center'}}>
                        Do not take photo of screen
                      </Text>
                      {/* <Text style={{fontSize: 16}}>Take photo in well lit area </Text> */}
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: '3%',
                      justifyContent: 'flex-start',
                    }}>
                    <Icon name="info-circle" size={30} color="#18ad86" />
                    <View
                      style={{
                        marginLeft: '5%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 16}}>
                        Take photo in well lit area{' '}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={{alignItems: 'center', marginTop: 25}}>
                  <TouchableOpacity
                    // disabled={this.state.detected.length > 0 ? false : true}

                    onPress={() => this.setState({viewScanLayout: true})}
                    // style={this.state.detected.length > 0 ? styles.start : styles.disable}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>Scan</Text>
                  </TouchableOpacity>
                </View>

                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'column',
                    flexGrow: 1,
                    justifyContent: 'flex-end',
                    marginTop: 25,
                  }}>
                  <View style={{flexDirection: 'row'}}>
                    <View
                      style={{
                        height: 10,
                        width: 10,
                        borderRadius: 50,
                        backgroundColor: '#63d4b6',
                        opacity: 0.7,
                      }}
                    />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleOne} />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleDesign} />
                    <View style={styles.singleDesign} />
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        )}
      </View>
    );
  }


  ImageToPdff = async image => {
    var checkWidth = '';
    var checkHeight = '';

    await Image.getSize(image, (width, height) => {
      (checkWidth = width), (checkHeight = height);
    });

    var t = '';
    let sourcePathRedLogo =
      '/storage/emulated/0/Android/data/com.verime/files' + '/redLogo.png';
    let sourcePath =
      '/storage/emulated/0/Android/data/com.verime/files' + '/Document.pdf';
    const docsDir = await PDFLib.getDocumentsDirectory();
    //   this.setState({pdfReady: `file://${pdf.filePath}`});
    const redLogoPath = `${docsDir}/redLogo.png`;
    // const imagePath = `${docsDir}/greenLogo.png`;
    const pdfPath = `${docsDir}/Document.pdf`;
    await ImageResizer.createResizedImage(
      image,
      checkWidth,
      checkHeight,
      'PNG',
      100,
    ).then(async response => {
      t = response.path;
    });
    try {
      const options = {
        imagePaths: [t],
        name: 'Document.pdf',
        maxSize: {
          // optional maximum image dimension - larger images will be resized
          width: checkWidth,
          height: checkHeight,
        },
        quality: 1.0,
        // optional compression paramter
      };

      await RNImageToPdf.createPDFbyImages(options).then(async pdf => {
        sourcePath = pdf.filePath;
      });
      await ImgToBase64.getBase64String(
        `${SERVER_URL}/assets/img/red_cropped.png`,
      )
        .then(async base64String => {
          await RNFS.writeFile(redLogoPath, base64String, 'base64');
        })
        .catch(err => alert(err));
      await RNFS.moveFile(sourcePath, pdfPath)
        .then(result => {})
        .catch(err => {
          console.log(err);
        });
      await RNFS.copyFile(redLogoPath, sourcePathRedLogo)
        .then(result => {})
        .catch(err => {
          console.log(err);
        });
      const page1 = PDFPage.modify(0).drawImage(redLogoPath, 'png', {
        x: checkWidth - 200,
        y: checkHeight - 1200,
        width: 100,
        height: 100,
      });
      await PDFDocument.modify(pdfPath)
        .modifyPages(page1)
        .write() // Returns a promise that resolves with the PDF's path
        .then(path => {
          console.log('PDF modified at: ' + path);
        })
        .catch(e => {
          alert(e);
        });
      await RNFS.moveFile(pdfPath, sourcePath)
        .then(result => {})
        .catch(err => {
          console.log(err);
        });
    } catch (e) {
      console.log(e);
    }
  };
}


const styles = StyleSheet.create({
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});

const mapStateToProps = state => {
  return {
    user_id: state.onboarding.user_id,
    token: state.onboarding.token,
    loading: state.onboarding.loading,
  };
};

export default connect(
  mapStateToProps,
  {sendDoc},
  (stateProps, dispatchProps, ownProps) => ({
    ...ownProps,
    ...stateProps,
    ...dispatchProps,
    sendDoc(data, dataLink) {
      dispatchProps.sendDoc(data, stateProps.token, stateProps.user_id, ownProps.navigation, dataLink)
    }
  })
)(DocumentScanner);
