import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {
  StyleSheet,
  Text,
  Image,
  View,
  TouchableOpacity,
  Switch,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import {copilot, walkthroughable, CopilotStep} from 'react-native-copilot';

const WalkthroughableText = walkthroughable(Text);
const WalkthroughableImage = walkthroughable(Image);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    paddingTop: 40,
  },
  title: {
    fontSize: 18,
    textAlign: 'center',
    height: 95,
    color: 'blue',
    overflow: 'visible',
    alignSelf: 'center',
  },
  midTitle: {
    fontSize: 18,
    textAlign: 'center',
    height: 130,
    color: '#4fa0b3',
  },

  middleView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginTop: 50,
  },
  bottomView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginTop: 50,
  },
  lastbottomView: {
    alignItems: 'center',
    justifyContent: 'center',
    height: 50,
    marginTop: 50,
  },
  button: {
    backgroundColor: '#2980b9',
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
    width: 200,
    justifyContent: 'center',
    backgroundColor: '#148567',
  },
  button2: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    alignItems: 'center',
    borderRadius: 10,
    height: 50,
    width: 200,
    justifyContent: 'center',
    backgroundColor: '#148567',
  },
  buttonText: {
    color: 'white',
    fontSize: 16,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  tabItem1: {
    textAlign: 'center',
    color: '#db1dc2',
    height: 65,
  },
  tabItem2: {
    textAlign: 'center',
    color: '#ed0c2e',
    height: 65,
  },
  activeSwitchContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: 20,
    alignItems: 'center',
    paddingHorizontal: 40,
  },
});

class App extends Component {
  static propTypes = {
    start: PropTypes.func.isRequired,
    copilotEvents: PropTypes.shape({
      on: PropTypes.func.isRequired,
    }).isRequired,
  };

  state = {
    secondStepActive: true,
  };

  componentDidMount() {
    this.props.copilotEvents.on('stepChange', this.handleStepChange);
    this.props.start();
  }

  handleStepChange = step => {};

  render() {
    return (
      <View style={styles.container}>
        <View style={{width: '90%'}}>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 25, fontWeight: 'bold', marginBottom: 5}}>
              {' '}
              Instructions
            </Text>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <CopilotStep text="Hey! First Step!" order={1} name="openApp">
              <WalkthroughableText style={styles.title}>
                {
                  'Open the Scanner by clicking \n on Scanner button  that is down there.'
                }
              </WalkthroughableText>
            </CopilotStep>
          </View>
          <View style={styles.middleView}>
            <CopilotStep
              active={this.state.secondStepActive}
              text="Second Step!"
              order={2}
              name="secondText">
              <WalkthroughableText
                style={styles.midTitle}

                // source={require('../images/icons/scannerImage.jpg')}
                // style={styles.profilePhoto}
              >
                {' '}
                {
                  'Carefully scan the Image. Scanner will automatically draw a rectangle on detected document and you will have to press your default camera button. '
                }{' '}
              </WalkthroughableText>
            </CopilotStep>
          </View>
          <View style={styles.bottomView}>
            <CopilotStep text="Third Step!" order={3} name="thirdText">
              <WalkthroughableText style={styles.tabItem1}>
                {'Do not scan a screen'}
              </WalkthroughableText>
            </CopilotStep>
          </View>
          {/* <View style={styles.lastbottomView}>
                        <CopilotStep text="Careful!" order={4} name="fourthText">
                            <WalkthroughableText style={styles.tabItem2}>
                                {'Donot take Screen Shot!!!'}
                            </WalkthroughableText>
                        </CopilotStep>


                    </View> */}
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => this.props.start()}>
              <Text style={styles.buttonText}>Start Tutorial Again</Text>
            </TouchableOpacity>
          </View>
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 50,
            }}>
            <TouchableOpacity
              style={styles.button2}
              onPress={() => this.props.navigation.navigate('scanner')}>
              <Text style={styles.buttonText}>Open Scanner</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}

export default copilot({
  animated: true, // Can be true or false
  overlay: 'svg', // Can be either view or svg
})(App);
