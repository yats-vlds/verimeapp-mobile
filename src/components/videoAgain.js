import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
  BackHandler,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {beforeGoBack, videoSave, callNumber} from '../Redux/Action/index';
import Header from '../constants/header';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
var RNFS = require('react-native-fs');
class videoAgain extends React.Component {
  constructor(props) {
    super(props);
    // this.willFocusSubscription = this.willFocusSubscription.bind(this);
    this.state = {
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      camera: 'back',
      data: '',
      autoFocusPoint: {
        normalized: {x: 0.5, y: 0.5}, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
          x: Dimensions.get('window').width * 0.5 - 32,
          y: Dimensions.get('window').height * 0.5 - 32,
        },
      },
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        quality: RNCamera.Constants.VideoQuality['720p'],
        mirrorVideo: false,
      },
      isRecording: false,
      canDetectFaces: false,
      canDetectText: false,
      canDetectBarcode: false,
      faces: [],
      textBlocks: [],
      barcodes: [],
      PP: true,
      retake: false,
      record: false,
      stopNext: false,
    };
  }

  callMe = () => {
    const str = this.state.data;
    const fileName = str.substr(str.indexOf('Camera') + 7, str.indexOf('.mp4'));
    const {config, fs, android} = RNFetchBlob;
    const path = fs.dirs.CacheDir + '/Camera/' + fileName;

    // console.log('filename',fileName)
    RNFS.copyFile(
      this.state.data,
      RNFS.PicturesDirectoryPath + '/Videos/' + fileName,
    ).then(
      () => {},
      error => {
        // console.log("CopyFile fail for video: " + error);
      },
    );
  };
  // componentWillMount() {

  //     this.willFocusSubscription = this.props.navigation.addListener(
  //         'willFocus',
  //         () => {
  //             alert('back')
  //         }
  //     );
  // }

  // componentWillUnmount() {
  //     this.willFocusSubscription.remove();
  // }
  // willFocusSubscription() {
  //     alert('call me')

  // }
  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: 'column',
            marginTop: 25,
            marginLeft: 25,
            marginRight: 25,
          }}>
          <Header gob={null} value={this.props.navigation} title="Video" />
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 170,
            }}>
            {this.state.data && this.state.retake ? (
              <View style={{alignItems: 'center'}}>
                <Video
                  source={{uri: this.state.data}} // Can be a URL or a local file.
                  ref={ref => {
                    this.player = ref;
                  }} // Store reference
                  onBuffer={this.onBuffer} // Callback when remote video is buffering
                  onError={this.videoError}
                  paused={this.state.PP}
                  resizeMode="cover"
                  // poster="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"
                  // Callback when video cannot be loaded
                  style={styles.backgroundVideo}
                />
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.PP) {
                      this.setState({PP: false});
                    } else {
                      this.setState({PP: true});
                    }
                  }}
                  style={styles.videoButton}>
                  <Text style={{color: 'white'}}>Play</Text>
                </TouchableOpacity>
              </View>
            ) : (
              <RNCamera
                ref={ref => {
                  this.camera = ref;
                }}
                style={styles.preview}
                type={RNCamera.Constants.Type.back}
                flashMode={RNCamera.Constants.FlashMode.off}
                androidCameraPermissionOptions={{
                  title: 'Permission to use camera',
                  message: 'We need your permission to use your camera',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
                androidRecordAudioPermissionOptions={{
                  title: 'Permission to use audio recording',
                  message: 'We need your permission to use your audio',
                  buttonPositive: 'Ok',
                  buttonNegative: 'Cancel',
                }}
                onGoogleVisionBarcodesDetected={({barcodes}) => {
                  // console.log(barcodes);
                }}
              />
            )}
            {this.state.data ? (
              <View style={{marginTop: 20}}>
                <View
                  style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                  <Icon name="copy" size={30} color="#18ad86" />
                  <View style={{marginLeft: '5%', alignItems: 'center'}}>
                    <Text style={{fontSize: 16}}>
                      Do not take photo of screen
                    </Text>
                    <Text style={{fontSize: 16}}>
                      Take scan in well lit area{' '}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: '3%'}}>
                  <Icon name="info-circle" size={30} color="#18ad86" />
                  <View style={{marginLeft: '5%', justifyContent: 'center'}}>
                    <Text style={{fontSize: 16, textAlign: 'center'}}>
                      Take scan in well lit area{' '}
                    </Text>
                  </View>
                </View>
              </View>
            ) : (
              <View style={{marginTop: 170}}>
                <View
                  style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
                  <Icon name="copy" size={30} color="#18ad86" />
                  <View style={{marginLeft: '5%', alignItems: 'center'}}>
                    <Text style={{fontSize: 16}}>
                      Do not take photo of screen
                    </Text>
                    <Text style={{fontSize: 16}}>
                      Take scan in well lit area{' '}
                    </Text>
                  </View>
                </View>
                <View style={{flexDirection: 'row', marginTop: '3%'}}>
                  <Icon name="info-circle" size={30} color="#18ad86" />
                  <View style={{marginLeft: '5%', justifyContent: 'center'}}>
                    <Text style={{fontSize: 16, textAlign: 'center'}}>
                      Take scan in well lit area{' '}
                    </Text>
                  </View>
                </View>
              </View>
            )}
          </View>
          <View style={{alignItems: 'center', marginTop: '3%'}}>
            {!this.state.record ? (
              <View>
                <TouchableOpacity
                  onPress={this.takeVideo.bind(this)}
                  style={styles.start}>
                  <Text style={{color: 'white', fontSize: 20}}>
                    Record Video
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View>
                {!this.state.stopNext ? (
                  <View>
                    <TouchableOpacity
                      onPress={() => {
                        this.stopVideo();
                        this.setState({stopNext: true, retake: true});
                      }}
                      style={styles.start}>
                      <Text style={{color: 'white', fontSize: 20}}>
                        Stop Video
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View
                    style={{
                      alignItems: 'center',
                      flexDirection: 'row',
                      justifyContent: 'center',
                    }}>
                    <TouchableOpacity
                      onPress={() => {
                        this.takeVideo();
                        this.setState({
                          retake: false,
                          data: '',
                          record: false,
                          stopNext: false,
                          PP: true,
                        });
                      }}
                      style={styles.start}>
                      <Text style={{color: 'white', fontSize: 20}}>
                        Retake Video
                      </Text>
                    </TouchableOpacity>
                    {this.state.data ? (
                      <TouchableOpacity
                        onPress={() => {
                          this.props.videoSave(this.state.data);
                          this.setState({check: false});
                          this.props.navigation.navigate('photo', {
                            camera: this.state.camera,
                          });
                        }}
                        style={styles.start2}>
                        <Text style={{color: 'white', fontSize: 20}}>Next</Text>
                      </TouchableOpacity>
                    ) : null}
                  </View>
                )}
              </View>
            )}
          </View>
          <View
            style={{
              alignItems: 'center',
              flexDirection: 'column',
              marginTop: 30,
              marginBottom: 30,
            }}>
            <View style={{flexDirection: 'row'}}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  borderRadius: 50,
                  backgroundColor: '#63d4b6',
                  opacity: 0.7,
                }}
              />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleOne} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
            </View>
          </View>
          {/* {this.state.data ? <Video source={{uri: this.state.data}}
                    // Can be a URL or a local file.
                    ref={(ref) => {
                        this.player = ref
                    }}                                      // Store reference
                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                    onError={this.videoError}               // Callback when video cannot be loaded
                    style={styles.backgroundVideo} /> : null} */}
          {this.state.data ? this.callMe() : null}
        </View>
      </ScrollView>
    );
  }

  takeVideo = async () => {
    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && !isRecording) {
      try {
        const promise = this.camera.recordAsync(this.state.recordOptions);

        if (promise) {
          const data = await promise;

          // console.log('takeVideo',data.uri);
          this.setState({isRecording: false, data: data.uri});
        } else {
          return <ActivityIndicator size="small" color="black" />;
        }
      } catch (e) {
        console.error(e);
      }
    }
  };
  stopVideo = async () => {
    await this.camera.stopRecording();

    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && isRecording) {
      try {
        this.setState({isRecording: false});
      } catch (e) {
        console.error(e);
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    alignItems: 'center',
    height: 50,
    width: '70%',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: '1%',
  },
  singleOne: {
    height: 10,
    width: 35,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: '1%',
  },
  backgroundVideo: {
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
    marginTop: -170,
    height: 400,
    width: 300,
    backgroundColor: 'black',
  },
  videoButton: {
    height: 50,
    width: 100,
    backgroundColor: '#04c491',
    marginTop: 10,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
const mapStateToProps = state => {
  return {
    callBack: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {beforeGoBack, videoSave},
)(video);
