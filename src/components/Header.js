import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { useNavigation } from '@react-navigation/native';

export default Header = ({title}) => {
  var navigation = useNavigation();

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity
        onPress={() => {
          navigation.goBack();
        }}
        style={{height: 30, width: 15, justifyContent: 'center'}}>
        <Icon name="long-arrow-left" size={15} color="black" />
      </TouchableOpacity>

      <View style={{flex: 1, width: '100%'}}>
        <Text style={{fontSize: 25, fontWeight: 'bold', textAlign: 'center'}}>
          {title}
        </Text>
      </View>
    </View>
  );
}
