import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
  BackHandler,
  ScrollView,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {beforeGoBack} from '../redux/actions/index';
import Header from './Header';
import Camera from '../constants/camera';
class video extends React.Component {
  constructor(props) {
    super(props);
    // this.willFocusSubscription = this.willFocusSubscription.bind(this);
    this.state = {
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      camera: 'back',
      autoFocusPoint: {
        normalized: {x: 0.5, y: 0.5}, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
          x: Dimensions.get('window').width * 0.5 - 32,
          y: Dimensions.get('window').height * 0.5 - 32,
          record: false,
          stopNext: false,
        },
      },
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        quality: RNCamera.Constants.VideoQuality['720p'],
        mirrorVideo: false,
      },
      isRecording: false,
      canDetectFaces: false,
      canDetectText: false,
      canDetectBarcode: false,
      faces: [],
      textBlocks: [],
      barcodes: [],
    };
  }
  componentDidMount() {
    this.props.beforeGoBack();
  }
  // componentWillMount() {

  //     this.willFocusSubscription = this.props.navigation.addListener(
  //         'willFocus',
  //         () => {
  //             alert('back')
  //         }
  //     );
  // }

  // componentWillUnmount() {
  //     this.willFocusSubscription.remove();
  // }
  // willFocusSubscription() {
  //     alert('call me')T

  // }
  render() {
    return (
      <ScrollView
        style={{
          flexDirection: 'column',
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
        }}
        showsHorizontalScrollIndicator={false}>
        <Header value={this.props.navigation} title="Video" />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 170,
          }}>
          <Camera />
          <View style={{marginTop: 170}}>
            <View style={{flexDirection: 'row', justifyContent: 'flex-start'}}>
              <Icon name="copy" size={30} color="#18ad86" />
              <View style={{marginLeft: '5%', alignItems: 'center'}}>
                <Text style={{fontSize: 16}}>Do not take photo of screen</Text>
                <Text style={{fontSize: 16}}>Take scan in well lit area </Text>
              </View>
            </View>
            <View style={{flexDirection: 'row', marginTop: '3%'}}>
              <Icon name="info-circle" size={30} color="#18ad86" />
              <View style={{marginLeft: '5%', justifyContent: 'center'}}>
                <Text style={{fontSize: 16, textAlign: 'center'}}>
                  Take scan in well lit area{' '}
                </Text>
              </View>
            </View>
          </View>
        </View>
        <View style={{alignItems: 'center', marginTop: '3%'}}>
          {!this.state.record ? (
            <View>
              <TouchableOpacity
                onPress={this.takeVideo.bind(this)}
                style={styles.start}>
                <Text style={{color: 'white', fontSize: 20}}>Record Video</Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View>
              {!this.state.stopNext ? (
                <View>
                  <TouchableOpacity
                    onPress={() => {
                      this.stopVideo();
                      this.setState({stopNext: true});
                    }}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>
                      Stop Video
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : (
                <View
                  style={{
                    alignItems: 'center',
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.takeVideo();
                      this.setState({stopNext: false});
                    }}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>
                      Retake Video
                    </Text>
                  </TouchableOpacity>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({check: false});
                      this.props.navigation.navigate('photo', {
                        camera: this.state.camera,
                      });
                    }}
                    style={styles.start2}>
                    <Text style={{color: 'white', fontSize: 20}}>Next</Text>
                  </TouchableOpacity>
                </View>
              )}
            </View>
          )}
        </View>
        <View
          style={{
            alignItems: 'center',
            flexDirection: 'column',
            marginTop: 30,
            marginBottom: 30,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                height: 10,
                width: 10,
                borderRadius: 50,
                backgroundColor: '#63d4b6',
                opacity: 0.7,
              }}
            />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleOne} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
          </View>
        </View>
      </ScrollView>
    );
  }

  takeVideo = async () => {
    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && !isRecording) {
      try {
        const promise = this.camera.recordAsync(this.state.recordOptions);

        if (promise) {
          this.setState({isRecording: true});
          const data = await promise;
        }
      } catch (e) {
        console.error(e);
      }
    }
  };
  stopVideo = async () => {
    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && isRecording) {
      try {
        const promise = this.camera.stopRecording();

        this.setState({isRecording: false});
      } catch (e) {
        console.error(e);
      }
    }
  };
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    alignItems: 'center',
    height: 50,
    width: '70%',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: '1%',
  },
  singleOne: {
    height: 10,
    width: 35,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: '1%',
  },
});
const mapStateToProps = state => {
  return {
    callBack: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {beforeGoBack},
)(video);
