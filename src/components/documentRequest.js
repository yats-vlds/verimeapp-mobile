import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {ActivityIndicator} from 'react-native-paper';
// import Image from 'react-native-image-progress';
import {
  acceptDocument,
  rejectDocument,
  favouriteColor,
} from '../redux/actions/index';

import {getDocumentRequests, acceptDocumentRequest, rejectDocumentRequest} from '@redux/user/actions'


class documentRequest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: '',
      favorite: false,
    };
  }
  // shouldComponentUpdate(nextProps) {
  //     console.log('....................................??????',this.props.docRequestList.length,nextProps.docRequestList.length)
  //     return this.props.docRequestList.length !== nextProps.docRequestList.length;

  // }
  componentDidMount = async () => {
    
    this.props.getDocumentRequests()

  };

  //     this.docFunction();
  //     setInterval(() => {
  //         this.docFunction();
  //     }, 9000);

  // }
  // docFunction=async()=>{
  //     await  this.props.Add();
  //     await this.props.callToken();
  //     await this.props.acceptDocument(this.props.addition.call_token)

  // }

  render() {
    if (!((this.props.docRequestList && this.props.docRequestList.length) || (this.props.docSignRequestList && this.props.docSignRequestList))) {
      return (
        <View style={{flex: 1}}>
          <View
            style={{
              marginTop: 200,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 20, color: '#bebebe'}}>No Requests</Text>
          </View>
        </View>
      );
    }

    return (
      <View>
        {this.props.docRequestList.map(item => {
            return (
              <View 
                key={item.id}
              >
                <View
                  key={item.id}
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 5,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 7},
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 3,
                  }}>
                  <View key={item.id}>
                    <View style={{flexDirection: 'row', padding: 10}}>
                      <View style={{marginRight: 10}}>
                        <Image
                          resizeMode="cover"
                          source={require('../images/icons/Ame-Splash-Screen.jpg')}
                          // indicator={Progress.Pie}
                          // indicatorProps={{
                          //     size: 75,

                          //     borderWidth: 0,
                          //     color: 'white',

                          //     unfilledColor: '#148567'
                          // }}

                          style={{
                            borderRadius: 50,
                            height: 50,
                            width: 50,
                          }}
                        />
                      </View>

                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          flex: 1,
                          justifyContent: 'space-between',
                        }}>
                        <View>
                          <Text style={{fontSize: 13}}>{`User ${
                            item.phone
                          } wants to access your document of ${
                            item.doc_name
                          }`}</Text>
                        </View>
                        {/* <View>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.favouriteColor(item.id);
                          }}>
                          <Icon
                            size={25}
                            name="star"
                            color={item.favorite ? '#148567' : '#aeaeae'}
                          />
                        </TouchableOpacity>
                      </View> */}
                      </View>
                      {item.loader ? (
                        <ActivityIndicator
                          size="small"
                          color="black"
                          animating={true}
                          style={{marginRight: 10}}
                        />
                      ) : (
                        <View
                          style={{
                            flexDirection: 'row',
                          }}>
                          <TouchableOpacity
                            onPress={() => {
                              this.setState({
                                id: item.id,
                                favorite: !this.state.favorite,
                              });
                              this.props.acceptDocumentRequest(
                                item.id,
                              );
                            }}
                            // style={{width: 120,height: 40,marginRight: 15,backgroundColor: '#148567',justifyContent: 'center',alignItems: 'center',borderRadius: 5}}
                          >
                            <Icon
                              name="check-circle"
                              color={this.state.favorite ? 'green' : '#239143'}
                              size={30}
                              style={{alignSelf: 'center', opacity: 0.4}}
                            />
                            <Text style={{fontSize: 15}}>Accept</Text>
                          </TouchableOpacity>
                          <TouchableOpacity
                            style={{marginLeft: 10}}
                            onPress={() =>
                              this.props.rejectDocumentRequest(
                                item.id,
                              )
                            }

                            // style={{width: 120,height: 40,backgroundColor: '#148567',justifyContent: 'center',alignItems: 'center',borderRadius: 5}}
                          >
                            <Icon
                              name="times-circle"
                              color="#cc3325"
                              size={30}
                              style={{alignSelf: 'center', opacity: 0.5}}
                            />
                            <Text style={{fontSize: 15, alignSelf: 'center'}}>
                              Decline
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}
                    </View>
                    <View />
                  </View>
                </View>
                      </View>
            );
          })}

        {this.props.docSignRequestList && this.props.docSignRequestList.length && 
          this.props.docSignRequestList.map((item) => {
            return (
              <View
                  key={item.id}
                  style={{
                    marginLeft: 10,
                    marginRight: 10,
                    marginTop: 5,
                    borderRadius: 5,
                    shadowColor: '#000',
                    shadowOffset: {width: 0, height: 7},
                    shadowOpacity: 0.25,
                    shadowRadius: 3.84,
                    elevation: 3,
                    backgroundColor: '#cef5d9',
                  }}>
                  <View key={item.id}>
                    <View style={{flexDirection: 'row', padding: 10}}>
                      <View style={{marginRight: 10}}>
                        <Image
                          resizeMode="cover"
                          source={require('../images/icons/Ame-Splash-Screen.jpg')}
                          // indicator={Progress.Pie}
                          // indicatorProps={{
                          //     size: 75,

                          //     borderWidth: 0,
                          //     color: 'white',

                          //     unfilledColor: '#148567'
                          // }}

                          style={{
                            borderRadius: 50,
                            height: 50,
                            width: 50,
                          }}
                        />
                      </View>

                      <View
                        style={{
                          flexDirection: 'row',
                          alignItems: 'center',
                          flex: 1,
                          justifyContent: 'space-between',
                        }}>
                        <View>
                          <Text style={{fontSize: 13}}>{`User ${
                            item.phone
                          } wants your signature on document of ${
                            item.doc_name
                          }`}</Text>
                        </View>
                        {/* <View>
                        <TouchableOpacity
                          onPress={() => {
                            this.props.favouriteColor(item.id);
                          }}>
                          <Icon
                            size={25}
                            name="star"
                            color={item.favorite ? '#148567' : '#aeaeae'}
                          />
                        </TouchableOpacity>
                      </View> */}
                      </View>

                      <View
                        style={{
                          flexDirection: 'row',
                        }}>
                        <TouchableOpacity
                          onPress={() =>
                            this.props.navi.navigate('signDocument', {
                              uri: item.documents,
                              item: item
                            })
                          }
                          style={{marginLeft: 10, justifyContent: 'center'}}

                          // style={{width: 120,height: 40,backgroundColor: '#148567',justifyContent: 'center',alignItems: 'center',borderRadius: 5}}
                        >
                          <Image
                            source={require('../images/icons/contract.png')}
                            style={{height: 30, width: 30}}
                          />
                        </TouchableOpacity>
                      </View>
                    </View>
                    <View />
                  </View>
                </View>
            )
          })
        }
      </View>
    );
  }
}

const mapStateToProps = state => {
  const {loading, docRequestList, docSignRequestList} = state.user;
  return {
    addition: state.addReducer,
    loading,
    docRequestList,
    docSignRequestList,
  };
};
export default connect(
  mapStateToProps,
  {
    getDocumentRequests,
    acceptDocumentRequest,
    rejectDocumentRequest,
    favouriteColor,
  },
)(documentRequest);
