import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';

export default class congratulation extends React.Component {
  render() {
    return (
      <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <View style={{justifyContent: 'center', alignItems: 'center'}}>
            <Image
              style={{width: 200, height: 200}}
              source={require('../images/icons/paper.png')}
            />
            <View
              style={{
                marginTop: '3%',
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center'}}>
                Your Document has been
              </Text>
              <Text style={{fontSize: 20, alignSelf: 'center'}}>
                {' '}
                successfully added
              </Text>
            </View>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Documents')}
              style={{
                marginTop: 30,
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Proceed
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
