import React from 'react';
import {View, text, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {RNCamera} from 'react-native-camera';
import moment from 'moment';
var counter = 0;
var runClock;
export default class fullPhotoCam extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      check: false,
      cameraType: 'front',
      detected: [],
      counter: moment()
        .hour(0)
        .minute(0)
        .second(0)
        .format('HH : mm : ss'),
      count: 0,
    };
  }
  takePicture = async () => {
    setInterval(() => {
      this.displayTime();
    }, 1000);
    this.setState({check: true});

    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync();

      this.setState({data: data.uri});
      // console.log('ok',this.state.data)
      // console.log('..............',pp)
    }
  };
  displayTime() {
    this.setState({
      counter: moment()
        .hour(0)
        .minute(0)
        .second(counter++)
        .format('HH : mm : ss'),
    });
  }
  render() {
    return (
      <View style={{flex: 1}}>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={{flex: 1}}
          flashMode={RNCamera.Constants.FlashMode.off}
          onFacesDetected={arg => {
            this.setState({detected: arg.faces});
          }}
          type={this.state.cameraType}
          faceDetectionLandmarks={
            RNCamera.Constants.FaceDetection.Landmarks.all
          }
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          androidRecordAudioPermissionOptions={{
            title: 'Permission to use audio recording',
            message: 'We need your permission to use your audio',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
          onGoogleVisionBarcodesDetected={({barcodes}) => {}}>
          <View>
            <Text>{this.state.counter}</Text>
          </View>
          <View
            style={{
              flexDirection: 'column',
              flex: 1,
            }}>
            <TouchableOpacity
              onPress={() => this.takePicture()}
              style={styles.camerButtonOutline}>
              <View style={styles.camerButton} />
            </TouchableOpacity>
          </View>
        </RNCamera>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  camerButtonOutline: {
    borderColor: 'white',
    borderRadius: 50,
    borderWidth: 3,
    height: 70,
    width: 70,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 35,
  },
  camerButton: {
    backgroundColor: 'white',
    borderRadius: 50,
    flex: 1,
    margin: 3,
  },
});
