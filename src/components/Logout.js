import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
  BackHandler,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {CommonActions} from '@react-navigation/native';
import {connect} from 'react-redux';

import {handleLogout} from '@redux/onboarding/actions';
import { Button } from 'react-native-paper';

class Logout extends React.Component {
  constructor(props) {
    super(props);

  }

  // BackHandler.addEventListener('hardwareBackPress',() => {

  //     console.log('.....................',this.props.navigation);
  //     this.props.navigation.goBack(null)
  //     //  this.props.navigation.dispatch(
  //     //     CommonActions.reset({
  //     //         index: 2,

  //     //         routes: [
  //     //             {name: 'home'},

  //     //         ],
  //     //     })
  //     // )

  // });

  // componentWillMount() {
  //     this.props.navigation.pop(0)
  // }

  // componentWillUnmount() {
  //     BackHandler.removeEventListener('hardwareBackPress',this.handleBackButtonClick);
  // }

  // handleBackButtonClick() {
  //     // this.props.navigation.navigate('home')
  //     navigation.dispatch(
  //         CommonActions.reset({
  //             index: 0,

  //             routes: [
  //                 {name: 'home'},
  //                 {
  //                     name: 'home',

  //                 },
  //             ],
  //         })
  //     );
  // }

  render() {
    // alert(this.props.addition.img)

    // }
    return (
      <View style={{flex: 1}}>
        <Button onPress={() => {this.props.handleLogout()}} >Logout</Button>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    // addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {handleLogout}
)(Logout);
