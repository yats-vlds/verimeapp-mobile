import React from 'react';
import {View, Text} from 'react-native';
import {Badge} from 'react-native-paper';
import {connect} from 'react-redux';

class badgeIcon extends React.Component {
  render() {
    return (
      <View>
        {(this.props.requests && this.props.requests.length > 0) || (this.props.docSignRequestList && this.props.docSignRequestList.length > 0) ? (
          <Badge
            style={{
              marginTop: 5,
              backgroundColor: '#148567',
              position: 'absolute',
              bottom: -20,
              left: 15,
            }}
            size={16}>
            {this.props.requests.length + this.props.docSignRequestList.length}
          </Badge>
        ) : null}
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    requests: state.user.docRequestList,
    docSignRequestList: state.user.docSignRequestList,
  };
};
export default connect(
  mapStateToProps,
  null,
)(badgeIcon);
