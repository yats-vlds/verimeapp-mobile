import React from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  BackHandler,
  ScrollView,
  SafeAreaView,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {NavigationAction} from 'react-navigation';
import {connect} from 'react-redux';
import {beforeGoBack} from '../redux/actions/index';
import Header from './Header';
import Camera from '../constants/camera';
import commisionerPin from './commisionerPin';
class photo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      data: '',
      check: false,
      camera: 'back',
      prevCamera: '',
    };
  }

  render() {
    // console.log(this.props.route.params.camera);

    return (
      <ScrollView
        style={{flexDirection: 'column', marginTop: 25, marginRight: 25}}>
        <Header value={this.props.navigation} title="Police Confirmation" />
        {this.state.data == '' ? (
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 170,
            }}>
            <RNCamera
              ref={ref => {
                this.camera = ref;
              }}
              style={styles.preview}
              type={RNCamera.Constants.Type.back}
              flashMode={RNCamera.Constants.FlashMode.off}
              androidCameraPermissionOptions={{
                title: 'Permission to use camera',
                message: 'We need your permission to use your camera',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              androidRecordAudioPermissionOptions={{
                title: 'Permission to use audio recording',
                message: 'We need your permission to use your audio',
                buttonPositive: 'Ok',
                buttonNegative: 'Cancel',
              }}
              onGoogleVisionBarcodesDetected={({barcodes}) => {
                // console.log(barcodes);
              }}
            />
          </View>
        ) : (
          <View style={{alignItems: 'center'}}>
            <Image
              source={this.state.data}
              resizeMode="contain"
              style={{
                height: 400,
                width: 400,
                overflow: 'hidden',
                marginTop: 10,
                marginBottom: 10,
              }}
            />
            <View style={{alignItems: 'flex-start', marginLeft: '15%'}} />
          </View>
        )}
        {this.state.data == '' ? (
          <View style={{marginTop: 170, alignItems: 'center'}}>
            <View style={{flexDirection: 'row'}}>
              <Icon name="copy" size={30} color="#18ad86" />
              <View style={{marginLeft: '5%', alignItems: 'center'}}>
                <Text style={{fontSize: 16, textAlign: 'center'}}>
                  Do not take photo of screen
                </Text>
                <Text style={{fontSize: 16}}>Take scan in well lit area </Text>
              </View>
            </View>
            <View
              style={{
                flexDirection: 'row',
                marginTop: '3%',
                justifyContent: 'flex-start',
              }}>
              <Icon name="info-circle" size={30} color="#18ad86" />
              <View
                style={{
                  marginLeft: '5%',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Text style={{fontSize: 16}}>Take scan in well lit area </Text>
              </View>
            </View>
          </View>
        ) : null}

        {!this.state.check ? (
          <View style={{alignItems: 'center', marginTop: 15}}>
            <TouchableOpacity
              onPress={this.takePicture.bind(this)}
              style={styles.start}>
              <Text style={{color: 'white', fontSize: 20}}>Save</Text>
            </TouchableOpacity>
          </View>
        ) : (
          <View
            style={{
              marginTop: '3%',
              flexDirection: 'row',
              justifyContent: 'center',
            }}>
            <TouchableOpacity
              onPress={() => {
                this.setState({data: '', check: false});
                this.takePicture.bind(this);
              }}
              style={styles.start}>
              <Text style={{color: 'white', fontSize: 20}}>Re-Scan</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('commisionerPin');
                this.setState({check: false});
              }}
              style={styles.start2}>
              <Text style={{color: 'white', fontSize: 20}}>Next</Text>
            </TouchableOpacity>
          </View>
        )}
        {/* <View style={{alignItems: 'center',flexDirection: 'column',marginTop: 30,marginBottom: 30}}>
                    <View style={{flexDirection: 'row'}}>
                        <View style={{
                            height: 10,width: 10,borderRadius: 50,backgroundColor: '#63d4b6',opacity: 0.7,
                        }}></View><View style={styles.singleDesign}></View><View style={styles.singleDesign}></View><View style={styles.singleDesign}></View><View style={styles.singleOne}></View><View style={styles.singleDesign}></View><View style={styles.singleDesign}></View><View style={styles.singleDesign}></View><View style={styles.singleDesign}></View>
                    </View>
                </View> */}
      </ScrollView>
    );
  }

  takePicture = async () => {
    this.setState({check: true});

    if (this.camera) {
      const options = {quality: 0.5, base64: true};
      const data = await this.camera.takePictureAsync(options);
      this.setState({data: data});
    }
  };
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    alignItems: 'center',
    height: 50,
    width: '70%',
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: '1%',
  },
  singleOne: {
    height: 10,
    width: 35,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: '1%',
  },
});
const mapStateToProps = state => {
  return {
    callBack: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {beforeGoBack},
)(photo);
