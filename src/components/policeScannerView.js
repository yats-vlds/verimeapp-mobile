import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';

import HomeHeader from './HeaderHome';
import Pdf from 'react-native-pdf';

var RNFS = require('react-native-fs');

class policeScannerView extends React.Component {

  render() {
    const source = {
      uri: this.props.route.params.imageUri,
      cache: true,
    };
    return (
      <ScrollView>
        <HomeHeader />

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          {this.props.route.params.imageUri != undefined ? (
            <View>
              <ImageBackground
                source={require('../images/icons/thumbnail.png')}
                style={{
                  width: 350,
                  height: 350,
                  overflow: 'hidden',
                  backgroundColor: '#bebebe',
                }}
                resizeMode="cover">
                <Pdf
                  source={source}
                  style={{
                    width: 400,
                    height: 400,
                    overflow: 'hidden',
                    flex: 1,
                  }}
                />
              </ImageBackground>
            </View>
          ) : null}

          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 30}}>
            <TouchableOpacity
              onPress={async () => {
                await RNFS.unlink(this.props.route.params.imageUri)
                  .then(() => {
                    console.log('FILE DELETED');
                  })
                  // `unlink` will throw an error, if the item to unlink does not exist
                  .catch(err => {
                    console.log(err.message);
                  });
                this.props.navigation.navigate('policeVerification');
              }}
              style={{
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Re-Scan
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('commissionerPin', {
                  uri: this.props.route.params.imageUri,
                });
              }}
              style={{
                opacity: 0.8,
                height: 50,
                width: 60,
                backgroundColor: 'black',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: '5%',
              }}>
              <Text style={{color: 'white', fontSize: 20}}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default policeScannerView