import React from 'react';
import {View, ActivityIndicator, Text} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import {
  tokenStatus,
  Add,
  getAllCustomDocuments,
  acceptDocument,
  sendFirebaseId,
} from '../redux/actions/index';
import AsyncStorage from '@react-native-community/async-storage';
import {ThemeProvider} from '@react-navigation/native';
import firebase from '@react-native-firebase/app';
import LottieView from 'lottie-react-native';

class splashScreen2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
    };
  }

  componentDidMount() {
    this.checkTokenStatus();
  }

  async checkTokenStatus() {
    await this.checkPermission();
    await this.props.Add();
    // alert('dahdj')

    try {
      // let id = await AsyncStorage.removeItem('id');
      // let token = await AsyncStorage.removeItem('token');

      let id = await AsyncStorage.getItem('id');
      let token = await AsyncStorage.getItem('token');
      let fcmToken = await AsyncStorage.getItem('fcmToken');

      if (id != null && token != null) {
        await this.props.sendFirebaseId(id, token, fcmToken);
        await this.props.tokenStatus(id, token);
        await this.props.getAllCustomDocuments(token);
        await this.props.acceptDocument(token);
        await this.props.Add();

        await this.props.navigation.reset({
          index: 0,
          routes: [{name: 'home'}],
        });
      } else {
        // this.props.asyncLoader();

        this.props.navigation.navigate('welcome');
      }
    } catch (error) {
      // this.props.asyncLoader();
      this.props.navigation.navigate('welcome');
    }
  }
  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    // If Premission granted proceed towards token fetch
    if (enabled) {
      this.getToken();
    } else {
      // If permission hasn’t been granted to our app, request user in requestPermission method.
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    console.log('fcmToken: ', fcmToken);
    if (!fcmToken) {
      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }
  componentWillUnmount() {
    this.checkTokenStatus();
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
          justifyContent: 'center',
        }}>
        {this.props.addition.async_loader ? (
          <LottieView source={require('../lottie/data.json')} autoPlay loop />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {Add, tokenStatus, getAllCustomDocuments, acceptDocument, sendFirebaseId},
)(splashScreen2);
