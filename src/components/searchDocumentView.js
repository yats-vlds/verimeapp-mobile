import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  ScrollView,
  ImageBackground,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import RNPickerSelect from 'react-native-picker-select';
import {connect} from 'react-redux';
import Header from './Header';
import FastImage from 'react-native-fast-image';
import Image from 'react-native-image-progress';
import * as Progress from 'react-native-progress';
import HomeHeader from './HeaderHome';
import Pdf from 'react-native-pdf';


class searchDocumentView extends React.Component {
  render() {
    return (
      <ScrollView>
        <HomeHeader />

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          {this.props.route.params.imageUri != undefined ? (
            <View>
              <ImageBackground
                source={require('../images/icons/thumbnail.png')}
                style={{
                  width: 350,
                  height: 350,
                  overflow: 'hidden',
                  backgroundColor: '#bebebe',
                }}
                resizeMode="cover">
                <Pdf
                  source={{uri: this.props.route.params.imageUri}}
                  style={{
                    width: 400,
                    height: 400,
                    overflow: 'hidden',
                    flex: 1,
                  }}
                />
              </ImageBackground>
            </View>
          ) : null}

          <View style={{marginTop: 20, marginBottom: 20}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'column'}}>
                <View>
                  <Text style={{color: '#148567'}}>Document Name:</Text>
                </View>
                {/* <View>
                                    <Text style={{color: '#148567'}}>Date Issue:</Text>


                                </View>

                                <View>
                                    <Text style={{color: '#148567'}}>Date End:</Text>

                                </View> */}
              </View>

              <View style={{flexDirection: 'column', marginLeft: '5%'}}>
                <View>
                  <Text>{this.props.route.params.name}</Text>
                </View>

                <View>{/* <Text>4/6/2020</Text> */}</View>
                <View>{/* <Text>6/6/2020</Text> */}</View>
              </View>
            </View>
          </View>
          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 10}}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('SEARCH')}
              style={{
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Done
              </Text>
            </TouchableOpacity>
            {/* <TouchableOpacity onPress={() => {
                            this.props.navigation.navigate('AddNewDocument',{uri: this.props.route.params.imageUri});

                        }} style={{
                            opacity: 0.8,
                            height: 50,
                            width: 60,
                            backgroundColor: 'black',
                            borderRadius: 10,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginLeft: '5%'
                        }}>
                            <Text style={{color: 'white',fontSize: 20}}>Next</Text>

                        </TouchableOpacity> */}
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps
)(searchDocumentView);
