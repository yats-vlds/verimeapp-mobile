import React, {useState} from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';

import * as selectors from '@redux/selectors'

export default () => {

  const acceptedDocs = selectors.getDocFavouriteList();

  return (
    <View>

      {acceptedDocs && acceptedDocs.length ? (
        acceptedDocs.map(item => {
          return (
            <View
              key={item.id}
              style={{
                marginLeft: 10,
                marginRight: 10,
                marginTop: 5,
                borderRadius: 5,
                shadowColor: '#000',
                shadowOffset: {width: 0, height: 7},
                shadowOpacity: 0.25,
                shadowRadius: 3.84,
                elevation: 3,
              }}>
              <View style={{padding: 10}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{marginRight: 10}}>
                    <Image
                      resizeMode="cover"
                      source={require('../images/icons/Ame-Splash-Screen.jpg')}
                      style={{
                        borderRadius: 50,
                        height: 50,
                        width: 50,
                      }}
                    />
                  </View>
                  <View>
                    <Text style={{fontSize: 13}}>{` User ${
                      item.phone
                    } wants to\n access your document of ${
                      item.doc_name
                    }`}</Text>
                  </View>
                </View>
              </View>
            </View>
          );
        })
      ) : (
        <View
          style={{
            marginTop: 209.5,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 20, color: '#bebebe'}}>No Records</Text>
        </View>
      )}
    </View>
  );
}

