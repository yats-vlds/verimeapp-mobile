import React from 'react';
import {View, Text, Image, Animated, Easing} from 'react-native';
import LottieView from 'lottie-react-native';
import {connect} from 'react-redux';
import HomeHeader from './HeaderHome';

class verifier extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      progress: new Animated.Value(0),
    };
  }
  componentDidMount() {
    Animated.timing(this.state.progress, {
      toValue: 1,
      duration: 3000,
      easing: Easing.linear,
    }).start();
  }
  render() {
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <HomeHeader />
        <View
          style={{
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <LottieView
            source={require('../lottie/verifyanime.json')}
            progress={this.state.progress}
          />
          <Text
            style={{
              fontSize: 30,
              fontWeight: 'bold',

              position: 'absolute',
              bottom: 120,
            }}>
            Verified
          </Text>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  null,
)(verifier);
