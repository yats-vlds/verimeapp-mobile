import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {documentCall, Add} from '../redux/actions/index';
import HomeHeader from './HeaderHome';
import Pdf from 'react-native-pdf';
var RNFS = require('react-native-fs');

class DocumentReviewToUpload extends React.Component {

  componentDidMount = async () => {
    uri: '/storage/emulated/0/Android/data/com.verime/files' + '/Document.pdf';
  };

  render() {
    const source = {
      uri: this.props.route.params.imageUri,
      cache: true,
    };
    // alert(this.props.route.params.imageUri);
    return (
      <ScrollView>
        <HomeHeader />

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          <View style={{flex: 1, alignItems: 'center'}} />

          {this.props.route.params.imageUri != undefined ? (
            <Pdf
              source={source}
              style={{
                width: 400,
                height: 400,
                overflow: 'hidden',
                flex: 1,
              }}
            />
          ) : null}

          <View
            style={{flexDirection: 'row', alignItems: 'center', marginTop: 30}}>
            <TouchableOpacity
              onPress={async () => {
                await RNFS.unlink(this.props.route.params.imageUri)
                  .then(() => {
                    console.log('FILE DELETED');
                  })
                  // `unlink` will throw an error, if the item to unlink does not exist
                  .catch(err => {
                    console.log(err.message);
                  });
                this.props.navigation.navigate('DocumentScan');
              }}
              style={{
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Re-Scan
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                this.props.navigation.navigate('AddNewDocument', {
                  uri: this.props.route.params.imageUri,
                });
              }}
              style={{
                opacity: 0.8,
                height: 50,
                width: 60,
                backgroundColor: 'black',
                borderRadius: 10,
                justifyContent: 'center',
                alignItems: 'center',
                marginLeft: '5%',
              }}>
              <Text style={{color: 'white', fontSize: 20}}>Next</Text>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {documentCall},
)(DocumentReviewToUpload);
