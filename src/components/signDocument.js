import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
class signDocument extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.imageContainer}>
          <Image
            style={{height: 200, width: 200}}
            source={require('../images/icons/paper.png')}
          />
        </View>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() =>
              this.props.navigation.navigate('digitaSignature', {
                uri: this.props.route.params,
                item: this.props.route.params.item,
              })
            }
            style={{
              marginTop: 30,
              width: 250,
              height: 50,
              backgroundColor: '#148567',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
              flexDirection: 'row',
            }}>
            <Text style={{fontSize: 20, color: 'white'}}>Download</Text>
            {/* <Image
              source={require('../images/icons/download_document.png')}
              style={{width: 25, height: 25}}
            /> */}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imageContainer: {
    marginTop: Dimensions.get('window').height * 0.2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
export default signDocument;
