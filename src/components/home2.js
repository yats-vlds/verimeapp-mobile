import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
export default class home2 extends React.Component {
  render() {
    return (
      <View style={{flex: 1}}>
        <View
          style={{flex: 0.3, justifyContent: 'center', alignItems: 'center'}}>
          <Icon name="user-circle" size={60} color="#148567" />
          <View style={{marginTop: '2%'}}>
            <Text
              style={{
                fontSize: 25,
                color: 'black',
                alignSelf: 'center',
                fontWeight: 'bold',
              }}>
              JACK LIAM
            </Text>
          </View>
        </View>
        <View
          style={{
            paddingLeft: '23%',
            paddingRight: '23%',
            paddingBottom: '23%',
            flex: 0.5,
          }}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/folder.png')}
                  style={{height: 35, width: 35}}
                />
              </TouchableOpacity>

              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                YOUR DOCS
              </Text>
            </View>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/video-camera.png')}
                  style={{height: 30, width: 40}}
                />
              </TouchableOpacity>
              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                VIDEO
              </Text>
            </View>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '5%',
            }}>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/data.png')}
                  style={{height: 30, width: 30}}
                />
              </TouchableOpacity>
              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                USAGE
              </Text>
            </View>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/correct.png')}
                  style={{height: 30, width: 30}}
                />
              </TouchableOpacity>
              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                YOUR DOCS
              </Text>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
              marginTop: '5%',
            }}>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/settings.png')}
                  style={{height: 30, width: 30}}
                />
              </TouchableOpacity>
              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                VIDEO
              </Text>
            </View>
            <View>
              <TouchableOpacity
                style={{
                  height: 100,
                  width: 100,
                  backgroundColor: 'white',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Image
                  source={require('../images/icons/plus.png')}
                  style={{height: 30, width: 30}}
                />
              </TouchableOpacity>
              <Text
                style={{fontSize: 12, marginTop: '10%', alignSelf: 'center'}}>
                USAGE
              </Text>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
