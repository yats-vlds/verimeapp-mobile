import React from 'react';
import {View} from 'react-native';
import invokeApp from 'react-native-invoke-app';

import {connect} from 'react-redux';
import {
  tokenStatus,
  Add,
  getAllCustomDocuments,
  acceptDocument,
  pushScreen,
  sendFirebaseId,
} from '../redux/actions/index';

import AsyncStorage from '@react-native-community/async-storage';
import LottieView from 'lottie-react-native';
import messaging from '@react-native-firebase/messaging';

var notificationOpen = '';
class splashScreen extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: true,
      check: false,
    };
  }

  componentDidMount() {
    this.checkTokenStatus();
  }

  async checkTokenStatus() {
    await this.checkPermission();
    // Register all listener for notification
    await this.createNotificationListeners();

    try {
      // let id = await AsyncStorage.removeItem('id');
      // let token = await AsyncStorage.removeItem('token');

      let id = await AsyncStorage.getItem('id');
      let token = await AsyncStorage.getItem('token');
      let fcmToken = await AsyncStorage.getItem('fcmToken');

      if (id != null && token != null) {
        await this.props.tokenStatus(id, token, this.props.navigation);

        await this.props.sendFirebaseId(id, token, fcmToken);
        await this.props.acceptDocument(token);
        await this.props.getAllCustomDocuments(token);

        if (
          this.props.addition.async_id == '' ||
          this.props.addition.async_token == ''
        ) {
          await this.props.navigation.navigate('welcome');
        } else {
          if (
            this.state.check &&
            this.props.addition.userInfo.documents != '' &&
            this.props.addition.userInfo.video != '' &&
            this.props.addition.userInfo.image != '' &&
            this.props.addition.userInfo.first_name != ''
          ) {
            await this.props.pushScreen(true);
            await this.props.navigation.navigate('home');

            this.setState({check: false});
          }
          //     await this.props.getAllCustomDocuments(token);
          //     await this.props.Add();
          // }
          else {
            if (this.props.addition.userInfo.documents == '') {
              this.props.navigation.navigate('scanner');
            } else if (this.props.addition.userInfo.video == '') {
              this.props.navigation.navigate('video');
            } else if (this.props.addition.userInfo.image == '') {
              this.props.navigation.navigate('photo');
            } else if (this.props.addition.userInfo.first_name == '') {
              this.props.navigation.navigate('personalInfo');
            }
            //else if (this.props.addition.userInfo.postal_adress == '') {
            //     await this.props.navigation.navigate('PaymentVerification')
            // }
            else {
              await this.props.navigation.navigate('home');
            }
          }
        }
      } else {
        await this.props.navigation.navigate('welcome');
      }
    } catch (error) {
      // this.props.asyncLoader();
      // await this.props.navigation.navigate('welcome');
    }
  }
  
  async checkPermission() {
    const enabled = await messaging().hasPermission();
    // If Premission granted proceed towards token fetch
    if (enabled) {
      this.getToken();
    } else {
      // If permission hasn’t been granted to our app, request user in requestPermission method.
      this.requestPermission();
    }
  }

  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');

    console.log('fcmToken: ', fcmToken);
    if (!fcmToken) {
      fcmToken = await messaging().getToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  async requestPermission() {
    try {
      await messaging().requestPermission();
      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
    }
  }

  async createNotificationListeners() {
    // This listener triggered when notification has been received in foreground
    this.notificationListener = messaging().onMessage(notification => {
      // const {title, body} = notification;

      invokeApp();

      // this.displayNotification(title, body);
    });

    // This listener triggered when app is in backgound and we click, tapped and opened notifiaction
    this.notificationOpenedListener = messaging().onMessage(
      notificationOpen => {
        this.props.navigation.navigate('videoCalling');
        // const {title, body} = notificationOpen.notification;

        // this.displayNotification(title, body);
      },
    );

    // This listener triggered when app is closed and we click,tapped and opened notification
    const notificationOpen = messaging().onMessage;

    // if (notificationOpen) {
    //   // console.log(SajjadLaunchApplication.open('com.verime.application'));

    //   const {title, body} = notificationOpen.notification;
    //   invokeApp();

    //   this.displayNotification(title, body);
    // }
  }

  displayNotification = async (title, body) => {
    // console.log(JSON.stringify(title), body);
    // we display notification in alert box with title and body
    // this.props.navigation.navigate('scanner');

    if (body != undefined && title != undefined) {
      invokeApp();

      // // console.log(SajjadLaunchApplication);
      // Alert.alert(
      //   title,
      //   body,
      //   [
      //     {
      //       text: 'Ok',
      //       onPress: () => {
      //         //  this.props.pushScreen(true)
      //         // this.props.navigation.navigate('videoCalling');
      //         // alert('ok')
      //       },
      //     },
      //   ],
      //   {cancelable: false},
      // );
    } else {
      this.setState({check: true});

      // console.log('......', SajjadLaunchApplication);
      await this.props.navigation.navigate('videoCalling');
    }
  };

  componentWillUnmount() {
    this.checkTokenStatus();
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          alignItems: 'center',
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
          justifyContent: 'center',
        }}>
        {this.props.addition.async_loader ? (
          <LottieView source={require('../lottie/data.json')} autoPlay loop />
        ) : null}
      </View>
    );
  }
}

const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
  {
    Add,
    tokenStatus,
    getAllCustomDocuments,
    acceptDocument,
    pushScreen,
    sendFirebaseId,
  },
)(splashScreen);
