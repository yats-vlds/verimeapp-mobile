import React from "react";
import {View, Text, StyleSheet, TouchableOpacity} from "react-native"
import IconSimpleLineAntd from "react-native-vector-icons/AntDesign";
import IconSimpleLineRefresh from "react-native-vector-icons/SimpleLineIcons";

export const HeaderChoiceForm = ({refreshHandler, name, navigation, leftArrow}) => {
    return (
        <View style={styles.header}>
            <View style={styles.leftArrow}>
                <IconSimpleLineAntd name="arrowleft" size={25} color="#ffffff" onPress={refreshHandler} onPress={() => navigation.navigate(`${leftArrow}`)}/>
            </View>
            <View style={styles.headerBottom}>
                <View style={styles.headerBottomLeft}>
                    <Text style={styles.headerBottomLeftText}>{name}</Text>
                </View>
                <View style={styles.headerBottomRight}>
                    <IconSimpleLineRefresh name="refresh" size={25} color="#ffffff" style={{transform: [{rotate: "90deg"}]}}  onPress={refreshHandler}/>
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    header: {
        zIndex: 9999,
        height: 100,
        width: "100%",
        backgroundColor: "#48AC98",
        paddingTop: 0,
        marginTop: 0,
        borderBottomStartRadius: 10,
        borderBottomRightRadius: 10
    },
    leftArrow: {
        paddingTop: "3%",
        paddingLeft: "4%"
    },
    headerBottom: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
    },
    headerBottomLeft: {
        paddingTop: "2%",
        paddingLeft: "4%"
    },
    headerBottomLeftText: {
        fontSize: 22,
        fontWeight: "bold",
        color: "#ffffff"
    },
    headerBottomRight: {
        paddingRight: "4%",
        paddingTop: "3%"

    },
})
