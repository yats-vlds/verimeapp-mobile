import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Animated,
  ActivityIndicator,
} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';

import {verifyOTP} from '@redux/onboarding/actions';
import Header from '../components/Header';

class OTP extends React.Component {
  constructor(props) {
    super(props);
    this.keyboardHeight = new Animated.Value(0);
    this.num1 = React.createRef();
    this.num2 = React.createRef();
    this.num3 = React.createRef();
    this.num4 = React.createRef();
    this.state = {
      textInput1: '',
      textInput2: '',
      textInput3: '',
      textInput4: '',
    };
  }
  inputNumber(value, flag) {
    const completeFlag = `num${flag}`;
    this.setState({[completeFlag]: value});
    flag = flag + 1;
    if (flag < 5 && value) {
      const nextFlag = `num${flag}`;
      const textInputToFocus = this[nextFlag];
      textInputToFocus.current.focus();
    }
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
        }}>
        <Header
          gob={null}
          value={this.props.navigation}
          title="OTP Verification"
        />

        <View style={{marginTop: 25}}>
          <View>
            <Text>Enter 4 digits code sent to</Text>
            <Text>{this.props.route.params.phoneNo}</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: 15,
            }}>
            <TextInput
              ref={this.num1}
              style={{
                borderBottomWidth: 1,
                width: 70,
                borderBottomColor: '#148567',
                textAlign: 'center',
                fontSize: 25,
              }}
              onChangeText={number => {
                this.inputNumber(number, 1);
                this.setState({textInput1: number});
              }}
              keyboardType="numeric"
              numberOfLines={1}
              maxLength={1}
            />
            <TextInput
              ref={this.num2}
              style={{
                marginLeft: '7%',
                borderBottomWidth: 1,
                width: 70,
                borderBottomColor: '#148567',
                textAlign: 'center',
                fontSize: 25,
              }}
              onChangeText={number => {
                this.inputNumber(number, 2);
                this.setState({textInput2: number});
              }}
              onKeyPress={e => {
                if (e.nativeEvent.key == 'Backspace') {
                  this.num1.current.focus();
                }
              }}
              keyboardType="numeric"
              numberOfLines={1}
              maxLength={1}
            />
            <TextInput
              ref={this.num3}
              style={{
                marginLeft: '7%',
                borderBottomWidth: 1,
                width: 70,
                borderBottomColor: '#148567',
                textAlign: 'center',
                fontSize: 25,
              }}
              onChangeText={number => {
                this.inputNumber(number, 3);
                this.setState({textInput3: number});
              }}
              keyboardType="numeric"
              onKeyPress={e => {
                if (e.nativeEvent.key == 'Backspace') {
                  this.num2.current.focus();
                }
              }}
              numberOfLines={1}
              maxLength={1}
            />
            <TextInput
              ref={this.num4}
              style={{
                marginLeft: '7%',
                borderBottomWidth: 1,
                width: 70,
                borderBottomColor: '#148567',
                textAlign: 'center',
                fontSize: 25,
              }}
              onChangeText={number => {
                this.inputNumber(number, 4);
                this.setState({textInput4: number});
              }}
              onKeyPress={e => {
                if (e.nativeEvent.key == 'Backspace') {
                  this.num3.current.focus();
                }
              }}
              keyboardType="numeric"
              numberOfLines={1}
              maxLength={1}
            />
          </View>
        </View>
        <View
          style={{alignItems: 'center', marginTop: '12%', marginBottom: 100}}>
          <Text style={{fontSize: 25, fontWeight: 'bold'}}>
            Enter Your Code
          </Text>
          {this.props.loading ? (
            <ActivityIndicator
              size="large"
              color="black"
              style={{marginTop: 25}}
            />
          ) : (
            <TouchableOpacity
              onPress={() => {
                if (
                  this.state.textInput1 != '' &&
                  this.state.textInput2 != '' &&
                  this.state.textInput3 != '' &&
                  this.state.textInput4 != ''
                ) {
                  const concatenated_pin = `${this.state.textInput1}${
                    this.state.textInput2
                  }${this.state.textInput3}${this.state.textInput4}`;
                  this.props.verifyOTP(concatenated_pin);
                } else {
                  Toast.show('Enter Code Please', Toast.SHORT);
                }
              }}
              style={styles.start}>
              <Text style={{color: 'white', fontSize: 20}}>Enter</Text>
            </TouchableOpacity>
          )}
        </View>

        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 40,
            flexGrow: 1,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleOne} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '15%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});

const mapStateToProps = state => {
  const {loading} = state.onboarding;
  return {
    loading
  };
};

export default connect(mapStateToProps, { verifyOTP })(OTP);
