import React, { useEffect, useState } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import SearchInput, {createFilter} from 'react-native-search-filter';
import HomeHeader from '../components/HeaderHome';

import * as selectors from '@redux/selectors'
import * as actions from '@redux/actions'
import { useNavigation } from '@react-navigation/native';
import { useDispatch } from 'react-redux';

const KEYS_TO_FILTERS = ['name', 'id'];

export default () => {
  const [searchTerm, setSearchTerm] = useState('');

  const navigation = useNavigation();
  const dispatch = useDispatch();

  const filteredDocs = selectors.getDocumentList().filter(
    createFilter(searchTerm, KEYS_TO_FILTERS),
  );

  useEffect(() => {
    dispatch(actions.getDocuments())
  }, [])

  return (
    <View style={styles.container}>
      <HomeHeader />
      <SearchInput
        onChangeText={setSearchTerm}
        style={styles.searchInput}
        placeholder="Type a message to search"
        inputViewStyles={{backgroundColor: '#dedede'}}
      />
      <ScrollView>
        {filteredDocs.map(item => {
          return (
            <TouchableOpacity
              key={item.id}
              onPress={() =>
                navigation.navigate('searchDocumentView', {
                  imageUri: item.document,
                  name: item.name,
                })
              }
              style={styles.emailItem}>
              <View>
                <Text>{item.name}</Text>
              </View>
            </TouchableOpacity>
          );
        })}
      </ScrollView>
    </View>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'flex-start',
  },
  emailItem: {
    borderBottomWidth: 0.5,
    borderColor: 'rgba(0,0,0,0.3)',
    padding: 10,
  },
  emailSubject: {
    color: 'rgba(0,0,0,0.5)',
  },
  searchInput: {
    borderColor: '#CCC',
    borderWidth: 0.5,
    backgroundColor: '#dedede',

    borderRadius: 5,
    padding: 15,
    marginTop: 10,
  },
});
