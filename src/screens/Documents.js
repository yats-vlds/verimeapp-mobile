import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {FloatingAction} from 'react-native-floating-action';

import {connect} from 'react-redux';
import {getDocuments} from '@redux/user/actions';
import AsyncStorage from '@react-native-community/async-storage';
import HomeHeader from '../components/HeaderHome';
import {color} from 'react-native-reanimated';

class Documents extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      text: '',
      img: '',
    };
    Dimensions.addEventListener('change', e => {
      // console.log('change');
      this.setState(e.window);
    });
  }

  componentDidMount() {
    this.props.getDocuments();
  }

  render() {
    const actions = [
      {
        text: 'Police Verification',
        icon: require('../images/icons/shield.png'),
        name: 'police',
        position: 1,
        color: '#148567',
      },
      {
        text: 'Add New Document',
        icon: require('../images/icons/copy.png'),
        name: 'document',
        position: 1,
        color: '#148567',
      },
    ];
    
    return (
      <View>
        <ScrollView style={{height: '100%'}}>
          <HomeHeader />

          <View
            style={{
              marginTop: '5%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 25, fontWeight: 'bold'}}>Docs</Text>
          </View>
          <View style={{alignItems: 'center'}}>
            <View style={{marginTop: '5%', width: '85%'}}>
              <View style={{width: '100%'}}>
                <View style={{marginTop: '5%', marginBottom: '10%'}}>
                  <View>
                    {(this.props.documentList && this.props.documentList.length) ? this.props.documentList.map((item, index) => {
                          console.log(item);
                          return (
                            <TouchableOpacity
                              key={item.id}
                              onPress={() => {
                                // if (item.id == -1) {
                                //   this.props.navigation.navigate('documentView', {
                                //     name: item.name,
                                //     imageUri: item.document,
                                //   });
                                //   return;
                                // }

                                this.props.navigation.navigate(
                                  'DocumentView',
                                  {
                                    id: item.id,
                                    name: item.name,
                                    imageUri: item.document,
                                  },
                                )
                              }}
                              style={{
                                height: 45,
                                width: '100%',
                                borderRadius: 3,
                                backgroundColor: 'white',
                                justifyContent: 'center',
                                marginTop: index != 0 ? '5%' : 0,
                              }}>
                              <Text style={{marginLeft: '3%', fontSize: 18}}>
                                {item.name}
                              </Text>
                            </TouchableOpacity>
                          );
                        })
                      : null}
                  </View>

                  {/* <View style={{marginTop: '10%'}}>
                      <TouchableOpacity onPress={() => this.props.navigation.navigate('DocumentScan')} style={{flexDirection: 'row'}}>
                          <Icon name="plus" size={15} color="#148567" />
                          <Text style={{marginLeft: '3%'}}>Add New Document</Text>

                      </TouchableOpacity>
                  </View>
                  <TouchableOpacity onPress={() => this.props.navigation.navigate('policeVerification')} style={{flexDirection: 'row',marginTop: '3%'}}>
                      <Icon name="plus" size={15} color="#148567" />
                      <Text style={{marginLeft: '3%'}}>Add Police Verification</Text>

                  </TouchableOpacity> */}
                </View>
              </View>
            </View>
          </View>
        </ScrollView>

        <FloatingAction
          actions={actions}
          onPressItem={name => {
            if (name == 'document') {
              this.props.navigation.navigate('DocumentScan');
            } else if (name == 'police') {
              this.props.navigation.navigate('scanStampedDocument');
            }
          }}
          style={{}}
          color="#148567"
        />
      </View>
    );
  }
}
const mapStateToProps = state => {
  const {user_data, documents} = state.user;
  return {
    user_data,
    documentList: documents
  };
};
export default connect(
  mapStateToProps,
  {getDocuments},
)(Documents);
