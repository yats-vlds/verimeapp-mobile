import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  ActivityIndicator,
  ToastAndroid,
} from 'react-native';
import {connect} from 'react-redux';
import {TextInput} from 'react-native-gesture-handler';
import Toast from 'react-native-simple-toast';

import VirtualKeyboard from '../components/react-native-virtual-keyboard/src/VirtualKeyboard';
import Header from '../components/Header';
import {getOtp, clearError} from '@redux/onboarding/actions';

var formated_number;

class EnterPhone extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
    };
  }

  changeText(newText) {
    this.setState({text: newText});
  }

  formatPhone = text => {
    var array = [];
    while (text.length > 3) {
      array.push(text.substring(0, 3));
      text = text.substring(3);
    }
    if (text.length > 0) {
      array.push(text);
    }
    formated_number = array.join(' ');
    return array.join(' ');
  };

  render() {
    var regex = /^(?:\+264|264)(\d{9})$/;
    var phoneNo = '';

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          marginTop: 25,
          marginLeft: 25,
          marginRight: 25,
        }}>
        <Header
          gob={null}
          value={this.props.navigation}
          title="Enter Your Phone"
        />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 25,
          }}>
          <TextInput
            maxLength={16}
            placeholder="264 813 048 402"
            editable={false}
            value={this.formatPhone(this.state.text)}
            style={{
              borderBottomWidth: 1,
              width: 300,
              borderBottomColor: '#63d4b6',
              color: 'black',
              fontSize: 20,
            }}
          />
        </View>
        <View style={{marginTop: 25}}>
          <VirtualKeyboard
            cellStyle={{
              borderWidth: 1,
              height: 50,
              marginRight: '5%',
              borderRadius: 10,
            }}
            color="black"
            pressMode="string"
            onPress={val => {
              this.changeText(val);
            }}
          />
        </View>

        <View style={{alignItems: 'center', marginTop: 25}}>
          {this.props.loading ? (
            <ActivityIndicator size="large" color="black" />
          ) : (
            <TouchableOpacity
              onPress={() => {
                const phoneNo = this.state.text.replace(/\s+/g, '');

                if (regex.test(phoneNo) == true) {
                  console.log(phoneNo);
                  this.props.getOtp(this.formatPhone(this.state.text));
                  // this.props.registerNumber(phoneNo, this.props.navigation, formated_number);
                } else {
                  Toast.show('Phone Number is Invalid', Toast.SHORT);
                }
              }}
              style={styles.start}>
              <Text style={{color: 'white', fontSize: 20}}>Send</Text>
            </TouchableOpacity>
          )}
        </View>
        <View
          style={{
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'flex-end',
            marginBottom: 40,
            flexGrow: 1,
          }}>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.singleDesign} />
            <View style={styles.singleOne} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },

  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});
const mapStateToProps = state => {
  const { loading, error } = state.onboarding;
  return {
    loading,
    error,
  };
};
const mapDispatchToProps = {
  getOtp,
  clearError,
};

export default connect( mapStateToProps, mapDispatchToProps )(EnterPhone);
