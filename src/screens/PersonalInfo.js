import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ScrollView,
  KeyboardAvoidingView,
  ActivityIndicator,
} from 'react-native';
import RNPickerSelect from 'react-native-picker-select';
import {TextInputMask} from 'react-native-masked-text';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';

import Header from '../components/Header';

import {updatePersonalInfo} from '@redux/onboarding/actions';

class PersonalInfo extends React.Component {
  constructor(props) {
    super(props);

    // Dummy data
    // this.state = {
    //   firstname: 'sadf',
    //   surname: 'asdfas',
    //   dob: '06/06/1975',
    //   id_no: '1232423',
    //   streetAddress: 'sdfasdf',
    //   postalAddress: 'asdfasdf',
    //   email: 'a@asdf.com',
    //   mobileNo: '12342315',
    //   telNo: '23452345',
    //   employer: 'safasdf',
    //   picker1: 'baseball',
    //   picker2: 'hockey',
    // };

    this.state = {
      firstname: '',
      surname: '',
      dob: '',
      id_no: '',
      streetAddress: '',
      postalAddress: '',
      email: '',
      mobileNo: '',
      telNo: '',
      employer: '',
      picker1: '',
      picker2: '',
    };
  }

  componentDidMount() {
    
    if (this.props.personalInfo != null) {
      this.setState(this.props.personalInfo);
    }
  }
  

  render() {
    const regix = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return (
      <KeyboardAvoidingView
        behavior={Platform.OS === 'ios' ? 'padding' : null}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 64 : 0}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View
            style={{
              flexDirection: 'column',
              marginTop: 25,
              marginLeft: 25,
              marginRight: 25,
            }}>
            <Header
              gob={null}
              value={this.props.navigation}
              title="Personal Info"
            />
            <View style={{alignItems: 'center', marginTop: 20}}>
              <View>
                <TextInput
                  onChangeText={text => this.setState({firstname: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="First Name"
                  value={this.state.firstname }
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({surname: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Surname"
                  value={this.state.surname}
                />
              </View>
              <View>
                <TextInputMask
                  onChangeText={text => this.setState({dob: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="DOB"
                  type={'datetime'}
                  options={{
                    format: 'DD/MM/YYYY',
                  }}
                  value={this.state.dob}
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({id_no: text})}
                  keyboardType="numeric"
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="ID NO"
                  value={this.state.id_no}
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({streetAddress: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Street Address"
                  value={this.state.streetAddress}
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({postalAddress: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Postal Adress"
                  value={this.state.postalAddress}
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({email: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Email"
                  keyboardType="email-address"
                  value={this.state.email}
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({mobileNo: text})}
                  value={this.state.mobileNo}
                  maxLength={13}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Mobile No"
                  keyboardType="numeric"
                />
              </View>
              <View>
                <TextInput
                  onChangeText={text => this.setState({telNo: text})}
                  maxLength={13}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Tel No (Office)"
                  keyboardType="numeric"
                  value={this.state.telNo}
                />
              </View>

              <View>
                <TextInput
                  onChangeText={text => this.setState({employer: text})}
                  style={{
                    borderBottomWidth: 1,
                    borderColor: '#63d4b6',
                    width: 300,
                    marginTop: '2%',
                  }}
                  placeholder="Employer"
                  value={this.state.employer}
                />
              </View>
              <View
                style={{
                  alignItems: 'center',
                  width: 300,
                  justifyContent: 'center',
                  marginTop: 20,
                }}>
                <RNPickerSelect
                  placeholder={{label: 'Select Industry'}}
                  onValueChange={value => this.setState({picker1: value})}
                  items={[
                    {label: 'Football', value: 'football'},
                    {label: 'Baseball', value: 'baseball'},
                    {label: 'Hockey', value: 'hockey'},
                  ]}
                  style={{inputAndroidContainer: {backgroundColor: 'red'}}}
                  value={this.state.picker1}
                />
                <RNPickerSelect
                  placeholder={{label: 'Select Occupation'}}
                  onValueChange={value => this.setState({picker2: value})}
                  items={[
                    {label: 'Football', value: 'football'},
                    {label: 'Baseball', value: 'baseball'},
                    {label: 'Hockey', value: 'hockey'},
                  ]}
                  style={{inputAndroidContainer: {backgroundColor: 'red'}}}
                  value={this.state.picker2}
                />
              </View>
            </View>

            <View style={{alignItems: 'center', marginTop: '10%'}}>
              {this.props.loading ? (
                <ActivityIndicator size="large" color="black" />
              ) : (
                <TouchableOpacity
                  onPress={() => {
                    if (
                      regix.test(this.state.email) == false
                    ) {
                      Toast.show('Invalid email!', Toast.SHORT);
                    } else if (
                      this.state.firstname &&
                      this.state.surname &&
                      this.state.dob &&
                      this.state.id_no &&
                      this.state.streetAddress &&
                      this.state.postalAddress &&
                      this.state.email &&
                      this.state.mobileNo &&
                      this.state.telNo &&
                      this.state.employer &&
                      this.state.picker1 &&
                      this.state.picker2
                    ) {
                      var r = this.state.dob.split('/');

                      if (
                        Number(r[0]) <= 31 &&
                        Number(r[0]) > 0 &&
                        (Number(r[1]) <= 12 && Number(r[1]) > 0) &&
                        (r[2].length > 3 &&
                          Number(r[2]) <= 2020 &&
                          Number(r[2] >= 1970))
                      ) {
                        this.props.updatePersonalInfo({
                          firstname: this.state.firstname,
                          surname: this.state.surname,
                          dob: this.state.dob,
                          id_no: this.state.id_no,
                          streetAddress: this.state.streetAddress,
                          postalAddress: this.state.postalAddress,
                          email: this.state.email,
                          mobileNo: this.state.mobileNo,
                          telNo: this.state.telNo,
                          employer: this.state.employer,
                          picker1: this.state.picker1,
                          picker2: this.state.picker2,
                        });
                      } else {
                        Toast.show('Please write correct date', Toast.SHORT);
                      }
                    } else {
                      Toast.show('Field is missing', Toast.SHORT);
                    }
                  }}
                  style={styles.start}>
                  <Text style={{color: 'white', fontSize: 20}}>Save Info</Text>
                </TouchableOpacity>
              )}
            </View>

            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                marginTop: '8%',
                marginBottom: '11%',
              }}>
              <View
                style={{
                  height: 10,
                  width: 10,
                  borderRadius: 50,
                  backgroundColor: '#63d4b6',
                  opacity: 0.7,
                }}
              />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleDesign} />
              <View style={styles.singleOne} />
              <View style={styles.singleDesign} />
            </View>
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});

const mapStateToProps = state => {
  const {loading, personalInfo} = state.onboarding;
  return {
    loading,
    personalInfo
  };
};
export default connect(
  mapStateToProps,
  {
    updatePersonalInfo
  },
)(PersonalInfo);