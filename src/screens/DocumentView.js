import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';
import {connect} from 'react-redux';
import {documentCall} from '../redux/actions/index';
import Pdf from 'react-native-pdf';
import HomeHeader from '../components/HeaderHome';

class DocumentView extends React.Component {
  render() {
    return (
      <ScrollView>
        <HomeHeader />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          {this.props.route.params.imageUri != undefined ? (
            <View>
              <ImageBackground
                source={require('../images/icons/thumbnail.png')}
                style={{
                  width: 350,
                  height: 350,
                  overflow: 'hidden',
                  backgroundColor: '#bebebe',
                }}
                resizeMode="cover">
                <Pdf
                  source={{uri: this.props.route.params.imageUri, cache: true}}
                  onLoadComplete={() => {
                    
                    // console.log(`number of pages: ${numberOfPages}`);
                  }}
                  style={{
                    width: '100%',
                    height: '100%',
                    overflow: 'visible',
                  }}
                />
              </ImageBackground>
            </View>
          ) : null}

          <View style={{marginTop: 10}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'column'}}>
                <View>
                  <Text style={{color: '#148567'}}>Document Name:</Text>
                </View>
              </View>
              <View style={{flexDirection: 'column', marginLeft: '5%'}}>
                <View>
                  <Text>{this.props.route.params.name}</Text>
                </View>
              </View>
            </View>
          </View>
          {this.props.route.params.id === -1 && <View style={{alignItems: 'center', marginTop: 10}}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.goBack()
              }
              style={{
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Done
              </Text>
            </TouchableOpacity>
          </View>}
          {this.props.route.params.id !== -1 && <View style={{alignItems: 'center', marginTop: 10}}>
            <TouchableOpacity
              onPress={() =>
                this.props.navigation.navigate('ReScanDocument', {
                  name: this.props.route.params.name,
                  id: this.props.route.params.id,
                })
              }
              style={{
                width: 200,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                Re-Scan
              </Text>
            </TouchableOpacity>
          </View>}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    user_data: state.user.user_data,
  };
};
export default connect(
  mapStateToProps,
  {documentCall},
)(DocumentView);
