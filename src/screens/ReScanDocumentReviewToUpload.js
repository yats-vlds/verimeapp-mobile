import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
  ImageBackground,
} from 'react-native';

import {connect} from 'react-redux';
import {
  reScanDoc,
} from '@redux/user/actions';
import {ActivityIndicator} from 'react-native-paper';
import HomeHeader from '../components/HeaderHome';
import Pdf from 'react-native-pdf';
var RNFS = require('react-native-fs');

class ReScanDocumentReviewToUpload extends React.Component {

  render() {
    // const source = {
    //   uri: this.props.route.params.uri,
    //   cache: true,
    // };
    return (
      <ScrollView>
        <HomeHeader />

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 2,
          }}>
          {this.props.route.params.uri != undefined ? (
            <View>
              <ImageBackground
                source={require('../images/icons/thumbnail.png')}
                style={{
                  width: 350,
                  height: 350,
                  overflow: 'hidden',
                  backgroundColor: '#bebebe',
                }}
                resizeMode="cover">
                <Pdf
                  source={{
                    uri: this.props.route.params.uri,
                    cache: true,
                  }}
                  style={{
                    width: 400,
                    height: 400,
                    overflow: 'hidden',
                    flex: 1,
                  }}
                />
              </ImageBackground>
            </View>
          ) : null}

          <View style={{marginTop: 10}}>
            <View style={{flexDirection: 'row'}}>
              <View style={{flexDirection: 'column'}}>
                <View>
                  <Text style={{color: '#148567'}}>Document Name:</Text>
                </View>
                {/* <View>
                    <Text style={{color: '#148567'}}>Date Issue:</Text>


                </View>

                <View>
                    <Text style={{color: '#148567'}}>Date End:</Text>

                </View> */}
              </View>

              <View style={{flexDirection: 'column', marginLeft: '5%'}}>
                <View>
                  <Text>{this.props.route.params.name}</Text>
                </View>

                {/* <View>
                                    <Text>4/6/2020</Text>
                                </View>
                                <View>
                                    <Text>6/6/2020</Text>
                                </View> */}
              </View>
            </View>
          </View>
          {this.props.loading ? (
            <ActivityIndicator
              size="large"
              color="black"
              style={{marginTop: 20}}
            />
          ) : (
            <View
              style={{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 10,
              }}>
              <TouchableOpacity
                onPress={async () => {
                  await RNFS.unlink(this.props.route.params.uri)
                    .then(() => {
                      console.log('FILE DELETED');
                    })
                    // `unlink` will throw an error, if the item to unlink does not exist
                    .catch(err => {
                      console.log(err.message);
                    });
                  this.props.navigation.navigate('ReScanDocument', {
                    name: this.props.route.params.name,
                    id: this.props.route.params.id,
                  });
                }}
                style={{
                  width: 200,
                  height: 50,
                  backgroundColor: '#148567',
                  justifyContent: 'center',
                  alignItems: 'center',
                  borderRadius: 10,
                }}>
                <Text
                  style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
                  Re-Scan
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => {
                  this.props.reScanDoc(
                    this.props.route.params.id,
                    `file://${this.props.route.params.uri}`,
                    this.props.route.params.name,
                  );
                }}
                style={{
                  opacity: 0.8,
                  height: 50,
                  width: 60,
                  backgroundColor: 'black',
                  borderRadius: 10,
                  justifyContent: 'center',
                  alignItems: 'center',
                  marginLeft: '5%',
                }}>
                <Text style={{color: 'white', fontSize: 20}}>Next</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => {
  return {
    loading: state.user.loading,
  };
};
export default connect(
  mapStateToProps,
  {reScanDoc},
)(ReScanDocumentReviewToUpload);
