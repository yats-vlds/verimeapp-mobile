import React, { useState } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  ToastAndroid,
} from 'react-native';
import HomeHeader from '../components/HeaderHome';
import {connect, useDispatch} from 'react-redux';
import {
  Add,
  callId,
  callToken,
  policeSelectedDocs,
  scanStampDocsSent,
  scanStampDocs,
} from '../redux/actions/index';
import {Checkbox} from 'react-native-paper';
import Icon from 'react-native-vector-icons/FontAwesome';
import {iid} from '@react-native-firebase/app';
import commisionerPin from '../components/commisionerPin';

import * as selectors from '@redux/selectors'
import * as Actions from '@redux/actions'
import { useNavigation } from '@react-navigation/native';


export default () => {

  const dispatch = useDispatch();
  const docs = selectors.getDocumentList()

  const navigation = useNavigation();

  const [selectedDocs, setSelectedDocs] = useState([]);

  const containsDoc = (id) => (
    selectedDocs.filter((value) => value == id).length
  )

  return (
    <View style={{flex: 1}}>
        <HomeHeader />
        <ScrollView
          style={{height: '100%'}}
          showsVerticalScrollIndicator={false}>
          <View
            style={{
              marginTop: 20,
              marginBottom: 10,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text style={{fontSize: 25, fontWeight: 'bold'}}>
              Verify Documents
            </Text>
          </View>
          {docs && docs.length ? (
            docs.map((item, index) => {
              if (item.id < 1) {
                return;
              }
              return (
                <View key={item.id} style={styles.container}>
                  <View
                    style={{
                      flexDirection: 'row',
                      alignItems: 'center',
                      width: '100%',
                      backgroundColor: 'white',
                    }}>
                    <Checkbox
                      status={containsDoc(item.id) ? 'checked' : 'unchecked'}
                      onPress={() => {
                        if (containsDoc(item.id)) {
                          setSelectedDocs(selectedDocs.filter(value => value != item.id))
                        } else {
                          setSelectedDocs([...selectedDocs, item.id])
                        }
                        // this.props.scanStampDocs(item.id);
                      }}
                    />
                    <View
                      // onPress={() => {
                      //   this.props.scanStampDocs(item.id);
                      // }}
                      style={{
                        height: 45,
                        borderRadius: 3,
                        justifyContent: 'center',
                        borderRadius: 5,
                        marginLeft: 10,
                        width: 300,
                      }}>
                      <Text style={{fontSize: 18, marginLeft: '3%'}}>
                        {item.name}
                      </Text>
                    </View>

                  </View>
                </View>
              );
            })
          ) : (
            <View
              style={{
                marginTop: 200,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text style={{fontSize: 20, color: '#bebebe'}}>No Documents</Text>
            </View>
          )}
          <View
            style={{
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
              marginBottom: 30,
            }}>
            <TouchableOpacity
              onPress={() => {

                if (selectedDocs.length) {
                  dispatch(Actions.docsSelectedForVerification(selectedDocs))
                  navigation.navigate('policeVerification');
                } else {
                  ToastAndroid.show("Please select at least one document", ToastAndroid.SHORT)
                }

              }}
              style={{
                width: 250,
                height: 50,
                backgroundColor: '#148567',
                justifyContent: 'center',
                alignItems: 'center',
                borderRadius: 10,
              }}>
              <Text
                style={{
                  color: 'white',
                  fontSize: 18,
                  flexWrap: 'wrap',
                }}>
                Scan Stamped Documents
              </Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
  )
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: 25,
    marginVertical: 5
  },
});