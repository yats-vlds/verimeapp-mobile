import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  Dimensions,
} from 'react-native';
import {connect} from 'react-redux';
import FastImage from 'react-native-fast-image';
import * as Progress from 'react-native-progress';

import {getUser, getDocumentRequests} from '@redux/user/actions';


class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      width: Dimensions.get('window').width,
      height: Dimensions.get('window').height,
      loading: true,
      check: false,
    };
    Dimensions.addEventListener('change', e => {
      this.setState(e.window);
    });
  }
  componentDidMount() {
    this.props.getUser();
    this.props.getDocumentRequests();
    // TODO: Move to video calling
    // if (this.props.user_data.push_screen) {
    //   this.props.navigation.navigate('videoCalling');
      // this.props.pushScreen(false);
    // }
  }


  render() {

    return (
      <View style={{flex: 1}}>
        <View style={{flex: 1}}>
          <View
            style={{flex: 0.3, justifyContent: 'center', alignItems: 'center'}}>
            {(this.props.user_data && this.props.user_data.image) ? (
              <FastImage
                onProgress={() => {
                  return (
                    <Progress.Pie
                      progress={0.4}
                      size={50}
                      indeterminate={true}
                    />
                  );
                }}
                source={{uri: this.props.user_data.image}}
                style={{
                  borderRadius: 50,
                  width: 70,
                  height: 70,
                  borderWidth: 1,
                  overflow: 'hidden',
                }}
              />
            ) : (
              <Progress.Pie size={30} progress={0.4} indeterminate={true} />
            )}
            <View style={{marginTop: '2%'}}>
              {this.props.user_data && this.props.user_data.first_name ? (
                <Text
                  style={{
                    fontSize: 25,
                    color: 'black',
                    alignSelf: 'center',
                    fontWeight: 'bold',
                  }}>
                  {this.props.user_data.first_name}
                </Text>
              ) : null}
            </View>
          </View>
          <View
            style={{
              flex: 0.6,
              width: this.state.width,
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: this.state.width * 0.6,
              }}>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}>
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Documents')}
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/folder.png')}
                      style={{height: 35, width: 35}}
                    />
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontSize: 10.8,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    YOUR DOCS
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Chatbot')}
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/application-form.png')}
                      style={{height: 30, width: 40}}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 10.8,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    APP FORM
                  </Text>
                </View>
              </View>

              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: '5%',
                }}>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('History')
                    }
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/data.png')}
                      style={{height: 30, width: 30}}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 10.8,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    USAGE
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('NumberVerification')
                    }
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/correct.png')}
                      style={{height: 30, width: 30}}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 10.8,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    VERIFY
                  </Text>
                </View>
              </View>
              <View
                style={{
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                  marginTop: '5%',
                }}>
                <View>
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('Settings')}
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/settings.png')}
                      style={{height: 30, width: 30}}
                    />
                  </TouchableOpacity>
                  <Text
                    style={{
                      fontSize: 12,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    SETTINGS
                  </Text>
                </View>
                <View>
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.navigate('DocumentScan')
                    }
                    style={{
                      height: 100,
                      width: 100,
                      backgroundColor: 'white',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <Image
                      source={require('../images/icons/plus.png')}
                      style={{height: 30, width: 30}}
                    />
                  </TouchableOpacity>

                  <Text
                    style={{
                      fontSize: 12,
                      marginTop: '10%',
                      alignSelf: 'center',
                    }}>
                    ADD
                  </Text>
                </View>
              </View>
            </View>
          </View>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  const {user_data} = state.user;
  return {
    user_data
  };
};

export default connect(
  mapStateToProps,
  {getUser, getDocumentRequests},
)(Home);
