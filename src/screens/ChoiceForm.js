import React from "react"
import {View, Text, TouchableOpacity, Image, StyleSheet} from "react-native"
import {HeaderChoiceForm} from "./HeaderChoiceForm";
import IconFoundation from "react-native-vector-icons/Foundation";
import IconRight from "react-native-vector-icons/AntDesign";

const ChoiceForm = () => {
    return (
        <View style={styles.layout}>
            <HeaderChoiceForm name={"Forms"} />
            <View style={styles.cardForForm}>
                <IconFoundation name="clipboard-notes" size={25} color="#000000"/>
                <Text style={styles.cardForFormText}>Steak Steak Steak Steak</Text>
                <IconRight name="right" size={20} color="#000000" style={styles.cardForFormIconRight}/>
            </View>
            {/*<TouchableOpacity*/}
            {/*    onPress={() => navigation.navigate('Documents')}*/}
            {/*    style={{*/}
            {/*        height: 100,*/}
            {/*        width: 100,*/}
            {/*        backgroundColor: 'white',*/}
            {/*        justifyContent: 'center',*/}
            {/*        alignItems: 'center',*/}
            {/*    }}>*/}
            {/*    <Image*/}
            {/*        source={require('../images/icons/folder.png')}*/}
            {/*        style={{height: 35, width: 35}}*/}
            {/*    />*/}
            {/*</TouchableOpacity>*/}

            {/*<Text*/}
            {/*    style={{*/}
            {/*        fontSize: 10.8,*/}
            {/*        marginTop: '10%',*/}
            {/*        alignSelf: 'center',*/}
            {/*    }}>*/}
            {/*    YOUR DOCS*/}
            {/*</Text>*/}
        </View>
    )
}

const styles = StyleSheet.create({
    layout: {
        flex: 1,
        backgroundColor: "#FFF"
    },
    cardForForm: {
        height:60,
        backgroundColor: "#F2F2F2",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "5%",
        borderRadius: 10,
        padding: 15,
        flexDirection: "row"
    },
    cardForFormText: {
        fontSize: 18,
        paddingLeft: "4%"
    },
    cardForFormIconRight: {
        marginLeft: "auto",
        paddingTop: "1%"
    }
})

export default ChoiceForm
