import React from 'react';
import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Animated,
} from 'react-native';

import HomeHeader from './../components/HeaderHome';
import {connect} from 'react-redux';

class NumberVerification extends React.Component {
  constructor(props) {
    super(props);
    this.keyboardHeight = new Animated.Value(0);
    this.num1 = React.createRef();
    this.num2 = React.createRef();
    this.num3 = React.createRef();
    this.num4 = React.createRef();
    this.state = {
      textInput1: '',
      textInput2: '',
      textInput3: '',
      textInput4: '',
    };
  }
  inputNumber(value, flag) {
    const completeFlag = `num${flag}`;
    this.setState({[completeFlag]: value});
    flag = flag + 1;
    if (flag < 5 && value) {
      const nextFlag = `num${flag}`;
      const textInputToFocus = this[nextFlag];
      textInputToFocus.current.focus();
    }
  }
  render() {
    return (
      <View>
        <HomeHeader />
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginTop: 25,
          }}>
          <View>
            <Text style={{fontSize: 20, fontWeight: 'bold'}}>
              Enter Number to Verify
            </Text>
          </View>
          <TextInput
            maxLength={13}
            placeholder="264 813 048 4026"
            keyboardType="numeric"
            style={{
              borderBottomWidth: 1,
              width: 350,
              borderBottomColor: '#148567',
              color: 'black',
              fontSize: 20,
              marginTop: 20,
            }}
          />
        </View>
        <View
          style={{
            marginTop: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Text style={{fontSize: 20, alignSelf: 'center'}}>
            Enter Provider Verification Code
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'center',
            marginTop: 15,
          }}>
          <TextInput
            ref={this.num1}
            style={{
              borderBottomWidth: 1,
              width: 70,
              borderBottomColor: '#148567',
              textAlign: 'center',
              fontSize: 25,
            }}
            onChangeText={number => {
              this.inputNumber(number, 1);
              this.setState({textInput1: number});
            }}
            value={this.state.num1}
            keyboardType="numeric"
            numberOfLines={1}
            maxLength={1}
          />
          <TextInput
            ref={this.num2}
            style={{
              marginLeft: '6%',
              borderBottomWidth: 1,
              width: 70,
              borderBottomColor: '#148567',
              textAlign: 'center',
              fontSize: 25,
            }}
            onChangeText={number => {
              this.inputNumber(number, 2);
              this.setState({textInput2: number});
            }}
            value={this.state.num2}
            keyboardType="numeric"
            numberOfLines={1}
            maxLength={1}
          />
          <TextInput
            ref={this.num3}
            style={{
              marginLeft: '6%',
              borderBottomWidth: 1,
              width: 70,
              borderBottomColor: '#148567',
              textAlign: 'center',
              fontSize: 25,
            }}
            onChangeText={number => {
              this.inputNumber(number, 3);
              this.setState({textInput3: number});
            }}
            value={this.state.num3}
            keyboardType="numeric"
            numberOfLines={1}
            maxLength={1}
          />
          <TextInput
            ref={this.num4}
            style={{
              marginLeft: '6%',
              borderBottomWidth: 1,
              width: 70,
              borderBottomColor: '#148567',
              textAlign: 'center',
              fontSize: 25,
            }}
            onChangeText={number => {
              this.inputNumber(number, 4);
              this.setState({textInput4: number});
            }}
            value={this.state.num4}
            keyboardType="numeric"
            numberOfLines={1}
            maxLength={1}
          />
        </View>
        <View style={{marginTop: 30, alignItems: 'center'}}>
          <Text style={{fontSize: 18}}>or</Text>
          <Text style={{fontSize: 18}}>wait for friend to grant access</Text>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('home')}
            style={{
              marginTop: 30,
              width: 200,
              height: 50,
              backgroundColor: '#148567',
              justifyContent: 'center',
              alignItems: 'center',
              borderRadius: 10,
            }}>
            <Text style={{fontSize: 20, alignSelf: 'center', color: 'white'}}>
              Proceed
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}
const mapStateToProps = state => {
  return {
    addition: state.addReducer,
  };
};
export default connect(
  mapStateToProps,
)(NumberVerification);
