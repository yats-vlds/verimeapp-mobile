import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
  ScrollView,
  ActivityIndicator,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import {connect} from 'react-redux';
import Header from '../components/Header';
import Video from 'react-native-video';
import RNFetchBlob from 'rn-fetch-blob';
var RNFS = require('react-native-fs');
import Ican from 'react-native-vector-icons/Ionicons';
import moment from 'moment';

import {uploadVideo} from '@redux/onboarding/actions';

var runClock;

class video extends React.Component {
  constructor(props) {
    super(props);
    // this.willFocusSubscription = this.willFocusSubscription.bind(this);
    this.state = {
      flash: 'off',
      zoom: 0,
      autoFocus: 'on',
      camera: 'back',
      data: '',
      cameraType: 'back',
      torch: RNCamera.Constants.FlashMode.off,

      autoFocusPoint: {
        normalized: {x: 0.5, y: 0.5}, // normalized values required for autoFocusPointOfInterest
        drawRectPosition: {
          x: Dimensions.get('window').width * 0.5 - 32,
          y: Dimensions.get('window').height * 0.5 - 32,
        },
      },
      depth: 0,
      type: 'back',
      whiteBalance: 'auto',
      ratio: '16:9',
      recordOptions: {
        mute: false,
        quality: RNCamera.Constants.VideoQuality['720p'],
        mirrorVideo: false,
      },
      isRecording: false,
      canDetectFaces: false,
      canDetectText: false,
      canDetectBarcode: false,
      faces: [],
      textBlocks: [],
      barcodes: [],
      PP: true,
      retake: false,
      record: false,
      stopNext: false,
      openCam: false,
      checkVid: false,
      counter: moment()
        .hour(0)
        .minute(0)
        .second(0)
        .format('HH : mm : ss'),
      count: 0,
    };
  }

  callMe = () => {
    const str = this.state.data;
    const fileName = str.substr(str.indexOf('Camera') + 7, str.indexOf('.mp4'));
    const {fs} = RNFetchBlob;
    //console.log('path',path)
    // console.log('filename',fileName)
    RNFS.copyFile(
      this.state.data,
      RNFS.PicturesDirectoryPath + '/Videos/' + fileName,
    ).then(
      () => {
        //  console.log("Video copied locally!!");
      },
      () => {
        // console.log("CopyFile fail for video: " + error);
      },
    );
  };
  // componentWillMount() {

  //     this.willFocusSubscription = this.props.navigation.addListener(
  //         'willFocus',
  //         () => {
  //             alert('back')
  //         }
  //     );
  // }

  // componentWillUnmount() {
  //     this.willFocusSubscription.remove();
  // }
  // willFocusSubscription() {
  //     alert('call me')

  // }
  render() {
    // alert(this.state.counter)

    return (
      <View style={{flex: 1}}>
        {!this.state.openCam ? (
          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              flexGrow: 1,
            }}>
            <View
              style={{
                flexDirection: 'column',
                marginTop: 40,
                marginLeft: 40,
                marginRight: 40,
                marginBottom: 40,
                flex: 1,
              }}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <View style={{width: '90%'}}>
                  <Header value={this.props.navigation} title="Video" />
                </View>
              </View>
              {this.state.data ? (
                <View
                  style={{
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginTop: 190,
                  }}>
                  <View style={{alignItems: 'center', width: 4}}>
                    <Video
                      source={{uri: this.state.data}} // Can be a URL or a local file.
                      ref={ref => {
                        this.player = ref;
                      }}
                      // Store reference
                      onBuffer={this.onBuffer} // Callback when remote video is buffering
                      onError={this.videoError}
                      paused={this.state.PP}
                      resizeMode="cover"
                      onEnd={() => this.setState({PP: true})}
                      // poster="http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/images/BigBuckBunny.jpg"
                      // Callback when video cannot be loaded
                      style={styles.backgroundVideo}
                    />
                    <TouchableOpacity
                      onPress={() => {
                        if (this.state.PP) {
                          this.setState({PP: false});
                        } else {
                          this.setState({PP: true});
                        }
                      }}
                      style={styles.videoButton}>
                      {!this.state.PP ? (
                        <Text style={{color: 'white'}}>Stop</Text>
                      ) : (
                        <Text style={{color: 'white'}}>Play</Text>
                      )}
                    </TouchableOpacity>
                  </View>

                  {this.state.data ? (
                    <View style={{marginTop: 25}}>
                      <View
                        style={{
                          flexDirection: 'row',
                          justifyContent: 'flex-start',
                        }}>
                        <Icon name="copy" size={30} color="#18ad86" />
                        <View style={{marginLeft: '5%', alignItems: 'center'}}>
                          <Text style={{fontSize: 16}}>
                            Do not take photo of screen
                          </Text>
                          {/* <Text style={{ fontSize: 16 }}>Take scan in well lit area </Text> */}
                        </View>
                      </View>
                      <View style={{flexDirection: 'row', marginTop: '3%'}}>
                        <Icon name="info-circle" size={30} color="#18ad86" />
                        <View
                          style={{marginLeft: '5%', justifyContent: 'center'}}>
                          <Text style={{fontSize: 16, textAlign: 'center'}}>
                            Take scan in well lit area{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                  ) : null}
                </View>
              ) : (
                <View>
                  <TouchableOpacity
                    onPress={() => this.setState({openCam: true})}
                    style={{
                      height: 330,
                      justifyContent: 'center',
                      alignItems: 'center',
                      borderWidth: 1,
                      backgroundColor: '#dedede',
                      marginTop: 25,
                    }}>
                    <Text style={{fontSize: 20, color: 'grey'}}>
                      Tab To Capture Video
                    </Text>
                  </TouchableOpacity>
                  <View
                    style={{
                      marginTop: 25,
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}>
                    <View
                      style={{
                        flexDirection: 'row',
                        justifyContent: 'flex-start',
                      }}>
                      <Icon name="copy" size={30} color="#18ad86" />
                      <View style={{marginLeft: '5%', alignItems: 'center'}}>
                        <Text style={{fontSize: 16}}>
                          Do not take photo of screen
                        </Text>
                        {/* <Text style={{fontSize: 16}}>Take scan in well lit area </Text> */}
                      </View>
                    </View>
                    <View style={{flexDirection: 'row', marginTop: '3%'}}>
                      <Icon name="info-circle" size={30} color="#18ad86" />
                      <View
                        style={{marginLeft: '5%', justifyContent: 'center'}}>
                        <Text style={{fontSize: 16, textAlign: 'center'}}>
                          Take scan in well lit area{' '}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              )}
              <View style={{alignItems: 'center', marginTop: 25}}>
                {!this.state.record ? (
                  <View>
                    <TouchableOpacity
                      onPress={() => this.setState({openCam: true})}
                      style={styles.start}>
                      <Text style={{color: 'white', fontSize: 20}}>
                        Record Video
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : (
                  <View>
                    {// !this.state.stopNext ?

                    //     <View>
                    //         <TouchableOpacity onPress={() => {this.stopVideo(); this.setState({stopNext: true,retake: true,})}} style={styles.start}>
                    //             <Text style={{color: 'white',fontSize: 20}}>Stop Video</Text></TouchableOpacity></View> :

                    this.props.loading ? (
                      <ActivityIndicator size="large" color="black" />
                    ) : (
                      <View
                        style={{
                          alignItems: 'center',
                          flexDirection: 'row',
                          justifyContent: 'center',
                        }}>
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({
                              openCam: true,
                              retake: false,
                              data: '',
                              record: false,
                              stopNext: false,
                              PP: true,
                            });
                          }}
                          style={styles.start}>
                          <Text style={{color: 'white', fontSize: 20}}>
                            Retake Video
                          </Text>
                        </TouchableOpacity>
                        {this.state.data ? (
                          <TouchableOpacity
                            onPress={() => {
                              this.props.uploadVideo(
                                this.state.data
                              );
                            }}
                            style={styles.start2}>
                            <Text style={{color: 'white', fontSize: 20}}>
                              Next
                            </Text>
                          </TouchableOpacity>
                        ) : null}
                      </View>
                    )}
                  </View>
                )}
              </View>
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'column',
                  flexGrow: 1,
                  justifyContent: 'flex-end',
                  marginTop: 25,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 50,
                      backgroundColor: '#63d4b6',
                      opacity: 0.7,
                    }}
                  />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleOne} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                </View>
              </View>
              {/* {this.state.data ? <Video source={{uri: this.state.data}}
                    // Can be a URL or a local file.
                    ref={(ref) => {
                        this.player = ref
                    }}                                      // Store reference
                    onBuffer={this.onBuffer}                // Callback when remote video is buffering
                    onError={this.videoError}               // Callback when video cannot be loaded
                    style={styles.backgroundVideo} /> : null} */}
            </View>
          </ScrollView>
        ) : (
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            flashMode={this.state.torch}
            type={this.state.cameraType}
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            onGoogleVisionBarcodesDetected={({}) => {}}>
            <View
              style={{
                flexDirection: 'column',
                flex: 1,
              }}>
              {!this.state.checkVid ? (
                <TouchableOpacity
                  onPress={() => this.takeVideo()}
                  style={styles.camerButtonOutline}>
                  <View style={styles.camerButton} />
                </TouchableOpacity>
              ) : (
                <TouchableOpacity
                  onPress={() => this.stopVideo()}
                  style={styles.camerButtonOutline}>
                  <View style={styles.camerButton2} />
                </TouchableOpacity>
              )}

              <View style={styles.timer}>
                <Text style={{color: 'white'}}>{this.state.counter}</Text>
              </View>
              {!this.state.checkVid ? (
                <View
                  style={{
                    position: 'absolute',
                    alignSelf: 'flex-end',
                    top: 10,
                    right: 20,
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.cameraType == 'back') {
                        this.setState({cameraType: 'front'});
                      } else {
                        this.setState({cameraType: 'back'});
                      }
                    }}>
                    <Image
                      source={require('../images/icons/rotate.png')}
                      style={{height: 45, width: 45}}
                      resizeMode="contain"
                    />
                  </TouchableOpacity>
                </View>
              ) : null}

              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: 55,
                  alignSelf: 'flex-end',
                  right: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 30,
                  backgroundColor: '#00000080',
                  height: 50,

                  width: 50,
                }}
                onPress={() => {
                  if (this.state.torch == RNCamera.Constants.FlashMode.off) {
                    this.setState({torch: RNCamera.Constants.FlashMode.torch});
                  } else {
                    this.setState({torch: RNCamera.Constants.FlashMode.off});
                  }
                }}>
                <Ican name="ios-flashlight" size={28} color="white" />
              </TouchableOpacity>
            </View>
          </RNCamera>
        )}
      </View>
    );
  }

  takeVideo = async () => {
    this.setState({checkVid: true});
    runClock = setInterval(() => {
      this.displayTime();
    }, 1000);

    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && !isRecording) {
      try {
        const promise = this.camera.recordAsync(this.state.recordOptions);

        if (promise) {
          const data = await promise;

          this.setState({isRecording: false, data: data.uri, openCam: false});

          // console.log(this.state.counter)
        } else {
          return <ActivityIndicator size="small" color="black" />;
        }
      } catch (e) {
        console.error(e);
      }
    }
  };
  stopVideo = async () => {
    await this.camera.stopRecording();
    this.setState({
      checkVid: false,
      counter: moment()
        .hour(0)
        .minute(0)
        .second(0)
        .format('HH : mm : ss'),
      count: 0,
    });
    clearInterval(runClock);

    const {isRecording} = this.state;
    if (!this.state.record) {
      this.setState({record: true});
    }

    if (this.camera && isRecording) {
      try {
        this.setState({isRecording: false});
      } catch (e) {
        console.error(e);
      }
    }
  };
  displayTime() {
    if (this.state.checkVid) {
      this.setState({
        counter: moment()
          .hour(0)
          .minute(0)
          .second((this.state.count = this.state.count + 1))
          .format('HH : mm : ss'),
      });
    } else {
      this.setState({
        counter: moment()
          .hour(0)
          .minute(0)
          .second(0)
          .format('HH : mm : ss'),
        count: 0,
      });
    }
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    // height: 85,
    // width: '90%',
    flex: 1,
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
  backgroundVideo: {
    // position: 'absolute',
    // top: 0,
    // left: 0,
    // bottom: 0,
    // right: 0,
    marginTop: -170,
    height: 400,
    width: 350,
    backgroundColor: 'black',
  },
  videoButton: {
    height: 50,
    width: 100,
    backgroundColor: '#04c491',
    marginTop: 30,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  camerButtonOutline: {
    borderColor: 'white',
    borderRadius: 50,
    borderWidth: 3,
    height: 70,
    width: 70,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 35,
  },
  camerButton: {
    backgroundColor: 'white',
    borderRadius: 50,

    flex: 1,
    margin: 3,
  },
  camerButton2: {
    backgroundColor: 'red',
    borderRadius: 50,

    flex: 1,
    margin: 3,
  },
  flasLight: {
    alignSelf: 'flex-end',
    position: 'absolute',
    bottom: 60,
    right: 50,
    borderRadius: 50,
  },
  timer: {
    alignSelf: 'center',
    position: 'absolute',
  },
});
const mapStateToProps = state => {
  const {loading} = state.onboarding;
  return {
    loading
  };
};
export default connect(
  mapStateToProps,
  {uploadVideo},
)(video);
