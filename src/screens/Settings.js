import React from 'react';
import {View, Text, TouchableOpacity} from 'react-native';
import HomeHeader from './../components/HeaderHome';
import {useDispatch} from 'react-redux';

import {handleLogout} from '@redux/onboarding/actions';

export default () => {

  const dispatch = useDispatch();
  return (
    <View>
        <HomeHeader />

        <View style={{marginTop: 20, alignItems: 'center'}}>
          <Text style={{fontSize: 20, alignSelf: 'center', fontWeight: 'bold'}}>
            Settings
          </Text>
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <TouchableOpacity
            onPress={() => dispatch(handleLogout())}
            style={{
              height: 45,
              width: '80%',
              borderRadius: 3,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 40,
            }}>
            <Text>LOGOUT</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            width: '100%',
          }}>
          <TouchableOpacity
            style={{
              height: 45,
              width: '80%',
              borderRadius: 3,
              backgroundColor: 'white',
              justifyContent: 'center',
              alignItems: 'center',
              marginTop: 20,
            }}>
            <Text>INSURANCE</Text>
          </TouchableOpacity>
        </View>
      </View>
  )
}