import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';

import {connect} from 'react-redux';

import {
  navigateToStep
} from '@redux/onboarding/actions';

class Welcome extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
        }}>
        <View>
          <View style={{alignItems: 'center', marginTop: 30}}>
            <Text style={styles.text}>Welcome</Text>
          </View>
        </View>
        <View
          style={{justifyContent: 'flex-end', marginBottom: 40, flexGrow: 1}}>
          <View style={{alignItems: 'center'}}>
            <TouchableOpacity
              onPress={() => {
                if (this.props.step != null && this.props.step != 0) {
                  this.props.navigateToStep();
                  return;
                }
                this.props.navigation.navigate('EnterPhone')
              }}
              style={styles.start}>
              <Text style={{color: 'white', fontSize: 20}}>Get Started</Text>
            </TouchableOpacity>
          </View>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              marginTop: 25,
            }}>
            {/* TODO: Extract them to component */}
            <View style={styles.singleOne} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});


const mapStateToProps = state => {
  return {
    step: state.onboarding.step,
  };
};

export default connect( mapStateToProps, { navigateToStep } )(Welcome);
