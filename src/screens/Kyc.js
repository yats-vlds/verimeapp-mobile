import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';

import Header from '../components/Header';

export default class Kyc extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    var rowComponent = (imageUrl, text) => (
      <View style={{flexDirection: 'row', marginTop: 20}}>
        <View
          style={{justifyContent: 'center', height: 50, alignItems: 'center'}}>
          <Image
            source={imageUrl}
            style={{height: 30, width: 40}}
            resizeMode="contain"
          />
        </View>
        <View style={{justifyContent: 'center', marginLeft: 15}}>
          <Text style={{fontSize: 20}}>{text}</Text>
        </View>
      </View>
    );

    return (
      <View
        style={{
          flex: 1,
          flexDirection: 'column',
          marginTop: 25,
          marginRight: 25,
          marginLeft: 25,
        }}>
        <Header value={this.props.navigation} title="How to get your kyc" />

        <View
          style={{marginTop: 40, flexDirection: 'column', marginLeft: '20%'}}>
          <View>
            {rowComponent(require('../images/icons/eye.png'), 'Scan ID')}
            {rowComponent(
              require('../images/icons/video-camera.png'),
              'Take Videos ',
            )}
            {rowComponent(require('../images/icons/info.png'), 'Personal Info')}
            {rowComponent(
              require('../images/icons/confirm.png'),
              'Address Confirmation',
            )}
            {rowComponent(
              require('../images/icons/fingerPrint.png'),
              'Finger Print',
            )}
          </View>
        </View>

        <View style={{marginTop: 25, alignItems: 'center'}}>
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate('Scanner')}
            style={styles.start}>
            <Text style={{color: 'white', fontSize: 20}}>Continue</Text>
          </TouchableOpacity>
        </View>

        <View
          style={{
            alignItems: 'center',
            flexDirection: 'column',
            flexGrow: 1,
            marginBottom: 40,
            justifyContent: 'flex-end',
          }}>
          <View style={{flexDirection: 'row'}}>
            <View
              style={{
                height: 10,
                width: 10,
                borderRadius: 50,
                backgroundColor: '#63d4b6',
                opacity: 0.7,
              }}
            />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleOne} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
            <View style={styles.singleDesign} />
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});
