import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  ScrollView,
  Dimensions,
  ActivityIndicator,
  BackHandler,
} from 'react-native';
import {WebView} from 'react-native-webview';
import Toast from 'react-native-simple-toast';
import {connect} from 'react-redux';
import {RadioButton} from 'react-native-paper';


import Header from '../components/Header';

import Constants from '../Constants';


import {postalVerification, requestPaymentUrl, getUser} from '@redux/onboarding/actions';

const SERVER_URL = Constants.SERVER_URL;

class PaymentVerification extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      firstname: '',
      surname: '',
      creditCard: '',
      day: '',
      year: '',
      cvv: '',
      value: 0,
      address1: '',
      addressOptional: '',
      city: '',
      zipCode: '',
      email: '',
      first: true,
      second: false,
      openWeb: false,
    };
    this.WEBVIEW_REF = React.createRef();
  }
  componentDidMount = async () => {

    if (this.props.personalInfo != null) {
      this.setState({
        firstname: this.props.personalInfo.firstname,
        surname: this.props.personalInfo.surname,
        email: this.props.personalInfo.email,
      })
    }

    BackHandler.addEventListener(
      'hardwareBackPress',
      this.handleBackButton
    );
  };

  componentWillUnmount = () => {
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  };

  handleBackButton = async () => {
    if (this.state.openWeb) {
      this.setState({openWeb: false});
      return true;
    }
    this.props.navigation.goBack(null);
  };

  render() {
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flex: 1,
            flexDirection: 'column',
            marginTop: 25,
            marginLeft: 25,
            marginRight: 25,
          }}>
          <Header
            gob={null}
            value={this.props.navigation}
            title="Payment Verification"
          />

          <View style={{alignItems: 'center', marginTop: 20}}>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Text>Postal Address: </Text>
              <RadioButton
                value={this.state.first}
                status={this.state.first ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({first: true, second: false});
                }}
              />
              <Text>Credit Card: </Text>
              <RadioButton
                value={this.state.second}
                status={this.state.second ? 'checked' : 'unchecked'}
                onPress={() => {
                  this.setState({first: false, second: true});
                  this.props.requestPaymentUrl();
                }}
              />
            </View>
            {this.state.first ? (
              <View>
                <View>
                  <TextInput
                    value={ this.state.firstname }
                    onChangeText={text => this.setState({firstname: text})}
                    placeholder="First Name"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '5%',
                      fontSize: 18,
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    value={ this.state.surname }
                    onChangeText={text => this.setState({surname: text})}
                    placeholder="Last Name"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    onChangeText={text => this.setState({address1: text})}
                    value={this.state.address1}
                    maxLength={100}
                    placeholder="Address line 1"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    onChangeText={text =>
                      this.setState({addressOptional: text})
                    }
                    value={this.state.addressOptional}
                    maxLength={100}
                    placeholder="Address Optional"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                  />
                </View>

                <View>
                  <TextInput
                    onChangeText={text => this.setState({city: text})}
                    value={this.state.city}
                    maxLength={20}
                    placeholder="City"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                  />
                </View>
                <View>
                  <TextInput
                    onChangeText={text => this.setState({zipCode: text})}
                    value={this.state.zipCode}
                    maxLength={10}
                    placeholder="Zip Code"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                    keyboardType="numeric"
                  />
                </View>
                <View>
                  <TextInput
                    onChangeText={text => this.setState({email: text})}
                    value={this.state.email}
                    maxLength={20}
                    placeholder="Email"
                    style={{
                      borderBottomWidth: 1,
                      borderColor: '#63d4b6',
                      width: 300,
                      marginTop: '4%',
                      fontSize: 18,
                    }}
                    keyboardType="email-address"
                  />
                </View>
                <View
                  style={{
                    alignItems: 'center',
                    justifyContent: 'flex-start',
                    marginBottom: 50,
                  }}>
                  {this.props.loading ? (
                    <ActivityIndicator
                      size="large"
                      color="black"
                      style={{marginTop: 25}}
                    />
                  ) : (
                    <TouchableOpacity
                      onPress={() => {
                        if (
                          this.state.firstname,
                          this.state.surname,
                          this.state.address1 &&
                          this.state.city &&
                          this.state.zipCode &&
                          this.state.email
                        ) {
                          this.props.postalVerification({
                            firstname: this.state.firstname,
                            surname: this.state.surname,
                            address1: this.state.address1,
                            addressOptional: this.state.addressOptional,
                            city: this.state.city,
                            zipCode: this.state.zipCode,
                            email: this.state.email,
                          });
                        } else {
                          Toast.show('Field is missing', Toast.SHORT);
                        }
                      }}
                      style={styles.start}>
                      <Text style={{color: 'white', fontSize: 20}}>
                        Confirm
                      </Text>
                    </TouchableOpacity>
                  )}
                </View>
              </View>
            ) : (
              <View>
                {this.props.payment_url ? (
                  <>
                  {this.state.openWeb ? (
                      <WebView
                        source={{ uri: this.props.payment_url }}
                        onNavigationStateChange={async e => {
                          if (e.canGoBack) {
                            this.props.getUser();
                          } else {
                          }
                        }}
                        style={{
                          height: Dimensions.get('window').height - 160,
                          width: Dimensions.get('window').width,
                        }}
                      />
                  ) : null }
                    <TouchableOpacity
                      onPress={() => {
                        this.setState({openWeb: true})
                      }}
                      style={[
                        styles.start,
                        {marginTop: Dimensions.get('screen').height / 3},
                      ]}>
                      <Text style={{color: 'white', fontSize: 20}}>
                        Proceed To payment
                      </Text>
                    </TouchableOpacity>
                  </>
                ) : <ActivityIndicator
                size="large"
                color="black"
                style={{marginTop: 25}}
                /> }

              </View>
            )}
            {this.state.first ? (
              <View
                style={{
                  flexDirection: 'column',
                  alignItems: 'center',
                  justifyContent: 'flex-end',
                  marginBottom: 40,
                  flexGrow: 1,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 50,
                      backgroundColor: '#63d4b6',
                      opacity: 0.7,
                    }}
                  />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleOne} />
                </View>
              </View>
            ) : null}
          </View>
        </View>
      </ScrollView>
    );
  }
}
const styles = StyleSheet.create({
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: '7%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
});
const mapStateToProps = state => {
  const {loading, personalInfo, payment_url} = state.onboarding;
  return {
    loading,
    personalInfo,
    payment_url,
  };
};
export default connect(
  mapStateToProps,
  {
    postalVerification,
    requestPaymentUrl,
    getUser,
  },
)(PaymentVerification);
