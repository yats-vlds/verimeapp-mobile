import React, { useState, useRef,useEffect } from "react";
import {
    View,
    Image,
    Text,
    StyleSheet,
    Button,
    ScrollView,
} from "react-native";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import IconAntDesign from "react-native-vector-icons/AntDesign";

import DocumentPicker from 'react-native-document-picker';
import { launchImageLibrary } from "react-native-image-picker";
import _ from "lodash";
import SelectMultiple from "react-native-select-multiple";
import { TextInput } from "react-native";
import { TouchableOpacity } from "react-native";
import RadioButtonRN from "radio-buttons-react-native";
import {HeaderChatbot} from "./HeaderChatbot";
import axios from 'axios';
import {HeaderChoiceForm} from "./HeaderChoiceForm";
import IconFoundation from "react-native-vector-icons/Foundation";
import IconRight from "react-native-vector-icons/AntDesign";



const styles = StyleSheet.create({
    scroll: {
        backgroundColor: "#FFF",
    },
    chatBot: {
        flex: 1,
        alignItems: "center",
        backgroundColor: "#FFF",
    },
    iconProfile: {
        marginTop: "30%",
        marginBottom: "5%",
        width: 125,
        height: 121,
        borderRadius: 20
    },
    hr: {
        borderBottomWidth: 1,
        borderBottomColor: "#47A897",
        width: 160.18,
        marginBottom: 13,
    },
    titleName: {
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "bold",
        fontSize: 15,
        textAlign: "center",
        color: "#000",
    },
    subtitle: {
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontSize: 14,
        textAlign: "center",
        color: "#000",
        marginBottom: 23,
    },
    textStatus: {
        width: 261,
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: 15,
        textAlign: "center",
        color: "#666666",
        marginBottom: 23,
    },
    questionSection: {
        flex: 1,
        flexDirection: "column",
        width: "100%",
        marginLeft: 10,
    },
    questionSection2: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        width: "90%",
        marginLeft: 10,
    },
    questionIcon: {
        width: 44.57,
        height: 43.02,
        borderRadius: 50
    },
    questionMessage: {
        fontFamily: "Roboto",
        fontStyle: "normal",
        fontWeight: "normal",
        fontSize: 15,
        color: "#000",
        marginLeft: "0.7%",
        marginTop: "2%",
        marginBottom: "1%",
        backgroundColor: "#F2F2F2",
        alignSelf:"flex-start",
        padding: "1.5%"
    },
    blockAnswerOnQuestion1: {
        marginTop: 20,
        alignItems: "baseline",
        height: 37,
        backgroundColor: "#47A897",
        borderRadius: 18.5,
        marginLeft: "auto",
        marginRight: 25,
        marginBottom: 20,
    },
    answerOnQuestion1: {
        fontFamily: "Roboto",
        fontWeight: "400",
        fontStyle: "normal",
        fontSize: 14,
        textAlign: "center",
        padding: 6,
        paddingLeft: 10,
        paddingRight: 10,
        color: "#ffffff",
    },
    cardForForm: {
        height:60,
        backgroundColor: "#F2F2F2",
        width: "80%",
        marginLeft: "auto",
        marginRight: "auto",
        marginTop: "5%",
        borderRadius: 10,
        padding: 15,
        flexDirection: "row"
    },
    cardForFormText: {
        fontSize: 18,
        paddingLeft: "4%"
    },
    cardForFormIconRight: {
        marginLeft: "auto",
        paddingTop: "1.3%"
    },
    buttonSend: {
        width: "70%"
    }
});



export const Chatbot = ({navigation}) => {
    let transformationValue = (para) => {
        const arr = [];
        para.forEach(el => arr.push(el.value));
        return arr.join(", ");
    };


    console.disableYellowBox = true;
    const scrollViewRef = useRef();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const [counter,setCounter] = useState(0);
    const [setid,setSetid] = useState(0)
    const [questions,setQuestions] = useState([]);
    const [answers,setAnswers] = useState([]);
    const [checkanswer,setCheckanswer] =useState([]);
    const [text,setText]= useState("");
    const [loading,setLoading]= useState(false);
    const [avatar,setAvatar] = useState("")
    const [showlist,setShowlist] = useState(true)
    const [forms,setForms] = useState([])

    useEffect(()=>{
        axios
        .get("http://testing.redboxtech.co/api/get/questions-sets")
        .then(function (res) {
            if(res.data.status==="success"){
                console.log("forms",res.data);
                setForms(res.data.data)
            }
        })
        .catch(function (error) {
            console.debug(error)
        });
    },[]);

    const showForm=(BUid)=>{
        axios
        .get("http://testing.redboxtech.co/api/questions/get/"+BUid)
        .then(function (res) {
            if(res.data.status==="success"){
                console.log("question",res.data);
                setSetid(res.data.data.setId);
                setQuestions(res.data.data.questionsList);
                setAvatar(res.data.data.businessIcon);
                setShowlist(false)
            }
        })
        .catch(function (error) {
            console.debug(error)
        });
    }

    const submitForm = () => {
        let form = new FormData();
        form.append("setId",setid)
        questions.map((item,i)=>{
            if(item.type==="text"||item.type==="textarea"||item.type==="date"||item.type==="file"){
                form.append(`questionSet[${i}][Qid]`,item.Qid);
                form.append(`questionSet[${i}][ansType]`,item.type);
                form.append(`questionSet[${i}][answer]`,answers[i]);
                form.append(`questionSet[${i}][answerId]`,"");
            }
            else if(item.type==="radio"||item.type==="checkbox"){
                let answerId="";
                item.options.map((x,j)=>{
                    if(answers[i].includes(x.option)){
                        if(!answerId)
                        answerId=answerId+x.answerId;
                        else
                        answerId=answerId+","+x.answerId;
                    }
                })
                form.append(`questionSet[${i}][Qid]`,item.Qid);
                form.append(`questionSet[${i}][ansType]`,item.type);
                form.append(`questionSet[${i}][answer]`,"");
                form.append(`questionSet[${i}][answerId]`,answerId);
            }

        })
        console.log(form, "submit");
        if(!loading){
            setLoading(!loading);

            axios
            .post("http://testing.redboxtech.co/api/questions/save/response",form)
            .then(function (res) {
                if(res.data.status==="success"){
                    console.debug("Success")
                }
                setLoading(!loading);
            })
            .catch(function (error) {
                console.debug(error);
                setLoading(!loading);
            });
        }

    };


    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = (date) => {
        let temp=[...answers]
        temp.push(date.toString().slice(4, 16));
        setAnswers(temp);
        setCounter(counter+1);
        hideDatePicker();
    };

    const handleCheckSubmit = () => {
            setTimeout(() => {
            let temp=[...answers];
            temp.push(transformationValue(checkanswer));
            setAnswers(temp)
            setCheckanswer([]);
            setCounter(counter+1)
            }, 200);
    };




    const refreshHandler = () => {
        setCounter(0);
        setAnswers([]);
    };

    const chooseFile =  async (type) => {



        try {
            const res = await DocumentPicker.pick({
              type: [DocumentPicker.types.allFiles],
            });
            let temp=[...answers];
            temp.push(res);
            await setAnswers(temp);
            setCounter(counter+1);
            console.log(
              res.uri,
              res.type, // mime type
              res.name,
              res.size
            );
          } catch (err) {
            if (DocumentPicker.isCancel(err)) {
              // User cancelled the picker, exit any dialogs or menus and move on
            } else {
              throw err;
            }
          }


    };

    const handleOptions=(options)=>{
            let o=options.map((item)=>{
                return {
                    label:item.option,
                    value:item.option
                }
            })
            return o;
    }


    return (
        <>
            <ScrollView contentContainerStyle={styles.scroll}
                        ref={scrollViewRef}
                        onContentSizeChange={() => scrollViewRef.current.scrollToEnd({ animated: true })}
                        stickyHeaderIndices={[0]}
            >

                {showlist?<View/>:<HeaderChoiceForm name={"Assistant"} refreshHandler={refreshHandler} navigation={navigation} leftArrow={"home"} />}

                {showlist?
                        <View style={{ flex: 1, height: 700 }}>
                            {forms.length?<HeaderChoiceForm name={"Forms"} navigation={navigation} leftArrow={"home"} /> :null}
                            <View>
                            {forms.map((item,i)=>
                            <TouchableOpacity onPress={()=>showForm(item.id)}>
                                <View style={styles.cardForForm}>
                                    <IconFoundation name="clipboard-notes" size={25} color="#000000"/>
                                    <Text style={styles.cardForFormText}>{item.title.length > 25 ? item.title.substring(0, 20) + "..." : item.title}</Text>
                                    <IconRight name="right" size={20} color="#000000" style={styles.cardForFormIconRight}/>
                                </View>
                            </TouchableOpacity>
                            )}
                            </View>
                        </View>
                        :
                <View style={{ flex: 1 }}>
                    <View style={styles.chatBot}>
                        <Image
                            source={{uri: avatar}}
                            style={styles.iconProfile}
                        />
                        <Text style={styles.titleName}>
                            Sasha Green
                        </Text>
                        <Text style={styles.subtitle}>
                            Your personal assistant
                        </Text>
                    </View>

                    {answers.map((item,i)=>{

                        return(
                        <>
                        <View style={styles.questionSection}>
                            <Image
                                source={{uri: avatar}}
                                style={styles.questionIcon}
                            />
                            <Text style={styles.questionMessage}>{questions[i].question}</Text>
                        </View>

                        {item.uri?
                        <View style={styles.blockAnswerOnQuestion1}>
                            <Text style={styles.answerOnQuestion1}>{item.name}</Text>
                        </View>
                        :
                        <View style={styles.blockAnswerOnQuestion1}>
                            <Text style={styles.answerOnQuestion1}>{item}</Text>
                        </View>
                        }
                        {questions.length-1===i&&
                            <View style={styles.questionSection}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage} onClick={submitForm()}>Thank you, mister! Good job!
                                </Text>
                            </View>
                        }
                        </>
                        )

                    })}
                    {questions.map((que,index)=>{
                        if(index ===counter){
                            if(que.type==='radio'){
                                return(
                            <>
                                <View style={styles.questionSection}>
                                    <Image
                                        source={{uri: avatar}}
                                        style={styles.questionIcon}
                                    />
                                    <Text style={styles.questionMessage}>{que.question}</Text>
                                </View>
                            <View style={{ marginBottom: "5%", marginTop: "5%" }}>
                                <RadioButtonRN
                                    data={handleOptions(que.options)}
                                    selectedBtn={(e) => {
                                        setTimeout(() => {
                                            let temp=[...answers];
                                            temp.push(e.value);
                                            setAnswers(temp)
                                            setCounter(counter+1);
                                        }, 500);
                                    }}
                                    boxDeactiveBgColor={"#FFF"}
                                    boxActiveBgColor={"#FFF"}
                                    circleSize={7}
                                    deactiveColor={"red"}
                                    boxStyle={{ marginTop: 0, borderRadius: 0, borderWidth: 0 }}
                                />
                            </View>
                            </>
                                )
                            }
                            if(que.type==='file'){
                                return(
                                    <View>
                                        <View style={styles.questionSection}>
                                        <Image
                                            source={{uri: avatar}}
                                            style={styles.questionIcon}
                                        />
                                        <Text style={styles.questionMessage}>{que.question}</Text>
                                        </View>
                                        <TouchableOpacity
                                            style={{ justifyContent: "center", alignItems: "center", backgroundColor: "#48AC98", margin: 13.5, padding: "1.5%", flexDirection: "row" }}
                                            activeOpacity={0.5}
                                            onPress={() => chooseFile("photo")}>
                                                <IconAntDesign name="upload" size={25} color="#FFF"/>
                                                <Text style={{color: "#FFF"}}>{ "  "}UPLOAD</Text>
                                        </TouchableOpacity>
                                    </View>
                                )
                            }
                            if(que.type==='date'){
                                return(
                                    <View>
                                        <View style={styles.questionSection}>
                                        <Image
                                            source={{uri: avatar}}
                                            style={styles.questionIcon}
                                        />
                                        <Text style={styles.questionMessage}>{que.question}</Text>
                                        </View>
                                        <View>
                                            <View style={{margin: 13.5}}>
                                                <Button title="Show Calendar" color="#48AC98" onPress={showDatePicker}/>
                                            </View>
                                            <DateTimePickerModal
                                                isVisible={isDatePickerVisible}
                                                mode="date"
                                                onConfirm={handleConfirm}
                                                onCancel={hideDatePicker}
                                            />
                                        </View>
                                    </View>
                                )
                            }
                            if(que.type==='checkbox'){
                                return(
                                    <View>
                                        <View style={styles.questionSection}>
                                        <Image
                                            source={{uri: avatar}}
                                            style={styles.questionIcon}
                                        />
                                        <Text style={styles.questionMessage}>{que.question}</Text>
                                        </View>
                                        <View style={{
                                            marginBottom: "0%",
                                            marginTop: "2%",
                                        }}>
                                            <SelectMultiple
                                                items={handleOptions(que.options)}
                                                selectedItems={checkanswer}
                                                onSelectionsChange={(s)=>setCheckanswer(s)}
                                                selectedRowStyle={{ backgroundColor: "#FFF" }}
                                                rowStyle={{
                                                    backgroundColor: "#FFF",
                                                    borderBottomWidth: 0,
                                                }}
                                                checkboxStyle={{ backgroundColor: "#FFF" }}
                                                selectedCheckboxStyle={{ backgroundColor: "#48AC98", opacity: 0.4}}
                                            />
                                            <View style={{margin: 13.5}}>
                                                <Button
                                                onPress={handleCheckSubmit}
                                                title="submit"
                                                style={styles.buttonSend}
                                                color="#48AC98"
                                            ></Button>
                                            </View>
                                        </View>
                                    </View>
                                )
                            }
                            if(que.type==='text'||que.type==='textarea'){
                                return(
                                    <View>
                                        <View style={styles.questionSection}>
                                        <Image
                                            source={{uri: avatar}}
                                            style={styles.questionIcon}
                                        />
                                        <Text style={styles.questionMessage}>{que.question}</Text>
                                        </View>
                                        <TextInput
                                                style={{
                                                    height: 40,
                                                    borderColor: "#D4D4D4",
                                                    borderWidth: 1,
                                                    margin: "3.2%",
                                                    padding: 5,
                                                    paddingLeft: 10,
                                                }}
                                                onChangeText={tex => setText(tex)}
                                                value={text}
                                                onBlur={() => {
                                                    let temp=[...answers];
                                                    temp.push(text);
                                                    setText("")
                                                    setAnswers(temp);
                                                    setCounter(counter+1);
                                                }}
                                                placeholderTextColor={"gray"}
                                                placeholder={"Start typing..."}
                                                autoFocus={true}
                                                selectionColor={"red"}
                                            />
                                    </View>
                                )
                            }

                        }
                    })
                    }
                    {/* {!answerOnQuestion1? (
                        <View style={{ marginBottom: "5%", marginTop: "5%" }}>
                            <RadioButtonRN
                                data={data3}
                                selectedBtn={(e) => {
                                    setTimeout(() => {
                                        console.log(e.value);
                                        setAnswerOnQuestion1(e.value);
                                    }, 500);
                                }}
                                boxDeactiveBgColor={"#E5E5E5"}
                                boxActiveBgColor={"#C2DEEA"}
                                circleSize={3}
                                deactiveColor={"red"}
                                boxStyle={{ marginTop: 0, borderRadius: 0, borderWidth: 0 }}
                            />
                        </View>
                    ) : (
                        <>
                            <View style={styles.blockAnswerOnQuestion1}>
                                <Text style={styles.answerOnQuestion1}>{answerOnQuestion1}</Text>
                            </View>
                            <View style={styles.questionSection2}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage}>To start, I`ll need you home address</Text>
                            </View>
                        </>
                    )}
                    {!answerOnQuestion2Blur && answerOnQuestion1 && (<TextInput
                        style={{
                            height: 40,
                            borderColor: "#D4D4D4",
                            borderWidth: 1,
                            margin: "5%",
                            padding: 5,
                            paddingLeft: 10,
                            marginBottom: "2%",
                        }}
                        onChangeText={text => setAnswerOnQuestion2(text)}
                        value={answerOnQuestion2}
                        onBlur={() => setAnswerOnQuestion2Blur(true)}
                        placeholderTextColor={"gray"}
                        placeholder={"TEXT"}
                        autoFocus={true}
                        selectionColor={"red"}
                    />)}
                    {answerOnQuestion2Blur && (
                        <>
                            <View style={styles.blockAnswerOnQuestion1}>
                                <Text style={styles.answerOnQuestion1}>{answerOnQuestion2 || ""}</Text>
                            </View>
                            <View style={styles.questionSection2}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage}>Сhoose two cool options</Text>
                            </View>
                        </>
                    )}
                    {answerOnQuestion2Blur && !isThreeAnswer && (
                        <View style={{
                            marginBottom: "8%",
                            marginTop: "4%",
                        }}>
                            <SelectMultiple
                                items={answerOneData}
                                selectedItems={threeAnswer}
                                onSelectionsChange={answerThreeChange}
                                onAddItem={onAddItem()}
                                selectedRowStyle={{ backgroundColor: "#C2DEEA" }}
                                rowStyle={{
                                    backgroundColor: "#E5E5E5",
                                    paddingLeft: "10%",
                                    borderBottomWidth: 0,
                                }}
                                checkboxStyle={{ backgroundColor: "#ECDFCF" }}
                                selectedCheckboxStyle={{ backgroundColor: "#C2DEEA" }}
                            />
                        </View>
                    )}
                    {isThreeAnswer && (
                        <>
                            <View style={styles.blockAnswerOnQuestion1}>
                                <Text style={styles.answerOnQuestion1}>{"I have " + transformationValue(threeAnswer)}</Text>
                            </View>
                            <View style={styles.questionSection2}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage}>When your birthday?</Text>
                            </View>
                        </>
                    )}
                    {isThreeAnswer && !date && (
                        <View style={{ marginTop: "5%" }}>
                            <Button title="Show Calendar" onPress={showDatePicker} />
                            <DateTimePickerModal
                                isVisible={isDatePickerVisible}
                                mode="date"
                                onConfirm={handleConfirm}
                                onCancel={hideDatePicker}
                            />
                        </View>
                    )}
                    {date && (<>
                        <View style={styles.blockAnswerOnQuestion1}>
                            <Text style={styles.answerOnQuestion1}>{date.toString().slice(4, 16) || ""}</Text>
                        </View>
                        <View style={styles.questionSection2}>
                            <Image
                                source={{uri: avatar}}
                                style={styles.questionIcon}
                            />
                            <Text style={styles.questionMessage}>Whats is your social security number?</Text>
                        </View>
                    </>)
                    }
                    {!answerOnQuestion5Blur && date && (<TextInput
                        style={{
                            height: 40,
                            borderColor: "#D4D4D4",
                            borderWidth: 1,
                            margin: "5%",
                            padding: 5,
                            paddingLeft: 10,
                            marginBottom: "2%",
                        }}
                        onChangeText={text => setAnswerOnQuestion5(text)}
                        keyboardType="number-pad"
                        value={answerOnQuestion5}
                        onBlur={() => setAnswerOnQuestion5Blur(true)}
                        placeholderTextColor={"gray"}
                        placeholder={"Number"}
                        autoFocus={true}
                        selectionColor={"red"}
                    />)}
                    {answerOnQuestion5Blur && answerOnQuestion5 && (
                        <>
                            <View style={styles.blockAnswerOnQuestion1}>
                                <Text style={styles.answerOnQuestion1}>{answerOnQuestion5 || ""}</Text>
                            </View>
                            <View style={styles.questionSection2}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage}>Please attach your photo file</Text>
                            </View>
                        </>
                    )}
                    {!filePath.uri && answerOnQuestion5Blur && answerOnQuestion5 && (
                        <View>
                            <TouchableOpacity
                                style={{ marginTop: 50, justifyContent: "center", alignItems: "center", marginBottom: "10%" }}
                                activeOpacity={0.5}
                                onPress={() => chooseFile("photo")}>
                                <IconAntDesign name="upload" size={35} color="#A0A0A0" />
                            </TouchableOpacity>
                        </View>
                    )}
                    {answerOnQuestion5Blur && answerOnQuestion5 && filePath.uri && (
                        <View style={{ marginBottom: "12%" }}>
                            <Image source={{ uri: filePath.uri }}
                                   style={{
                                       width: 86,
                                       height: 74,
                                       marginLeft: "auto",
                                       marginRight: 25,
                                       marginTop: 15,
                                       marginBottom: 15,
                                   }} />
                            <View style={styles.questionSection2}>
                                <Image
                                    source={{uri: avatar}}
                                    style={styles.questionIcon}
                                />
                                <Text style={styles.questionMessage} onClick={submitForm()}>Thank you, mister! Good job!
                                </Text>
                            </View>
                        </View>
                    )} */}
                {/* </View> */}
                </View>
                }
            </ScrollView>
        </>
    );
};
