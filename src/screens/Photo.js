import React from 'react';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  ActivityIndicator,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import Toast from 'react-native-simple-toast';
import Header from '../components/Header';
import Ican from 'react-native-vector-icons/Ionicons';
import {ScrollView} from 'react-native-gesture-handler';
import {connect} from 'react-redux';
import ImageResizer from 'react-native-image-resizer';

import {uploadPhoto} from '@redux/onboarding/actions';

class Photo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: '',
      check: false,
      cameraType: 'front',
      detected: [],
      openCam: false,
      torch: RNCamera.Constants.FlashMode.off,
    };
  }
  
  render() {
    return (
      <View style={{flex: 1}}>
        {!this.state.openCam ? (
          <ScrollView
            contentContainerStyle={{
              flexGrow: 1,
            }}
            showsVerticalScrollIndicator={false}>
            <View
              style={{
                flexDirection: 'column',
                marginLeft: 40,
                marginRight: 40,
                flex: 1,
                marginTop: 40,
                marginBottom: 40,
              }}>
              <View style={{width: '100%', flexDirection: 'row'}}>
                <View style={{width: '90%'}}>
                  <Header value={this.props.navigation} title="Photo" />
                </View>
              </View>

              {/* {this.state.data == '' ?
                        <View style={{alignItems: 'center',marginTop: 170}}>
                            <RNCamera
                                ref={(ref) => {
                                    this.camera = ref;
                                }}
                                style={styles.preview}
                                flashMode={RNCamera.Constants.FlashMode.off}
                                onFacesDetected={(arg) => {this.setState({detected: arg.faces})}}
                                type={this.state.cameraType}
                                faceDetectionLandmarks={RNCamera.Constants.FaceDetection.Landmarks.all}
                                androidCameraPermissionOptions={{
                                    title: 'Permission to use camera',
                                    message: 'We need your permission to use your camera',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                androidRecordAudioPermissionOptions={{
                                    title: 'Permission to use audio recording',
                                    message: 'We need your permission to use your audio',
                                    buttonPositive: 'Ok',
                                    buttonNegative: 'Cancel',
                                }}
                                onGoogleVisionBarcodesDetected={({barcodes}) => {

                                }}
                            />



                        </View> :  */}

              {this.state.data ? (
                <View style={{alignItems: 'center', marginTop: 25}}>
                  <Image
                    source={{uri: this.state.data}}
                    resizeMode="contain"
                    style={{
                      height: 400,
                      width: 400,
                      overflow: 'hidden',
                      marginBottom: 10,
                    }}
                  />
                  <View style={{alignItems: 'flex-start', marginLeft: '15%'}} />
                </View>
              ) : (
                <TouchableOpacity
                  onPress={() => this.setState({openCam: true})}
                  style={{
                    height: 330,
                    justifyContent: 'center',
                    alignItems: 'center',
                    borderWidth: 1,
                    backgroundColor: '#dedede',
                    marginTop: 25,
                  }}>
                  <Text style={{fontSize: 20, color: 'grey'}}>
                    Tab To Capture Image
                  </Text>
                </TouchableOpacity>
              )}
              {/* } */}
              {this.state.data == '' ? (
                <View style={{marginTop: 25, alignItems: 'center'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="copy" size={30} color="#18ad86" />
                    <View style={{marginLeft: '5%', alignItems: 'center'}}>
                      <Text style={{fontSize: 16, textAlign: 'center'}}>
                        Do not take photo of screen
                      </Text>
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: '3%',
                      justifyContent: 'flex-start',
                    }}>
                    <Icon name="info-circle" size={30} color="#18ad86" />
                    <View
                      style={{
                        marginLeft: '5%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 16}}>
                        Take photo in well lit area{' '}
                      </Text>
                    </View>
                  </View>
                </View>
              ) : (
                <View style={{marginTop: 25, alignItems: 'center'}}>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="copy" size={30} color="#18ad86" />
                    <View style={{marginLeft: '5%', alignItems: 'center'}}>
                      <Text style={{fontSize: 16, textAlign: 'center'}}>
                        Do not take photo of screen
                      </Text>
                      {/* <Text style={{ fontSize: 16 }}>Take photo in well lit area </Text> */}
                    </View>
                  </View>
                  <View
                    style={{
                      flexDirection: 'row',
                      marginTop: '3%',
                      justifyContent: 'flex-start',
                    }}>
                    <Icon name="info-circle" size={30} color="#18ad86" />
                    <View
                      style={{
                        marginLeft: '5%',
                        justifyContent: 'center',
                        alignItems: 'center',
                      }}>
                      <Text style={{fontSize: 16}}>
                        Take photo in well lit area{' '}
                      </Text>
                    </View>
                  </View>
                </View>
              )}

              {!this.state.check ? (
                <View style={{alignItems: 'center', marginTop: 25}}>
                  <TouchableOpacity
                    // disabled={this.state.detected.length > 0 ? false : true}

                    onPress={() => this.setState({openCam: true})}
                    // style={this.state.detected.length > 0 ? styles.start : styles.disable}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>
                      Take Photo
                    </Text>
                  </TouchableOpacity>
                </View>
              ) : this.props.loading ? (
                <ActivityIndicator size="large" color="black" />
              ) : (
                <View
                  style={{
                    alignItems: 'center',
                    marginTop: 30,
                    flexDirection: 'row',
                    justifyContent: 'center',
                  }}>
                  <TouchableOpacity
                    onPress={() => {
                      this.setState({data: '', check: false, openCam: true});
                      this.takePicture.bind(this);
                    }}
                    style={styles.start}>
                    <Text style={{color: 'white', fontSize: 20}}>Re-Take</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    onPress={() => {
                      if (this.state.detected.length > 0) {
                        this.props.uploadPhoto(
                          this.state.data
                        );
                      } else {
                        Toast.show(
                          'Face is not detected in picture',
                          Toast.SHORT,
                        );
                      }
                    }}
                    style={styles.start2}>
                    <Text style={{color: 'white', fontSize: 20}}>Next</Text>
                  </TouchableOpacity>
                </View>
              )}
              <View
                style={{
                  alignItems: 'center',
                  flexDirection: 'column',
                  flexGrow: 1,
                  justifyContent: 'flex-end',
                  marginTop: 25,
                }}>
                <View style={{flexDirection: 'row'}}>
                  <View
                    style={{
                      height: 10,
                      width: 10,
                      borderRadius: 50,
                      backgroundColor: '#63d4b6',
                      opacity: 0.7,
                    }}
                  />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleOne} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                  <View style={styles.singleDesign} />
                </View>
              </View>
            </View>
          </ScrollView>
        ) : (
          <RNCamera
            ref={ref => {
              this.camera = ref;
            }}
            style={styles.preview}
            flashMode={this.state.torch}
            onFacesDetected={arg => {
              this.setState({detected: arg.faces});
            }}
            type={this.state.cameraType}
            faceDetectionLandmarks={
              RNCamera.Constants.FaceDetection.Landmarks.all
            }
            androidCameraPermissionOptions={{
              title: 'Permission to use camera',
              message: 'We need your permission to use your camera',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            androidRecordAudioPermissionOptions={{
              title: 'Permission to use audio recording',
              message: 'We need your permission to use your audio',
              buttonPositive: 'Ok',
              buttonNegative: 'Cancel',
            }}
            onGoogleVisionBarcodesDetected={({}) => {}}>
            <View
              style={{
                flexDirection: 'column',
                flex: 1,
              }}>
              <TouchableOpacity
                onPress={() => this.takePicture()}
                style={styles.camerButtonOutline}>
                <View style={styles.camerButton} />
              </TouchableOpacity>
              <View
                style={{
                  position: 'absolute',
                  alignSelf: 'flex-end',
                  top: 10,
                  right: 20,
                }}>
                <TouchableOpacity
                  onPress={() => {
                    if (this.state.cameraType == 'back') {
                      this.setState({cameraType: 'front'});
                    } else {
                      this.setState({cameraType: 'back'});
                    }
                  }}>
                  <Image
                    source={require('../images/icons/rotate.png')}
                    style={{height: 45, width: 45}}
                    resizeMode="contain"
                  />
                </TouchableOpacity>
              </View>
              <TouchableOpacity
                style={{
                  position: 'absolute',
                  bottom: 55,
                  alignSelf: 'flex-end',
                  right: 30,
                  alignItems: 'center',
                  justifyContent: 'center',
                  borderRadius: 30,
                  backgroundColor: '#00000080',
                  height: 50,

                  width: 50,
                }}
                onPress={() => {
                  if (this.state.torch == RNCamera.Constants.FlashMode.off) {
                    this.setState({torch: RNCamera.Constants.FlashMode.torch});
                  } else {
                    this.setState({torch: RNCamera.Constants.FlashMode.off});
                  }
                }}>
                <Ican name="ios-flashlight" size={28} color="white" />
              </TouchableOpacity>
            </View>
          </RNCamera>
        )}
      </View>
    );
  }

  takePicture = async () => {
    this.setState({check: true});

    if (this.camera) {
      const data = await this.camera.takePictureAsync();

      const image = data.uri;
      
      this.setState({data: image, openCam: false});
      var checkWidth = '';
      var checkHeight = '';

      await Image.getSize(image, (width, height) => {
        (checkWidth = width), (checkHeight = height);
      });
  
      var t = '';
      
      await ImageResizer.createResizedImage(
        image,
        300,
        300,
        'PNG',
        50,
      ).then(({uri}) => {
        this.setState({data: uri, openCam: false});
      });


      // console.log('ok',this.state.data)
      // console.log('..............',pp)
    }
  };
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // flexDirection: 'column',
    // backgroundColor: 'black',
  },
  preview: {
    // height: 50,
    // width: '70%',
    // flex: 1,
    // position: 'relative',
    // zIndex: 1
    flex: 1,
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    padding: 15,
    paddingHorizontal: 20,
    alignSelf: 'center',
    margin: 20,
  },
  text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  start: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: '#18ad86',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  disable: {
    opacity: 0.8,
    height: 50,
    width: 200,
    backgroundColor: 'grey',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  start2: {
    opacity: 0.8,
    height: 50,
    width: 60,
    backgroundColor: 'black',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: '5%',
  },
  lastDesign: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  singleDesign: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#63d4b6',
    opacity: 0.7,
    marginLeft: 5,
  },
  singleOne: {
    height: 10,
    width: 10,
    borderRadius: 50,
    backgroundColor: '#04c491',
    marginLeft: 5,
  },
  camerButtonOutline: {
    borderColor: 'white',
    borderRadius: 50,
    borderWidth: 3,
    height: 70,
    width: 70,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 35,
  },
  camerButton: {
    backgroundColor: 'white',
    borderRadius: 50,
    flex: 1,
    margin: 3,
  },
});
const mapStateToProps = state => {
  const {loading} = state.onboarding;
  return {
    loading
  };
};
export default connect(
  mapStateToProps,
  {uploadPhoto},
)(Photo);
