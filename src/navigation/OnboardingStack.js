import React from 'react';

import {createStackNavigator} from '@react-navigation/stack';

import {fromLeft, fromTop} from 'react-navigation-transitions';
import Welcome from '../screens/Welcome';
import EnterPhone from '../screens/EnterPhone';
import OTP from '../screens/OTP';
import Kyc from '../screens/Kyc';
import Photo from '../screens/Photo';
import Video from '../screens/Video';
import home2 from './../components/home2';
// import Documents from './../components/documents';
import DocumentScanner from '../screens/scanner';
// import scanner from './../components/scanner';
import PersonalInfo from '../screens/PersonalInfo';
import DocumentScan from '../components/DocumentScan';
import header from '../components/Header';
import policeVerification from './../components/policeVerification';
import ReScanDocument from './../screens/ReScanDocument';
import splashScreen1 from './../components/splashScreen1';
import splashScreen2 from './../components/splashScreen2';
import toltip from './../components/toltip';
import fullPhotoCam from './../components/fullPhotoCam';
import {Badge} from 'react-native-paper';
import videoCalling from './../components/videoCalling';
import policeConfirmation from './../components/policeConfirmation';
import PaymentVerification from '../screens/PaymentVerification';
import congratulation from './../components/congratulation';
import DocumentReviewToUpload from './../components/DocumentReviewToUpload';
import AddNewDocument from '../components/AddNewDocument';


import {BottomNavigation} from './../../Navigation'


const Stack = createStackNavigator();

export default function ({ navigation, route }) {
	return (

			<Stack.Navigator screenOptions={{ headerShown: false, animationEnabled: false }} initialRouteName="WelcomeScreen">
				{/* <Stack.Screen
					name="splashScreen1"
					component={splashScreen1}
				/>
				<Stack.Screen
					name="splashScreen2"
					component={splashScreen2}
				/> */}
				<Stack.Screen
					name="WelcomeScreen"
					component={Welcome}
				/>
				<Stack.Screen
					name="EnterPhone"
					component={EnterPhone}
				/>
				<Stack.Screen
					name="Otp"
					component={OTP}
				/>
				<Stack.Screen
					name="Kyc"
					component={Kyc}
					TouchableOpacity={() => navigation}
				/>
				<Stack.Screen
					name="Scanner"
					component={DocumentScanner}
				/>
				<Stack.Screen
					name="Video"
					component={Video}
				/>
				<Stack.Screen
					name="Photo"
					component={Photo}
				/>
				<Stack.Screen
					name="PersonalInfo"
					component={PersonalInfo}
				/>
				<Stack.Screen
					name="PaymentVerification"
					component={PaymentVerification}
				/>

				<Stack.Screen
					name="videoCalling"
					component={videoCalling}
				/>
				<Stack.Screen
					name="policeConfirmation"
					component={policeConfirmation}
				/>
				<Stack.Screen
					name="policeVerification"
					component={policeVerification}
				/>

				<Stack.Screen
					name="congratulation"
					component={congratulation}
				/>
				<Stack.Screen
					name="header"
					component={header}
				/>
				<Stack.Screen
					name="ReScanDocument"
					component={ReScanDocument}
				/>
				<Stack.Screen
					name="DocumentScan"
					component={DocumentScan}
				/>
				<Stack.Screen
					name="DocumentReviewToUpload"
					component={DocumentReviewToUpload}
				/>
				<Stack.Screen
					name="home"
					component={BottomNavigation}
				/>
				<Stack.Screen
					name="AddNewDocument"
					component={AddNewDocument}
				/>

				{/* <Stack.Screen name="toltip" component={toltip} options={{animationEnabled: false,headerShown: false}} /> */}

				<Stack.Screen
					name="fullPhotoCam"
					component={fullPhotoCam}
				/>

				{/* <Stack.Screen name="yourDocs" component={yourDocs} options={{headerShown: false}} /> */}
				{/* <Stack.Screen name="home2" component={bottomNavi} options={{headerShown: false}} /> */}
			</Stack.Navigator>
	);
}
