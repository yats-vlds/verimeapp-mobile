import React, {useState, useEffect} from 'react';
import {Linking, StyleSheet, Text, View} from 'react-native';
import {WebView} from 'react-native-webview';

import Constants from '../../Constants';

const SERVER_URL = Constants.SERVER_URL;

// const useMount = func =>
//   useEffect(
//     () =>
//       func(
//         Linking.openURL(`${SERVER_URL}/transection`),
//       ),
//     [],
//   );

// const useInitialURL = () => {
//   const [url, setUrl] = useState(null);
//   const [processing, setProcessing] = useState(true);

// useMount(() => {
//   const getUrlAsync = async () => {
//     // Get the deep link used to open the app
//     const initialUrl = await Linking.getInitialURL();

//     // The setTimeout is just for testing purpose
//     setTimeout(() => {
//       setUrl(initialUrl);
//       setProcessing(false);
//     }, 1000);
//   };

//   getUrlAsync();
// });

//   return {url, processing};
// };

const App = () => {
  // const {url: initialUrl, processing} = useInitialURL(
  //   `${SERVER_URL}/transection`,
  // );
  onNavigationStateChange = e => {
    console.log(e);
  };
  return (
    <View style={styles.container}>
      <WebView
        source={{uri: `${SERVER_URL}/transection`}}
        onNavigationStateChange={e => this.onNavigationStateChange(e)}
        onShouldStartLoadWithRequest={e => {
          console.log(e);
          return true;
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {flex: 1, justifyContent: 'center'},
});

export default App;
